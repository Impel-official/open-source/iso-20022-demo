import {customElement} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {RestAdapter, restAdapter} from "../shared/rest/RestAdapter";
import { TransferRequestStore, transferRequestStore } from "../tranfer-request/TransferRequestStore";
import {typography} from "../shared/config/styles";

const styles = css`
${typography}
.container{
    display: flex;
    flex-direction: column;
    width: 500px;
    margin: auto;
}
.settings {
  margin-top: 50px;
  margin-bottom: 25px;
}
.row {
   display: flex;
   margin-bottom: 1em;
}
.row * {
  box-sizing: border-box;
}
.label {
    min-width: 18em;
    padding-right: 1em;
    display: flex;
    align-self: center;
}
ui5-input, input-suggestion {
    width: 100%;
}
.card-content {
    margin: 2em;
}
#sendButton {
    margin-top: 2em;
    width: 100%;
    height: 3em;
}
.hidden {
    display: none;
}
ui5-busy-indicator{
  width:100%;
}

ui5-card-header::part(title){
  font-family: 'Graphie', sans-serif !important;
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 100%;
  letter-spacing: 0.05em;
  color: #002C72;
}
`

@customElement('settings-view')
export class SettingsView extends LitElement {
  static override styles = styles;


  private restAdapter: RestAdapter;
  private transferRequestStore: TransferRequestStore;

  constructor() {
    super();
    this.restAdapter = restAdapter;
    this.transferRequestStore = transferRequestStore;
  }

  override connectedCallback() {
    super.connectedCallback();
  }

  setConfirmationLevelSimple(){
    this.transferRequestStore.setDetailedProgress(false);
  }
  setConfirmationLevelDetailed(){
    this.transferRequestStore.setDetailedProgress(true);
  }

  override render(){

    const darkMode = html`
      <div class="row">
        <div class="label"><ui5-title level="H5"><b>Dark Mode</b></ui5-title></div>
        <ui5-switch text-on="Yes" text-off="No" disabled></ui5-switch>
      </div>
    `
    return html`

        <div class="container">
          <div class="settings"><span class="h4">Settings</span></div>
          <ui5-card>
              <div class="card-content">
                  ${false ? darkMode : ''}
                  <div class="row">
                      <div class="label"><ui5-title level="H5"><b>Alert Confirmation Level</b></ui5-title></div>
                      <div>
                          <ui5-radio-button text="Simple" .checked="${!this.transferRequestStore.isDetailedProgress()}" name="alertConfirmationLevel"
                                @click=${this.setConfirmationLevelSimple}></ui5-radio-button>
                          <ui5-radio-button text="Detailed" .checked="${this.transferRequestStore.isDetailedProgress()}" name="alertConfirmationLevel"
                                @click=${this.setConfirmationLevelDetailed}></ui5-radio-button>
                      </div>
                  </div>
              </div>
          </ui5-card>
        </div>
    `
  }
}
