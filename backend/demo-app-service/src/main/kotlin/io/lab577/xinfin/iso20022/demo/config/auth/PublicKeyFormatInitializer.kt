package io.lab577.xinfin.iso20022.demo.config.auth

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.MapPropertySource

class PublicKeyFormatInitializer: ApplicationContextInitializer<ConfigurableApplicationContext> {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(PublicKeyFormatInitializer::class.java)
    }
    override fun initialize(applicationContext: ConfigurableApplicationContext) {

        val environment = applicationContext.environment

        val publicKeyFromEnv = System.getenv("REST_SECURITY_ISSUER_PUBLIC_KEY")

        if(publicKeyFromEnv != null){

            logger.debug("Read public key from env var $publicKeyFromEnv")

            environment.propertySources.forEach { propertySource ->
                val publicKey = propertySource.getProperty("security.oauth2.resource.jwt.key-value")

                if(publicKey !== null){
                    val formattedPk = publicKeyFromEnv.toString().replace("\\n", "\n")

                    val formattedProperty = MapPropertySource(
                        "formatted-${propertySource.name}", mapOf("security.oauth2.resource.jwt.key-value" to formattedPk)
                    )

                    environment.propertySources.addBefore(propertySource.name, formattedProperty)
                }
            }

        }

    }
}
