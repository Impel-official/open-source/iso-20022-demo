export class Party {
  public constructor(
    public bic: string,
    public name: string,
    public xinFinAddress: string,
  ) {}
  
  static fromJson(json: any): Party {
    // TODO: this needs json schema validation
    return new Party(json.bic, json.name, json.xinFinAddress)
  }
}
