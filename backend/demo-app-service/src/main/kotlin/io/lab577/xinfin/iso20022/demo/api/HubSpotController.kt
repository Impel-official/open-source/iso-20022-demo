package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.api.dto.HubSpotUser
import io.lab577.xinfin.iso20022.demo.integration.hubspot.HubSpotClientService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/hubspot")
class HubSpotController(
    val hubSpotClientService: HubSpotClientService
) {

    @GetMapping("/contact/{id}")
    fun findContact(@PathVariable id: String): HubSpotUser? {
        val hubSpotUser = hubSpotClientService.findContactByIdProperty(id)
        return if (hubSpotUser != null) HubSpotUser(hubSpotUser.id, hubSpotUser.properties["email"]!!)
        else null
    }
}

