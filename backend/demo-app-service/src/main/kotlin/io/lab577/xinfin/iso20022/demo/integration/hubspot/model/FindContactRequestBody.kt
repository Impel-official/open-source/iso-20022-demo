package io.lab577.xinfin.iso20022.demo.integration.hubspot.model

data class FindContactRequestBody(
    val filterGroups: List<FilterGroup>
)

data class FilterGroup(
    val filters: List<Filter>
)

data class Filter(
    val propertyName: String,
    val operator: String,
    val value: String
)

