type AuthNMethod = "KEYCLOAK" | "HUBSPOT" | "NONE";

export class User {
  constructor(
    readonly id: string,
    readonly username: string,
    readonly authNMethod: AuthNMethod,
    readonly email?: string
  ) {}
  
  isAuthenticated(): boolean {
    return this.authNMethod !== 'NONE';
  }
  
  shouldHideLogout(): boolean {
    return this.authNMethod === "HUBSPOT";
  }
  
  static createUnauthenticatedUser(): User {
    return new User('0', 'ACCOUNT NAME', 'NONE', undefined);
  }
}
