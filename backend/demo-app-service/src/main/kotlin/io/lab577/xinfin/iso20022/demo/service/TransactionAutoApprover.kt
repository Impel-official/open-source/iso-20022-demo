package io.lab577.xinfin.iso20022.demo.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Component
@ConditionalOnProperty(value = ["app.autoApproveTransactions.enabled"], havingValue = "true", matchIfMissing = false)
class TransactionAutoApprover(
    private val transactionService: TransactionService,
    @Value("\${app.bic}")
    var bic: String,
    @Value("\${app.autoApproveTransactions.approveDelayMs}")
    var approveDelayMs: Long
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(TransactionAutoApprover::class.java)
    }

    protected val approvedTransactions: MutableList<UUID> = mutableListOf()

    @Scheduled(fixedDelayString = "\${app.autoApproveTransactions.scheduledTaskFixedDelayMs:5000}")
    fun approve() {
        val now = OffsetDateTime.now()
        transactionService.findTransactionsToApprove(bic)
            .filter { !approvedTransactions.contains(it.id) }
            .filter { it.created!!.plus(approveDelayMs, ChronoUnit.MILLIS).isBefore(now) }
            .forEach {
                try {
                    logger.info("Auto approve transaction {id: ${it.id}}")
                    transactionService.review(it.id, true)
                    approvedTransactions.add(it.id)
                } catch (ex: Exception) {
                    logger.error("Failed to auto approve transaction {id: ${it.id}}", ex)
                }
            }
    }
}
