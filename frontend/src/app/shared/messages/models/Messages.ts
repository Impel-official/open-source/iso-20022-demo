import {Message, MessageType} from './Message';

export class Messages {
  private readonly counter;
  private readonly messages: Array<Message>;
  
  constructor(initialMessages: Array<Message> = [], initialMessageCounter = 0) {
    this.messages = initialMessages;
    this.counter = initialMessageCounter;
  }
  
  addMessage(type: MessageType, description: string): Messages {
    const message = new Message(this.counter.toString(), type, description);
    return new Messages([...this.messages, message], this.counter + 1);
  }
  
  removeMessage(messageId: string): Messages {
    return new Messages(
      this.messages.filter(it => it.id !== messageId),
      this.counter
    );
  }
  
  getMessages() {
    return [...this.messages]
  }
  
  static empty() {
    return new Messages([], 0);
  }
}