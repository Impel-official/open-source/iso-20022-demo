import builder from '@rsql/builder';
import {emit} from '@rsql/emitter';
import {parse} from '@rsql/parser';
import {ExpressionNode} from '@rsql/ast';
import {enumFromStringValue} from "../../shared/utils";
import {TransactionDtoDirectionEnum, TransactionDtoStatusEnum} from "../../shared/api";
import dayjs from "dayjs";

export class TransactionsTableFilter {
  static allowedCurrencies: Array<string> = [];
  static allowedStates: Array<string> = [];

  public constructor(
      public readonly startDate: string,
      public readonly endDate: string,
      public readonly fieldType?: TransactionsTableFilterFieldType,
      public readonly fieldValue?: string,
      public readonly status?: TransactionsTableFilterStatus,
  ) {
    this.validate()
  }

  validate() {
    // TODO: validate fields and throw error events
  }

  static fromUrlParams(urlSearchParams: URLSearchParams): TransactionsTableFilter {
    const status = enumFromStringValue(TransactionsTableFilterStatus, urlSearchParams.get('status') ?? '');
    const fieldType = enumFromStringValue(TransactionsTableFilterFieldType, urlSearchParams.get('fieldType') ?? '');
    return new TransactionsTableFilter(
        urlSearchParams.get('startDate') ?? '',
        urlSearchParams.get('endDate') ?? '',
        fieldType,
        urlSearchParams.get('fieldValue') ?? '',
        status,
    )
  }

  static toUrlParams(filter: TransactionsTableFilter): URLSearchParams {
    const searchParams = Object.entries(filter)
        .filter(([k, v]) => v != null && v !== '')
        .reduce((acc, [k, v]) => ({...acc, [k]: v}), {});
    return new URLSearchParams(searchParams);
  }

  toRSQL(): string {
    const filterExpressions: Array<ExpressionNode> = [];
    if (this.startDate) {
      const startDate = dayjs.utc(`${this.startDate} UTC`).startOf("date").toISOString();
      filterExpressions.push(builder.ge('requestDate', startDate));
    }
    if (this.endDate) {
      const endDate = dayjs.utc(`${this.endDate} UTC`).endOf("date").toISOString();
      filterExpressions.push(builder.le('requestDate', endDate));
    }
    if (this.status) {
      const {status, direction} = this.getStatusAndDirection()
      if (direction) {
        filterExpressions.push(builder.eq('direction', direction));
      }
      if (status) {
        filterExpressions.push(builder.eq('status', status));
      }
    }
    if (this.fieldType && this.fieldValue) {
      const filterExpression = builder.and(
          parse(`${this.fieldType}==${this.fieldValue}`)
      );
      filterExpressions.push(filterExpression);
    }
    if (filterExpressions.length == 0) return '';
    const initialFilterExpression = <ExpressionNode>filterExpressions.pop();
    const filterExpression = filterExpressions.reduce(
        (total, current) => builder.and(total, current),
        initialFilterExpression
    );
    return emit(filterExpression);
  }

  getStatusAndDirection(): { status?: TransactionDtoStatusEnum, direction?: TransactionDtoDirectionEnum } {
    let direction: TransactionDtoDirectionEnum | undefined = undefined;
    let status: TransactionDtoStatusEnum | undefined = undefined;
    switch (this.status) {
      case TransactionsTableFilterStatus.inbox_received:
        direction = 'INBOX';
        status = 'Received';
        break;
      case TransactionsTableFilterStatus.inbox_accepted:
        direction = 'INBOX';
        status = 'Approved';
        break;
      case TransactionsTableFilterStatus.inbox_rejected:
        direction = 'INBOX';
        status = 'Rejected';
        break;
      case TransactionsTableFilterStatus.outbox_received:
        direction = 'OUTBOX';
        status = 'Received';
        break;
      case TransactionsTableFilterStatus.outbox_approved:
        direction = 'OUTBOX';
        status = 'Approved';
        break;
      case TransactionsTableFilterStatus.outbox_rejected:
        direction = 'OUTBOX';
        status = 'Rejected';
        break;
      case TransactionsTableFilterStatus.outbox_failed:
        direction = 'OUTBOX';
        status = 'Failed';
        break;
      case TransactionsTableFilterStatus.expired: {
        status = 'Expired';
        break;
      }
    }
    return {status, direction}
  }

  static empty(): TransactionsTableFilter {
    return new TransactionsTableFilter(
        '',
        '',
        undefined,
        '',
        undefined,
    )
  }
}

export enum TransactionsTableFilterStatus {
  expired = 'expired',
  inbox_received = 'inbox_received',
  inbox_accepted = 'inbox_accepted',
  inbox_rejected = 'inbox_rejected',
  outbox_received = 'outbox_received',
  outbox_approved = 'outbox_accepted',
  outbox_rejected = 'outbox_rejected',
  outbox_failed = 'outbox_failed',
}

export enum TransactionsTableFilterFieldType {
  endToEndId = 'endToEndId',
  counterPartyBic = 'counterPartyBic',
  recipientAccount = 'recipientAccount',
  senderAccount = 'senderAccount',
  amount = 'amount',
}
