import {Date2} from '../../shared/models/Date2';
import {Amount} from '../../shared/models/Amount';
import {TransactionMessageCollateralDto, TransactionDtoDirectionEnum, TransactionDtoStatusEnum} from "../../shared/api";

export class Transaction {
  public readonly requestDate2: Date2;
  public readonly validUntilDate2: Date2;
  public readonly amount2: Amount;

  constructor(
    public readonly id: string,
    public readonly senderBic: string,
    public readonly recipientBic: string,
    public readonly counterPartyBic: string,
    public readonly senderAccount: string,
    public readonly recipientAccount: string,
    public readonly requestDate: string,
    public readonly currency: string,
    public readonly amount: string,
    public readonly direction: TransactionDtoDirectionEnum,
    public readonly status: TransactionDtoStatusEnum,
    public readonly type: string,
    public readonly endToEndId: string,
    public readonly validUntil: string,
    public readonly referenceInfo: string,
    public readonly collateral: TransactionMessageCollateralDto,
    public readonly initiator: string,
    public readonly responder: string,
    public readonly rollupTransactionHash: string,

    public readonly messages: ReadonlyArray<TransactionMessage>,
    public readonly isApprovalPending: boolean
  ) {
    this.requestDate2 = Date2.from(requestDate);
    this.validUntilDate2 = Date2.from(validUntil);
    this.amount2 = Amount.from(amount, currency);
  }

  static fromJson(json: any): Transaction {
    // TODO: this needs json schema validation
    return new Transaction(
      json.id,
      json.senderBic,
      json.recipientBic,
      json.counterPartyBic,
      json.senderAccount,
      json.recipientAccount,
      json.requestDate,
      json.currency,
      json.amount,
      json.direction,
      json.status,
      json.type,
      json.endToEndId,
      json.validUntil,
      json.referenceInfo,
      json.collateral,
      json.initiator,
      json.responder,
      json.rollupTransactionHash,
      json.messages.map((it: any)=> TransactionMessage.fromJson(it)),
      false // always reset when fetching data from backend
    )
  }


  equals(transaction: Transaction) {
    return this.id === transaction.id;
  }

  updateStatus(status: TransactionDtoStatusEnum): Transaction {
    return this.copy(status);
  }

  updateRollupTransactionHash(rollupTransactionHash: string): Transaction {
    return this.copy(undefined, undefined, undefined, rollupTransactionHash);
  }

  addMessage(message: TransactionMessage): Transaction {
    return this.copy(undefined, [...this.messages, message]);
  }

  isUnapproved(): boolean {
    return this.status === 'Received'
  }

  setIsApprovalPending(isApprovalPending: boolean): Transaction {
    return this.copy(undefined, undefined, isApprovalPending);
  }

  private copy(
    status?: TransactionDtoStatusEnum,
    messages?: Array<TransactionMessage>,
    isApprovalPending?: boolean,
    rollupTransactionHash?: string
  ): Transaction {
    return new Transaction(
      this.id,
      this.senderBic,
      this.recipientBic,
      this.counterPartyBic,
      this.senderAccount,
      this.recipientAccount,
      this.requestDate,
      this.currency,
      this.amount,
      this.direction,
      status || this.status,
      this.type,
      this.endToEndId,
      this.validUntil,
      this.referenceInfo,
      this.collateral,
      this.initiator,
      this.responder,
      rollupTransactionHash || this.rollupTransactionHash,
      messages ? messages : this.messages.map(it => it.copy()),
      isApprovalPending ? isApprovalPending : this.isApprovalPending
    )
  }
}

export class TransactionMessage{
  public readonly messageDate2: Date2;
  public readonly amount2: Amount;
  public readonly direction2: string;

  constructor(
    public id: string,
    public transactionId: string,
    public senderBic: string,
    public recipientBic: string,
    public counterPartyBic: string,
    public currency: string,
    public amount: string,
    public direction: string,
    public type: '009' | '002',
    public messageDate: string,
    public network: string,
    public collateral: string,
    public rollupTransactionHash: string,
  ) {
    this.messageDate2 = Date2.from(messageDate);
    this.amount2 = Amount.from(amount, currency);
    this.direction2 = this.direction === 'OUTBOX' ? 'Sent' : 'Received';
  }

  copy(): TransactionMessage {
    return new TransactionMessage(
      this.id,
      this.transactionId,
      this.senderBic,
      this.recipientBic,
      this.counterPartyBic,
      this.currency,
      this.amount,
      this.direction,
      this.type,
      this.messageDate,
      this.network,
      this.collateral,
      this.rollupTransactionHash
    )
  }


  static fromJson(json: any): TransactionMessage {
    // TODO: this needs json schema validation
    return new TransactionMessage(
      json.id,
      json.transactionId,
      json.senderBic,
      json.recipientBic,
      json.counterPartyBic,
      json.currency,
      json.amount,
      json.direction,
      json.type,
      json.messageDate,
      json.network,
      json.collateral,
      json.rollupTransactionHash
    )
  }
}
