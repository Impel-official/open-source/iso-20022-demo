export class TransactionsTableSorting {
  public constructor(
    public readonly sort: string,
    public readonly direction: string
  ) {
    this.validate();
  }
  
  validate(){
    // TODO: validate fields and throw error events
  }
  
  static default(): TransactionsTableSorting{
    return new TransactionsTableSorting('requestDate', 'DESC')
  }
}