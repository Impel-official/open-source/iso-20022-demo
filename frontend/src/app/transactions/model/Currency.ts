export class Currency {

  // TODO: This should be part of the service
  private readonly symbols: {[key: string]: string} = {
    "GBP": "£",
    "USD": "$",
    "EUR": "€"
  };

  public constructor(
    public code: string,
  ) {
  }

  get symbol(): string {
    return this.symbols[this.code];
  }

  static fromJson(json: any): Currency {
    // TODO: this needs json schema validation
    return new Currency(json)
  }

}