import {App} from './App';
import {DashboardView} from './app/dashboard/DashboardView';
import {NavBar} from './app/shared/components/NavBar';
import {FooterBar} from "./app/shared/components/FooterBar";
import {NotFoundView} from './app/shared/components/NotFoundView';
import {UnauthorizedView} from './app/shared/components/UnauthorizedView';
import {TransferRequestView} from './app/tranfer-request/TransferRequestView';
import {TransactionsView} from './app/transactions/components/TransactionsView';
import {UnapprovedView} from './app/transactions/components/UnapprovedView';
import {TransactionsTable} from './app/transactions/model/TransactionsTable';
import {
  TransactionsTableFilters
} from './app/transactions/components/table/TransactionsTableFilters';
import {MessagesElement} from './app/shared/messages/MessagesElement';
import {TablePaging} from './app/transactions/components/table/TablePaging';
import {ShareDemoExperiencePanel} from "./app/shared/components/ShareDemoExperiencePanel";
import {AppToast} from "./app/shared/components/AppToast";
import {CharactersRemainingElement} from './app/shared/components/CharactersRemainingElement';

// add all elements
declare global {
  interface HTMLElementTagNameMap {
    "app-root": App,
    "dashboard-view": DashboardView,
    "footer-bar": FooterBar,
    "share-demo-experience-panel": ShareDemoExperiencePanel,
    "nav-bar": NavBar,
    "not-found-view": NotFoundView,
    "unauthorized-view": UnauthorizedView,
    "user-messages": MessagesElement,
    "transfer-request-view": TransferRequestView,
    "transactions-view": TransactionsView,
    "transactions-table": TransactionsTable,
    "transactions-table-filters": TransactionsTableFilters,
    "unapproved-view": UnapprovedView,
    "table-paging": TablePaging,
    "app-toast": AppToast,
    "characters-remaining": CharactersRemainingElement,
  }
}


