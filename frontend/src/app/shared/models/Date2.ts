import {formatDateTime} from "../date-utils";

export class Date2 {
  private constructor(private date: Date) {}

  valueOf(): number {
    return this.date.valueOf();
  }

  static from(date: string): Date2 {
    return new Date2(new Date(date));
  }

  getTime(): number {
    return this.date.getTime();
  }
  
  getFormatted(): string {
    return formatDateTime(this.date);
  }
}
