package io.lab577.xinfin.iso20022.demo.service

import io.lab577.xinfin.iso20022.demo.config.WebSocketConfig.Companion.WEBSOCKET_TOPIC_TRANSACTION_MESSAGES
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionProofStatus
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.domain.repository.TransactionEntityRepository
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessageEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional(readOnly = true)
class TransactionService(
    repository: TransactionEntityRepository,
    @Lazy
    private val transactionMessageService: TransactionMessageService,
    private val simpTemplate: SimpMessagingTemplate,
) : BaseService<TransactionEntity, UUID, TransactionEntityRepository>(repository) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(TransactionService::class.java)
    }

    @Transactional(readOnly = false)
    fun review(id: UUID, approve: Boolean): UUID {
        val transaction = getById(id)
        return transactionMessageService.sendPacs002(transaction, approve)
    }

    fun getByEndToEndId(endToEndId: String): TransactionEntity =
        repository.findTopByEndToEndIdOrderByCreatedDesc(endToEndId)

    @Transactional(readOnly = false)
    fun updateProofStatusByMessagesIds(proofStatus: TransactionProofStatus, messagesIds: List<UUID>): Int =
        repository.updateProofStatusByMessagesIds(proofStatus, messagesIds)


    @Transactional(readOnly = false)
    fun updateRollupTransactionHashByMessagesIds(transactionHash: String, messagesIds: List<UUID>): Int {
        return repository.updateRollupTransactionHashByMessagesIds(transactionHash, messagesIds).also {
            repository.findAllByMessagesIds(messagesIds).forEach {
                simpTemplate.convertAndSend(
                    WEBSOCKET_TOPIC_TRANSACTION_MESSAGES,
                    TransactionMessageEvent.from(it)
                )
            }
        }
    }

    fun getByStatusAndSenderBic(
        status: String,
        senderBic: String,
        sortBy: String,
        sortDirection: Sort.Direction,
        pageNumber: Int,
        pageSize: Int
    ): Page<TransactionEntity> = repository.getByStatusAndSenderBic(
        status, senderBic, PageRequest.of(pageNumber, pageSize, Sort.by(sortDirection, sortBy))
    )

    fun findReadyToCommitTxRecipients(senderBic: String): List<String> =
        repository.findRecipientBicsByStatusesAndSenderBic(
            setOf(TransactionStatus.Approved, TransactionStatus.Rejected), senderBic
        )

    fun findByStatusAndRecipientBic(
        status: TransactionStatus, recipientBic: String, pageable: PageRequest
    ): Page<TransactionEntity> = repository.findByStatusAndRecipientBic(
        status, recipientBic, pageable
    )

    fun findAllByStatusAndRecipientBic(
        status: TransactionStatus, recipientBic: String
    ): List<TransactionEntity> = repository.findAllByStatusAndRecipientBic(
        status, recipientBic
    )

    fun findAllByStatusesAndRecipientBic(
        statuses: Set<TransactionStatus>,
        proofStatus: TransactionProofStatus,
        recipientBic: String
    ): List<TransactionEntity> = repository.findAllByStatusesAndRecipientBic(
        statuses, proofStatus, recipientBic
    )

    fun findTransactionsToApprove(recipientBic: String): List<TransactionEntity> {
        return repository.findAllByStatusAndRecipientBic(TransactionStatus.Received, recipientBic)
    }

}
