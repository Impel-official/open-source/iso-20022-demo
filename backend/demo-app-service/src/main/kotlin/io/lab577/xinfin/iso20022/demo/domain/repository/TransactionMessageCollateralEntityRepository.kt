package io.lab577.xinfin.iso20022.demo.domain.repository

import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageCollateralEntity
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TransactionMessageCollateralEntityRepository : BaseRepository<TransactionMessageCollateralEntity, UUID>
