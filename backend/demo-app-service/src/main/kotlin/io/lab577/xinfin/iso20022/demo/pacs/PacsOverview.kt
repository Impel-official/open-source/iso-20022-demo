package io.lab577.xinfin.iso20022.demo.pacs

import io.lab577.xinfin.iso20022.demo.api.dto.TransactionMessageCollateralDto
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import java.math.BigDecimal
import java.time.OffsetDateTime

data class PacsOverview(
    val senderBic: String,
    val recipientBic: String,
    val senderAccount: String? = null,
    val recipientAccount: String? = null,
    val amount: BigDecimal,
    val currency: String,
    val end2endId: String,
    val referenceInfo: String? = null,
    val type: PacsType,
    val rawXml: String,
    val messageDate: OffsetDateTime,
    val validUntil: OffsetDateTime?,
    val statusOverride: TransactionStatus? = null,
    val collateral: TransactionMessageCollateralDto?
) {
    override fun toString(): String {
        return "PacsOverview(" +
            "senderBic='$senderBic', " +
            "recipientBic='$recipientBic', " +
            "amount=$amount, " +
            "end2endId='$end2endId', " +
            "messageDate=$messageDate, " +
            ")"
    }
}
