// This file will be replaced with actual app config
var config = {
  "branding" : {
    "logoUrl": "",
    "icoUrl": "",
    "orgName": ""
  },
  "rest" : {
    "references": "",
    "transactions": "",
    "transactionMessages": ""
  },
  "stomp" : {
    "url": "",
    "newTransactionsTopic": "",
    "transactionsChangesTopic": ""
  },
  "bic": "",
}
