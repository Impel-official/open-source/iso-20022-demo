package io.lab577.xinfin.iso20022.demo.integration.hubspot.model

data class FindContactResponse(
    val total: Int,
    val results: List<HubSpotContact>,
    val archived: Boolean?
)
