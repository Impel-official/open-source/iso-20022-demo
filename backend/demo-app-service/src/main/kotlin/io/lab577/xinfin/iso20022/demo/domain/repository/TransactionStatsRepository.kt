package io.lab577.xinfin.iso20022.demo.domain.repository

import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.metrics.TransactionCollateralStatsDataProjection
import io.lab577.xinfin.iso20022.demo.metrics.TransactionStatsDataProjection
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.OffsetDateTime
import java.util.*

@Repository
interface TransactionStatsRepository : BaseRepository<TransactionEntity, UUID> {
    @Query(
        "SELECT new io.lab577.xinfin.iso20022.demo.metrics.TransactionStatsDataProjection(" +
            "   COUNT(te), " +                // count
            "   SUM(te.amount), " +           // amountTotal
            "   te.currency" +                // currency
            ") " +
            "FROM TransactionEntity AS te " +
            "WHERE te.senderBic = :senderBic " +
            "   and te.requestDate >= :startDateTime " +
            "   and te.requestDate <= :endDateTime " +
            "GROUP BY te.currency"
    )
    fun getSentTransactionStatsDataProjections(
        @Param("senderBic") senderBic: String,
        @Param("startDateTime") startDateTime: OffsetDateTime,
        @Param("endDateTime") endDateTime: OffsetDateTime
    ): List<TransactionStatsDataProjection>

    @Query(
        "SELECT new io.lab577.xinfin.iso20022.demo.metrics.TransactionCollateralStatsDataProjection(" +
            "   tmce.type, " +         // type
            "   tmce.tokenSymbol, " +  // tokenSymbol
            "   SUM(tmce.amount)" +    // amountTotal
            ") " +
            "FROM TransactionMessageEntity AS tme " +
            "LEFT JOIN tme.transaction AS te " +
            "LEFT JOIN tme.collateral AS tmce " +
            "WHERE te.senderBic = :senderBic " +
            "   and tme.collateral IS NOT NULL " +
            "   and te.requestDate >= :startDateTime " +
            "   and te.requestDate <= :endDateTime " +
            "GROUP BY tmce.type, tmce.tokenSymbol"
    )
    fun getSentTransactionCollateralStatsDataProjections(
        @Param("senderBic") senderBic: String,
        @Param("startDateTime") startDateTime: OffsetDateTime,
        @Param("endDateTime") endDateTime: OffsetDateTime
    ): List<TransactionCollateralStatsDataProjection>

    @Query(
        "SELECT new io.lab577.xinfin.iso20022.demo.metrics.TransactionStatsDataProjection(" +
            "   COUNT(te), " +                // count
            "   SUM(te.amount), " +           // amountTotal
            "   te.currency " +               // currency
            ") " +
            "FROM TransactionEntity AS te " +
            "WHERE te.recipientBic = :recipientBic " +
            "   and te.requestDate >= :startDateTime " +
            "   and te.requestDate <= :endDateTime " +
            "GROUP BY te.currency"
    )
    fun getReceivedTransactionStatsDataProjections(
        @Param("recipientBic") recipientBic: String,
        @Param("startDateTime") startDateTime: OffsetDateTime,
        @Param("endDateTime") endDateTime: OffsetDateTime
    ): List<TransactionStatsDataProjection>

    @Query(
        "SELECT new io.lab577.xinfin.iso20022.demo.metrics.TransactionCollateralStatsDataProjection(" +
            "   tmce.type, " +         // type
            "   tmce.tokenSymbol, " +  // tokenSymbol
            "   SUM(tmce.amount)" +    // amountTotal
            ") " +
            "FROM TransactionMessageEntity AS tme " +
            "LEFT JOIN tme.transaction AS te " +
            "LEFT JOIN tme.collateral AS tmce " +
            "WHERE te.recipientBic = :recipientBic " +
            "   and tme.collateral IS NOT NULL " +
            "   and tmce.type = io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType.ERC20 " +
            "   and te.requestDate >= :startDateTime " +
            "   and te.requestDate <= :endDateTime " +
            "GROUP BY tmce.type, tmce.tokenSymbol"
    )
    fun getReceivedTransactionCollateralStatsDataProjections(
        @Param("recipientBic") senderBic: String,
        @Param("startDateTime") startDateTime: OffsetDateTime,
        @Param("endDateTime") endDateTime: OffsetDateTime
    ): List<TransactionCollateralStatsDataProjection>

    @Query(
        "SELECT COUNT(tme)" +
            "FROM TransactionMessageEntity AS tme " +
            "WHERE tme.messageDate >= :startDateTime " +
            "   and tme.messageDate <= :endDateTime "
    )
    fun countTransactionMessages(
        @Param("startDateTime") startDateTime: OffsetDateTime,
        @Param("endDateTime") endDateTime: OffsetDateTime
    ): Long
}
