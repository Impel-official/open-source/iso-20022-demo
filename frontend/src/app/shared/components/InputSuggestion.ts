import {css, html, LitElement} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';

export type Suggestion = {value: string, description: string, extra: string | null};

const styles = css`
#suggestions-input {
  width: 100%;
}
`

@customElement('input-suggestion')
export class InputSuggestion extends LitElement {
  static override styles = styles;

  @property()
  singleLineItems = true;
  @property()
  value = '';
  @property()
  placeholder = '';
  @property()
  suggestions: Array<Suggestion> = [];
  @property()
  disabled = false;
  @property()
  valueState = 'None';
  @property()
  valueStateMessage = 'Input is invalid';
  @property()
  suggestionRenderer = function (suggestion: Suggestion, singleLine = false){
    const li: any = document.createElement("ui5-suggestion-item");
    if(singleLine){
      li.text = `${suggestion.value}`;
      li.additionalText = `${suggestion.description}`;
    }
    else{
      li.description = suggestion.description;
      li.text = suggestion.value;
    }
    return li;
  }

  @query('#suggestions-input')
  public input: any;
  
  private updateSuggestions(event: any){
    const searchValue = this.input.value;
    this.value = searchValue;
    let suggestionItems: Array<Suggestion> = [];

    if (searchValue) {
      suggestionItems = this.suggestions.filter(item => {
        const foundMatchOnValue =
          item.value.toUpperCase().indexOf(searchValue.toUpperCase()) > -1;
        const foundMatchOnDescription =
          item.description.toUpperCase().indexOf(searchValue.toUpperCase()) > -1;
        return foundMatchOnValue || foundMatchOnDescription;
      });
    }

    // remove the previous suggestions
    [].slice.call(this.input.children).forEach(child => {
      this.input.removeChild(child);
    });

    // add the new suggestions according to the  user input
    suggestionItems.forEach(item => {
      this.input.appendChild(this.suggestionRenderer(item, this.singleLineItems));
    });

    const changeEvent = new CustomEvent<any>('input', event);
    this.dispatchEvent(changeEvent);
  }

  onChange(event: any){
    const changeEvent = new CustomEvent<any>('change', event);
    this.dispatchEvent(changeEvent);
  }
  
  override render() {
    return html`<ui5-input id="suggestions-input"
                           value-state=${this.valueState} 
                           show-suggestions
                           placeholder=${this.placeholder}
                           .value=${this.value}
                           @input=${this.updateSuggestions}
                           @change=${this.onChange}
                           .disabled=${this.disabled}
    ><div slot="valueStateMessage">${this.valueStateMessage}</div></ui5-input>`;
  }
}