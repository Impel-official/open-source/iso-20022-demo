package io.lab577.xinfin.iso20022.demo.integration.hubspot

import io.lab577.xinfin.iso20022.demo.integration.hubspot.config.HubSpotCacheConfig.Companion.HUBSPOT_CACHE
import io.lab577.xinfin.iso20022.demo.integration.hubspot.config.HubSpotProperties
import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.Filter
import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.FilterGroup
import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.FindContactRequestBody
import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.HubSpotContact
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.Cacheable
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service

@Service
class HubSpotClientService(
    val hubSpotClient: HubSpotClient,
    val hubSpotProperties: HubSpotProperties
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(HubSpotClientService::class.java)
    }

    @Cacheable(cacheNames = [HUBSPOT_CACHE], key = "#id", sync = true)
    @Retryable(maxAttemptsExpression = "\${app.integration.hubspot.retryMaxAttempts}")
    fun findContactByIdProperty(id: String): HubSpotContact? {
        logger.info("Finding HubSpot contact by id property '${hubSpotProperties.idPropertyName}'")
        val body = FindContactRequestBody(
            listOf(
                FilterGroup(
                    listOf(
                        Filter(hubSpotProperties.idPropertyName, "EQ", id)
                    )
                )
            )
        )
        return hubSpotClient.findContact(body, "Bearer ${hubSpotProperties.apiKey}")
            .results.firstOrNull()
    }
}

