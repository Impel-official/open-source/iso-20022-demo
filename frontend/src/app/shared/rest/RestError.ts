import {Response} from "@web/test-runner/dist/reporter/utils/request";

export class BaseError extends Error{
  public readonly isError = true;

  constructor(
      public readonly code: string,
      public override readonly message: string,
  ) {
    super(message);
  }
}

export class RestFieldError extends BaseError{
  constructor(
      public override readonly code: string,
      public override readonly message: string,
      public readonly property: string,
  ){
    super(code, message);
  }
}

export class RestError extends BaseError{
  constructor(
      public override readonly code: string,
      public override readonly message: string,
      public readonly fieldErrors?: Array<RestFieldError>,
      public readonly response?: Response,

  ){
    super(code, message);
  }
}