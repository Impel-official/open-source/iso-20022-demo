package io.lab577.xinfin.iso20022.demo.integration.iso20022

import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.api.MetricsControllerImplApi
import org.springframework.cloud.openfeign.FeignClient

@FeignClient(
    name = "iso20022Metrics",
    url = "\${app.integration.iso20022.url}",
    configuration = [Iso20022OAuthFeignConfig::class]
)
interface Iso20022MetricsClient : MetricsControllerImplApi
