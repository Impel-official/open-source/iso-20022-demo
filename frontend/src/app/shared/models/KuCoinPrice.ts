export class KuCoinPrice {

  constructor(
    public readonly code: string,
    public readonly data: any
  ) {
  }

  static fromJson(json: any): KuCoinPrice {
    // TODO: this needs json schema validation
    return new KuCoinPrice(
      json.code,
      json.data,
    )
  }
}