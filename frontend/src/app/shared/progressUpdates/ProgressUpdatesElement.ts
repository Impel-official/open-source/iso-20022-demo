import {customElement, queryAll, query, property} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {repeat} from 'lit/directives/repeat.js';
import { ProgressUpdate, ProgressUpdateType } from "./models/ProgressUpdate";
import { ProgressUpdates } from "./models/ProgressUpdates";

const FADE_DURATION_SEC = 0.5;

const styles = css`
#dialog {
  padding: 1rem;
}
.container {
  display: flex;
  flex-direction: column;
  align-items: center;
}

.progress-updates-area {
  margin-top: 2rem;
  width: 100%;
  max-width: 600px;
  min-height: 300px;
}
.progress-update-item {
  transition: all ${FADE_DURATION_SEC}s ease-out;
  opacity: 0;
}
.footer {
  display: flex;
  justify-content: center;
}
title {
  color: #6a6d70;
}
.show {
  opacity: 1;
}
`
// this is from UUI5 docs
type ListType = "Success" | "Information" | "Warning" | "Error";
type MapProgressUpdateTypeToListProperties = 
  Record<ProgressUpdateType, { listType: ListType, description: string  }>;
const map: MapProgressUpdateTypeToListProperties = {
  Success: {listType: "Success",  description: "Success"},
  Waiting: {listType: "Information",  description: "Waiting"},
  Skipped: {listType: "Warning",  description: "Skipped"},
  Error: {listType: "Error",  description: "Error"}
};

@customElement('progress-updates')
export class ProgressUpdatesElement extends LitElement {
  static override styles = styles;

  @property()
  actionTitle: String = '';
  @property()
  progressUpdates: ProgressUpdates = ProgressUpdates.empty();

  @queryAll('.progress-update-item')
  private progressUpdateItems: any;
  @query('#dialog')
  private dialog: any;
  
  override updated(){
    if(this.progressUpdateItems && this.progressUpdateItems.length == 0) return;
    setTimeout( () => {
      const progressUpdateItems = this.progressUpdateItems as Array<HTMLElement>;
      progressUpdateItems.forEach(m => m.classList.add('show'));
    })
  }
  
  show() {
    this.dialog.show();
  }
  
  hide() {
    this.dialog.close();
  }
  
  isOpen(): boolean {
    return this.dialog.isOpen();
  }
  
  renderProcessUpdateRow(processUpdate: ProgressUpdate) {
    return html`
      <ui5-li additional-text=${map[processUpdate.type].description}
              additional-text-state=${map[processUpdate.type].listType}
              type="Inactive"
              class="progress-update-item"
      ><span class="title">${processUpdate.description}</span>
      </ui5-li>
    `
  }

  override render() {
    return html`
      <ui5-dialog id="dialog" header-text=${this.actionTitle}>
        <slot name="header" slot="header"></slot>
        <div class="container">
          <div class="progress-updates-area">
            <ui5-list mode="None">
             ${repeat(
               this.progressUpdates.getProgressUpdates(),
               (it) => it.id,
               (it) => {return this.renderProcessUpdateRow(it)}
             )}
           </ui5-list>
         </div>
        </div>
        <slot name="footer" slot="footer"></slot>
      </ui5-dialog>
    `
  }
}