import {customElement, property} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {TransactionSummary} from '../../model/TransactionSummary';
import {TransactionSelectedEvent} from "./TransactionsTable";
import {colorsConfig, commonCss} from "../../../shared/config/styles";


const styles = css`
${commonCss}
`

@customElement('tx-details-link')
export class TxDetailsLink extends LitElement {
  static override styles = styles;

  @property()
  transaction: TransactionSummary | undefined;

  private showTransactionDetails(event: CustomEvent) {
    event.cancelBubble = true

    const eventOptions = {
      detail: this.transaction,
      bubbles: true,
      composed: true
    };
    const transactionSelected: TransactionSelectedEvent =
      new CustomEvent<TransactionSummary>('transactionSelected', eventOptions);
    this.dispatchEvent(transactionSelected);
  }

  override render() {
    return html`
      <ui5-link class="link" @click=${this.showTransactionDetails}>
          <ui5-icon class="link" name="sap-icon://message-information" style="margin-left: 12px"></ui5-icon>
      </ui5-link>
    `
  }
}
