package io.lab577.xinfin.iso20022.demo.config.auth

import com.fasterxml.jackson.databind.ObjectMapper
import io.lab577.xinfin.iso20022.demo.api.EventsControllerImpl.Companion.DEMO_EVENT_API
import io.lab577.xinfin.iso20022.demo.integration.hubspot.config.HubSpotProperties
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Suppress("DEPRECATION")
@Configuration
@EnableWebSecurity
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfigurer(
    val resourceServerProperties: ResourceServerProperties,
    val securityProperties: RestSecurityProperties,
    val hubSpotProperties: HubSpotProperties,
) : ResourceServerConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources.resourceId(resourceServerProperties.resourceId)
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.anonymous()
            .and().cors().configurationSource(corsConfigurationSource())
            .and().headers().frameOptions().disable()
            .and().csrf().disable().formLogin().disable()
            .let {
                when {
                    hubSpotProperties.enabled -> it.httpBasic()
                }
                it
            }
            .authorizeRequests().antMatchers(DEMO_EVENT_API).let {
                when {
                    securityProperties.enabled -> it.authenticated()
                    else -> it.permitAll()
                }
            }
            .and().authorizeRequests().antMatchers(securityProperties.apiMatcher).let {
                when {
                    securityProperties.enabled -> it.authenticated()
                    else -> it.permitAll()
                }
            }
            .and().authorizeRequests().anyRequest().permitAll()
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", securityProperties.getCorsConfiguration())
        return source
    }

    @Bean
    fun jwtAccessTokenCustomizer(mapper: ObjectMapper?): JwtAccessTokenCustomizer {
        return JwtAccessTokenCustomizer(mapper!!)
    }
}
