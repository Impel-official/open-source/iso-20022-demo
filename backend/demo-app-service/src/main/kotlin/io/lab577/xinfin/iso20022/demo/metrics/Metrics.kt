package io.lab577.xinfin.iso20022.demo.metrics

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.NetworkAccountDto
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.ProofMetricsDto
import io.lab577.xinfin.iso20022.demo.utils.NoArg
import java.math.BigDecimal

class TransactionAllMetrics(
    val sentTransactionStats: TransactionStats,
    val receivedTransactionStats: TransactionStats,
    val transactionMessagesStats: TransactionMessageStats,
    val proofMetrics: ProofMetricsDto?,
    val networkAccount: NetworkAccountDto?
)

data class TransactionStats(
    val last24Hours: TransactionStatsData,
    val mtd: TransactionStatsData,
    val ytd: TransactionStatsData,
    val all: TransactionStatsData
)

@NoArg
data class TransactionStatsDataProjection(
    val count: Long?,
    val amountTotal: BigDecimal?,
    val currency: String?
)

@NoArg
data class TransactionCollateralStatsDataProjection(
    val type: TransactionMessageCollateralType?,
    val tokenSymbol: String?,
    val amountTotal: BigDecimal?
)

data class TransactionStatsData(
    val count: Long,
    val moneyTotal: Map<String, BigDecimal>,
    val nativeCollateralAmountTotal: BigDecimal,
    val erc20CollateralTotal: Map<String, BigDecimal>
) {
    companion object {
        fun mapFromProjections(
            projections: List<TransactionStatsDataProjection>?,
            collateralProjections: List<TransactionCollateralStatsDataProjection>?
        ): TransactionStatsData {
            return TransactionStatsData(
                projections?.sumOf { it.count ?: 0 } ?: 0,
                projections?.associateBy({ it.currency ?: "" }, { it.amountTotal ?: BigDecimal.ZERO }) ?: emptyMap(),
                collateralProjections
                    ?.filter { it.type == TransactionMessageCollateralType.NATIVE }
                    ?.sumOf { it.amountTotal ?: BigDecimal.ZERO } ?: BigDecimal.ZERO,
                collateralProjections
                    ?.filter { it.type == TransactionMessageCollateralType.ERC20 }
                    ?.associateBy({ it.tokenSymbol ?: "" }, { it.amountTotal ?: BigDecimal.ZERO })
                    ?: emptyMap()
            )
        }
    }
}

data class TransactionMessageStats(
    val last24Hours: TransactionMessageStatsData,
    val mtd: TransactionMessageStatsData,
    val ytd: TransactionMessageStatsData,
    val all: TransactionMessageStatsData
)

data class TransactionMessageStatsData(
    val count: Long
)
