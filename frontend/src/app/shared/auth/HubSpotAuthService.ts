import {AppConfig} from "../config";
import {User} from "../user/User";
import {UserStore} from "../user/UserStore";
import {AuthService} from "./AuthService";
import {NavigateRequestEvent} from "../routing/RoutingService";
import {RestAdapter} from "../rest/RestAdapter";

export class HubSpotAuthService implements AuthService {

    private HUB_SPOT_ID_PARAM = 'xcx';
    private HUB_SPOT_ID_KEY = 'hubSpotXcx';

    constructor(
        private config: AppConfig,
        private restAdapter: RestAdapter,
        private userStore: UserStore,
        private localStorage: Storage
    ) {
    }

    async init() {
        const queryParams = Object.fromEntries(new URLSearchParams(window.location.search).entries());
        const idParamValue = queryParams[this.HUB_SPOT_ID_PARAM];
        if (idParamValue) {
            this.localStorage.setItem(this.HUB_SPOT_ID_KEY, idParamValue as string)
            window.history.pushState({}, document.title, window.location.pathname);
            this.userStore.setUser(undefined);
        }

        const id = this.localStorage.getItem(this.HUB_SPOT_ID_KEY);
        if (id) {
            if (!this.restAdapter.areRequestsAuthenticated()) {
                this.restAdapter.authenticateHttpRequests(this.getToken(id));
            }
            const user = await this.getHubSpotUser(id);
            if (user) {
                this.userStore.setUser(user);
                !window.dispatchEvent(new CustomEvent('user-authenticated', {detail: user}));
            }
        }
    }

    private getToken(id: string): string {
        const basicCredentials = window.btoa(`user:${id}`)
        return `Basic ${basicCredentials}`
    }

    private async getHubSpotUser(id: string): Promise<User | undefined> {
        try {
            console.log("Getting HubSpot contact");
            const hubSpotContact = await this.restAdapter.getHubSpotContact(id);
            console.log(`Received HubSpot contact`, hubSpotContact);
            const username = this.config.branding.orgName.toUpperCase()
            return new User(hubSpotContact.id, username, "HUBSPOT", hubSpotContact.email);
        } catch (error) {
            console.log('Error occurred when getting HubSpot contact');
            console.log(error);
            return undefined;
        }
    }

    isAuthenticated(): boolean {
        return this.userStore.hasUser();
    }

    redirectToLoginPage(redirectPath: string) {
        console.log("You are not authorized");
        const navigationRequestEvent: NavigateRequestEvent = {name: 'unauthorized'}
        !window.dispatchEvent(new CustomEvent('navigate-request', {detail: navigationRequestEvent}));
    }

    getUser() {
        if (!this.userStore.hasUser()) {
            throw 'No user is set';
        }
        return this.userStore.user!;
    }

    async authenticate() {
        // Not implemented for HubSpot
    }

    logout() {
        this.userStore.setUser(undefined);
    }
}
