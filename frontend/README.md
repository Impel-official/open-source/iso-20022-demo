# DEMO ISO 20022 UI

## Generate API services and models
In order to generate API services and models from OpenApi use following command
```bash
npm run generate-api
```
OR
```bash
./openapi-generator/codegen.sh
```

Script will generate files to [src/app/shared/api](src/app/shared/api) using config file [openapi-generator/config.json](openapi-generator/config.json).
By default the script will get OpenAPI definition from `http://localhost:8095/v3/api-docs`, which means that local instance of Gateway has to be running.
We can change OpenAPI URL by changing API_DOCS variable in the script.
