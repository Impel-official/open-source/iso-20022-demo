package io.lab577.xinfin.iso20022.demo.api.dto

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import java.math.BigDecimal

data class TransactionMessageCollateralDto(
    val type: TransactionMessageCollateralType,
    val amount: BigDecimal,
    val exchangePrice: BigDecimal?,
    val exchangeCurrency: String?,
    val tokenSymbol: String? = null,
) {
    override fun toString(): String {
        return "TransactionMessageCollateralDto(" +
            "type=$type, " +
            "amount=$amount, " +
            "exchangePrice=$exchangePrice, " +
            "exchangeCurrency=$exchangeCurrency, " +
            "tokenSymbol=$tokenSymbol" +
            ")"
    }
}
