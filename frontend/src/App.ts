import {css, html, LitElement} from 'lit';
import {customElement, state} from 'lit/decorators.js';
import {AppConfig, config} from "./app/shared/config";
import {Router} from "@vaadin/router";
import {RouteName} from "./app/shared/routing/routeConfig";

const styles = css`
.container {
  margin: 16px 8px;
}
user-messages {
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 999;
  box-sizing: border-box;
  width: 100%;
  padding: 0 16px;
}
`;

@customElement('app-root')
export class App extends LitElement {
  static override styles = styles;

  private config: AppConfig;

  @state()
  shareDemoExperienceVisible = false;

  constructor() {
    super();

    this.config = config;

    window.addEventListener('location-changed', (event: CustomEvent<Router.Location>) => {
      const routeName = event.detail.route?.name as RouteName
      this.shareDemoExperienceVisible = !(routeName === 'unauthorized')
    });
  }

  override render() {
    const shareDemoExperiencePanel = (this.config.shareDemoExperience && this.shareDemoExperienceVisible)
        ? html`
                <share-demo-experience-panel></share-demo-experience-panel>`
        : '';

    return html`
        <slot name="nav-bar"></slot>
        <user-messages></user-messages>
        <div class="container">
            <slot name="main"></slot>
            ${shareDemoExperiencePanel}
        </div>
        <slot name="footer-bar"></slot>
    `;
  }
}
