import {css, html, LitElement} from 'lit';
import {customElement, property, query} from "lit/decorators.js";
import {User} from "./User";
import {styleMap} from 'lit/directives/style-map.js';
import {commonCss, typography} from "../config/styles";
import {AppConfig, config} from '../config';

const styles = css`
${commonCss}
${typography}
.profile {
  position: relative;
  display: flex;
}

#button, #button:link {
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
  border: 2px solid #005DF2;
  color: #005DF2;
  background: #FFF;
  padding: 8px 32px;
}

.profile a {
  display: flex;
  align-items: center;
}

#username {
  margin-right: 8px;
}

#button ui5-icon{
  color: #005DF2;
}

#button:hover,
#button:hover *,
#button:hover ui5-icon {
  color: #FFF;
  background: #005DF2;
}
`;

export type MenuItemName = 'Logout' | 'Settings';
export type MenuItemSelectedEvent = CustomEvent<MenuItemName>;

@customElement('user-avatar')
export class UserAvatarElement extends LitElement {
  static override styles = styles;

  @property()
  user?: User;

  @query('#button')
  button: any;
  @query('#popover')
  popover: any;

  private config: AppConfig;
  private internalUser: User;

  constructor() {
    // we should use DI for that
    super();
    this.config = config;
    this.internalUser = User.createUnauthenticatedUser();
  }

  private toggleNotificationsList() {
    if(!this.internalUser.isAuthenticated()) return;
    this.popover.isOpen() ?
      this.popover.close() :
      this.popover.showAt(this.button);

    this.popover.focus();
  }

  private menuItemSelected(name: MenuItemName) {
    const eventOptions = {
      detail: name,
      bubbles: true,
      composed: true
    };
    const menuItemSelectedEvent: MenuItemSelectedEvent =
      new CustomEvent<MenuItemName>(
        'menuItemSelectedEvent', eventOptions
      );
    this.dispatchEvent(menuItemSelectedEvent);
  }

  override render() {
    // always show a user, even if there is no authentication
    this.internalUser = this.user !== undefined ? this.user : this.internalUser;
    const logoutStyles = {display: this.internalUser.shouldHideLogout()? 'none' : 'block'};


    return html`
      <div class="profile">
        <a id="button"
           icon="employee"
           design="Transparent"
           @click=${this.toggleNotificationsList}
        >
          <span id="username" class="profile-button-text">User: ${this.internalUser.username}</span>
          <ui5-icon name="action-settings"></ui5-icon>
        </a>
      </div>

      <ui5-popover id="popover"
                   placement-type="Bottom"
                   horizontal-align="Center"
      >
        <ui5-list>
            <ui5-li @click=${() => this.menuItemSelected('Settings')}>Settings</ui5-li>
            <ui5-li @click=${() => this.menuItemSelected('Logout')}
                    style=${styleMap(logoutStyles)}
            >
              Logout
            </ui5-li>
        </ui5-list>
      </ui5-popover>
    `
  }
}
