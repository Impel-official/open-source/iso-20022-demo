import {html, LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {AppConfig, config} from "../config";

@customElement('characters-remaining')
export class CharactersRemainingElement extends LitElement {
    @property()
    max = 500;
    @property()
    count = 0;

    private config: AppConfig;
    
    constructor() {
        super();
        this.config = config;
    }

    override render() {
        return html`
            <div class="ui5-textarea-exceeded-text" style="color: #6a6d70">
                ${this.max - this.count} characters left
            </div>
        `;
    }
}
