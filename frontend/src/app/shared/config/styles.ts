import {css} from "lit";

export const colorsConfig = {
  link: css`#005DF2`,
  linkDark: css`#002C72`,
  linkActive: css`#54CCCC`,
  linkDisabled: css`#C0BFBFFF`,
  tableBorder: css`#D9ECFF`,
  th: css`#002C72`,
  thBg: css`#D9ECFF`,
  trAccentBg: css`#F0F8FF`, // approximate colour -- can't access the exact value on Figma
  trBg: css`#FFF`,
  trAltBg: css`#F4FAFF`,
  td: css`#002C72`,
  card: css`#002C72`,
  cardBg: css`#F5FAFF`,
  label: css`#002C72`,
  panelBg: css`#F4FAFF`,
  error: css`#CD0120`,
  tableBorderFirstColumn: css`#FFFFFF`
}

export const fonts = {
  font1: css`'Graphie'`,
  font2: css`'Noto Sans'`,
}

export const fontWeight = {
  book: css`400`,
  regular: css`350`,
  light: css`300`,
}

export const typography = css`
.h1 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 60px;
  line-height: 70px;
  letter-spacing: -1vw;
  text-transform: none;
  color: #002C72;
}
.h2 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 55px;
  line-height: 65px;
  letter-spacing: -0.5vw;
  text-transform: none;
  color: #002C72;
}
.h3 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 45px;
  line-height: 55px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.h4 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 35px;
  line-height: 45px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.h5 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 28px;
  line-height: 38px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.h6 {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.regular};
  font-size: 22px;
  line-height: 32px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.subtitle {
  font-family: 'Noto Sans', sans-serif!important;
  font-weight: ${fontWeight.light};
  font-size: 28px;
  line-height: 30px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.body1 {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.light};
  font-size: 18px;
  line-height: 28px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.body2 {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.light};
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.button-text {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.regular};
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.small-link {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.regular};
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0;
  text-transform: none;
  color: #002C72;
}
.overline {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.regular};
  font-size: 16px;
  line-height: 100%;
  letter-spacing: 5vw;
  text-transform: uppercase;
  color: #002C72;
}
/* these do not exist as typography styles but exists on pages*/
.menu-item-text {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 18px;
  line-height: 100%;
  letter-spacing: 0.05em;
  text-transform: none;
  color: #002C72;
}
.profile-button-text {
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 18px;
  line-height: 100%;
  letter-spacing: 0.05em;
  text-transform: none;
  color: #005DF2;
}
.reporting-table-label {
  font-family: ${fonts.font2}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 14px;
  line-height: 30px;
  text-align: right;
  color: #002C72;
}
`

export const commonCss = css`
.flex {
  display: flex;
}

.flex-content-space-between {
  justify-content: space-between;
}

.flex-content-end {
  justify-content: flex-end;
}

.flex-row {
  flex-direction: row;
}

.justify-self-start {
  justify-self: flex-start;
}

.justify-self-center {
  justify-self: center;
}

.justify-self-end {
  justify-self: flex-end;
}

.row {
   display: flex;
   margin-bottom: 1em;
   overflow-x: hidden;
}
.column {
  display: flex;
  flex-direction: column;
  margin-bottom: 1em;
}
.hidden {
  display: none !important;
}
a {
  cursor: pointer;
}
ui5-icon.link,
ui5-icon.link:hover,
a.link,
a.link:any-link,
a.link:visited
{
  font-family: 'Graphie', sans-serif!important;
  font-style: normal;
  color: ${colorsConfig.link};
}
a.link.enabled {
  color: ${colorsConfig.link};
}
a.link.disabled {
  color: ${colorsConfig.linkDisabled};
  pointer-events: none;
  cursor: default;
}
.link-plain {
  color: ${colorsConfig.linkDark};
  text-decoration: none;
}


ui5-icon.link:hover,
a.link.enabled:hover
{
  font-family: 'Graphie', sans-serif!important;
  font-style: normal;
  color: ${colorsConfig.linkActive};
}

ui5-button{
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 100%;
  letter-spacing: 0.04em;
}
.text-danger {
  color: ${colorsConfig.error}
}
.bold {
  font-weight: 700;
}

.text-center {
  text-align: center;
}

.mt-1 {
  margin-top: 1rem
}
.mt-2 {
  margin-top: 2rem
}

.mb-1 {
  margin-bottom: 1rem
}
.mb-2 {
  margin-bottom: 2rem
}

.code {
  word-break: break-word;
  white-space: pre-wrap;
}
`

export const formCss = css`
.label, ui5-label {
  min-width: 10em;
  min-height: 1rem;
  padding-right: 1em;
  margin: 8px 6px 3px 0;
  display: flex;
  font-style: normal;
  font-weight: 300;
  font-size: 18px;
  line-height: 30px;
  color: ${colorsConfig.label};
}
ui5-input, input-suggestion, ui5-textarea {
  background-color: #FFF;
  width: 100%;
  border-top-color: white !important;
  border-right-color: white !important;
  border-left-color: white !important;
  border-bottom-color: ${colorsConfig.link};
  border-bottom: 1px solid ${colorsConfig.link};
  border-radius: 0!important;
}
ui5-input.out-of-card,
input-suggestion.out-of-card,
ui5-textarea.out-of-card{
  background-color: ${colorsConfig.cardBg};
}
`

export const tableCss = css`
.positive { color: rgb(9, 222, 9); }
.negative { color: rgb(214, 39, 85); }
.actionable { color: rgb(179, 177, 46); }

.tables-container{
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
}

ui5-table, .ui5-table, #table {
  color: ${colorsConfig.td};
  border: 1px solid ${colorsConfig.tableBorder} !important;
  border-radius: 5px;
}

ui5-table, .ui5-table, table,
ui5-table-header-row,
.ui5-table-header-row,
ui5-table-row::part(row), tr,
ui5-table-cell, td {
  color: ${colorsConfig.td};
  border: 0;
}
ui5-table td,
ui5-table ui5-table-cell{
  height:2.75rem;
}

ui5-table-cell, td{
  font-family:'Noto Sans', Arial, Helvetica, sans-serif !important;
  font-style: normal;
  font-weight: 300;
  font-size: 12px;
  line-height: 20px;
  /* identical to box height, or 214% */
  text-align: center;
}

ui5-table-cell:not(:last-of-type),
td:not(:last-of-type) {
  border-right: 1px solid ${colorsConfig.tableBorder};
}
ui5-table-row:nth-of-type(even)::part(row),
tr:nth-of-type(even){
  background: ${colorsConfig.trAltBg};
}

ui5-table-row:nth-of-type(odd)::part(row),
tr:nth-of-type(odd){
  background: ${colorsConfig.trBg};
}
ui5-table-column::part(column),
ui5-table-column::part(column), th{
  background: ${colorsConfig.thBg};
  font-family:'Noto Sans', Arial, Helvetica, sans-serif !important;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 30px;
  text-align: center;
  color: ${colorsConfig.th};
}

tr:nth-child(even) .vertical-th,
ui5-table-row:nth-child(even) .vertical-th{
  background: #ECF6FF;
}
tr:nth-child(odd) .vertical-th,
ui5-table-row:nth-child(odd) .vertical-th{
  background: #E7F3FF;
}
.vertical-th{
  text-align:right;
}


.paging-area {
   display: flex;
   flex-direction: row;
   margin-top: 0.5em;
   margin-bottom: 2em;
   justify-content: flex-end;
}
.paging-area .per-page {
   margin-left: 2em;
}
ui5-table-column span {
  "line-height: 1.4rem
}
span .ui5-table-row-popin-title {
  font-weight: bold;
}
.amount {
  text-align: end;
}
.amount span {
  margin-right: 1rem;
}

div.inline-row {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr) ) ;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-content: stretch;
}
.inline-col{
  display: flex;
  flex-direction: row;
  align-self: stretch;
  margin-right: 4px;
}
.inline-col-label{
  margin-right: 4px;
  font-weight: bold;
}

div.wrap{
  max-width: 150px;
  overflow-wrap: break-word;
  word-break: break-all;
}

.text-right {
  text-align: right;
}

.w-100 {
  width: 100%;
}
`

export const kvTableCss = css`
.kv-table {
  width: 100%;
  word-break: break-word;
  border-collapse: collapse;
}
.kv-table tr, .kv-table td {
  color: #002C72;
  border-collapse: collapse;
  border:  1px solid #D9ECFF;
}
.kv-table td {
  height:2.75rem;
}
.kv-table tr:not(:last-child) td{
  border-bottom:  1px solid #FFF;
}
.kv-table tr:nth-child(odd){
  background: #F4FAFF;
}
.kv-table tr:nth-child(even){
  background: #FFF;
}
.kv-table td.label{
  background: #D9ECFF;
  text-align: right;
  font-weight: 400;
  font-size: 14px;
  line-height: 30px;
  padding-right: 20px;
  width:40%;
}
.kv-table td.value{
  font-weight: 300;
  font-size: 12px;
  line-height: 20px;
  padding-left: 20px;
}
`

export const cardCss = css`
.title,
.ui5-card-header,
ui5-card-header,
ui5-card-header::part(title){
  /* Desktop H4 */
  font-family: ${fonts.font1}, sans-serif!important;
  font-weight: ${fontWeight.book};
  font-size: 22px;
  line-height: 32px;
  color: ${colorsConfig.card};
  text-align: left;
  width: 100%;
  margin-bottom: 50px;
}

.cards-container{
  display: flex;
  gap: 80px 60px; /* row-gap column gap */
  flex-flow: row wrap;
  /*
  align-content: center;
  justify-content: space-between;
  align-content: space-between;
  */

}

.cards-container ui5-card,
.cards-container div.card{
  display: flex;
}
.card-center{
    max-width: 749px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: auto;
}

ui5-card-header.secondary::part(title){
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  color: ${colorsConfig.card};
}
.card-content {
    margin: 1em;
}

ui5-card, div.card{
  display: flex;
  justify-content: center;
  border-radius: 5px;
}

div.card{
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 1rem;
  align-items: center;
  justify-content: center;
  background-color: ${colorsConfig.cardBg};
  width: 100%;
  box-sizing: border-box;
}

ui5-card.small, .card.small{
  width: 600px;
}
div.card.light,
ui5-card.light,
ui5-card.light ui5-card-header{
  background-color: #FFF;
}

div.amount,
div.non-amount{
  font-weight: bold;
}
.cards {
  margin: 0 auto;
  display: grid;
  grid-gap: 1rem;
  margin-top: 2rem;
}

@media (min-width: 600px) {
  .cards { grid-template-columns: repeat(2, 1fr); }
}
@media (min-width: 900px) {
  .cards { grid-template-columns: repeat(3, 1fr); }
}

/* Desktop menu */
@media all and (min-width: 960px) {
  div.card {
    width: auto;
    padding-top: 40px;
    padding-bottom: 60px;
  }

  div.form {
    max-width: 50.5rem;
    padding-left: 6rem;
    padding-right: 6rem;
  }

  .title,
  .ui5-card-header,
  ui5-card-header,
  ui5-card-header::part(title){
    font-size: 35px;
    line-height: 45px;
  }
}
`

export const mobileCss = css`
@media (max-width: 600px) {

  .mobile-hidden {
    display: none;
  }

  .mobile-justify-self-start {
    justify-self: flex-start;
  }

  .mobile-flex-grow {
    flex-grow: 1;
  }

  .mobile-flex-one-item-per-row {
    flex-basis: 100%;
  }

  .mobile-flex-wrap {
    flex-wrap: wrap;
  }

}
`
