package io.lab577.xinfin.iso20022.demo.service

import io.github.perplexhub.rsql.RSQLJPASupport
import io.github.perplexhub.rsql.RSQLJPASupport.toSpecification
import io.lab577.xinfin.iso20022.demo.domain.repository.BaseRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityNotFoundException

abstract class BaseService<T : Any, PK, R : BaseRepository<T, PK>>(
    val repository: R
) {

    /**
     * Find resources page-by-page using a JPA Specification
     *
     * @param pageRequest page request
     * @return resources
     */
    open fun findPagedByFilter(
        filter: String,
        sortBy: String,
        sortDirection: Sort.Direction,
        pageNumber: Int,
        pageSize: Int
    ): Page<T> =
        if (filter.isBlank()) findPaginated(PageRequest.of(pageNumber, pageSize, Sort.by(sortDirection, sortBy)))
        else findPaginated(
            toSpecification<T>(filter).and(RSQLJPASupport.toSort("$sortBy,${sortDirection.toString().lowercase()}")),
            pageNumber, pageSize
        )

    /**
     * Find resources page-by-page using a JPA Specification
     *
     * @param pageRequest page request
     * @return resources
     */
    fun findPaginated(
        specification: Specification<T>,
        pageNumber: Int = 0,
        pagesize: Int = 10
    ): Page<T> {
        return repository.findAll(specification, PageRequest.of(pageNumber, pagesize))
    }

    /**
     * Find resources page-by-page using a JPA Specification
     *
     * @param pageRequest page request
     * @return resources
     */
    open fun count(filter: String): Long = if (filter.isBlank()) count() else findCount(toSpecification(filter))

    /**
     * Find resources page-by-page using a JPA Specification
     *
     * @param pageRequest page request
     * @return resources
     */
    fun findCount(specification: Specification<T>): Long = repository.count(specification)

    /**
     * Find resources page-by-page
     *
     * @param pageRequest page request
     * @return resources
     */
    open fun findPaginated(
        pageRequest: Pageable = Pageable.ofSize(10).first()
    ): Page<T> {
        return repository.findAll(pageRequest)
    }

    /**
     * Saves a given entity. Use the returned instance for further operations
     * as the save operation might have changed the entity instance completely.
     *
     * @param resource EntityModel to create
     * @return new resource
     */
    @Transactional(readOnly = false)
    open fun save(resource: T): T {
        val persisted = repository.save(resource)
        // TODO: move to domain events
        postSave(persisted)
        return persisted
    }

    /**
     * Saves all given entities.
     *
     * @param resource EntityModel to create
     * @return new resource
     */
    @Transactional(readOnly = false)
    open fun saveAll(resources: Iterable<T>): Iterable<T> {
        val persisted = repository.saveAll(resources)
        // TODO: move to domain events
        persisted.forEach { postSave(it) }
        return persisted
    }

    /**
     * Override to synchronously handle updates,
     * without going through domain events
     */
    open fun postSave(resource: T) {
        // NO-OP
    }

    /**
     * Delete an existing resource.
     *
     * @param resource EntityModel to delete
     */
    @Transactional(readOnly = false)
    open fun delete(resource: T) {
        repository.delete(resource)
    }

    /**
     * Delete existing resources by id.
     *
     * @param ids identity of resources to delete
     */
    @Transactional(readOnly = false)
    open fun deleteAllById(ids: Iterable<PK>) {
        repository.deleteAllById(ids)
    }

    /**
     * Find resource by id.
     *
     * @param id EntityModel id
     * @return resource
     */
    open fun findById(id: PK): T? {
        return repository.findByIdOrNull(id)
    }

    /**
     * Get resource by id.
     *
     * @param id EntityModel id
     * @return resource
     */
    open fun getById(id: PK): T {
        return findById(id) ?: throw EntityNotFoundException("Entity $id not found")
    }

    /**
     * Find resources by their ids.
     *
     * @param ids EntityModel ids
     * @return a list of retrieved resources, empty if no resource found
     */
    open fun findByIds(ids: Set<PK>): List<T> {
        return repository.findAllById(ids).toList()
    }

    /**
     * Find all resources.
     *
     * @return a list of all resources.
     */
    open fun findAll(): List<T> {
        return repository.findAll().toList()
    }

    /**
     * Count all resources.
     *
     * @return number of resources
     */
    open fun count(): Long {
        return repository.count()
    }

}
