package io.lab577.xinfin.iso20022.demo.integration.config

import org.slf4j.LoggerFactory
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.registration.ClientRegistration
import java.util.*

class OAuthClientCredentialsFeignManager(
    private val manager: OAuth2AuthorizedClientManager,
    private val clientRegistration: ClientRegistration
) {

    companion object {
        private val logger = LoggerFactory.getLogger(OAuthClientCredentialsFeignManager::class.java)
    }

    private val principal: Authentication

    init {
        principal = createPrincipal()
    }

    private fun createPrincipal(): Authentication {
        return object : Authentication {
            override fun getAuthorities(): Collection<GrantedAuthority> {
                return emptySet()
            }

            override fun getCredentials(): Any? {
                return null
            }

            override fun getDetails(): Any? {
                return null
            }

            override fun getPrincipal(): Any {
                return this
            }

            override fun isAuthenticated(): Boolean {
                return false
            }

            @Throws(IllegalArgumentException::class)
            override fun setAuthenticated(isAuthenticated: Boolean) {
            }

            override fun getName(): String {
                return clientRegistration.clientId
            }
        }
    }

    fun getAccessToken(): String? {
        try {
            val oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
                .withClientRegistrationId(clientRegistration.registrationId)
                .principal(principal)
                .build()
            val client = manager.authorize(oAuth2AuthorizeRequest)
            check(!Objects.isNull(client)) { "client credentials flow on " + clientRegistration.registrationId + " failed, client is null" }
            return client!!.accessToken.tokenValue
        } catch (exp: Exception) {
            logger.error("client credentials error " + exp.message)
        }
        return null
    }
}
