import copy from 'rollup-plugin-copy';
import {terser} from 'rollup-plugin-terser';
import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import rollupTypescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import commonjs from "@rollup/plugin-commonjs";
import del from 'rollup-plugin-delete'

// ${bundleName}.config.js
// ${bundleName} == ${env}-${bicCode} on infra
// eg dev-BBBBCAXXXXX
const bundleName = process.env.BUNDLE_NAME
const urlPathName = process.env.URL_PATH;
const pathNameFolder = urlPathName ? `/${urlPathName}` : '';

const rootPath = `dist/${bundleName}`;
const bundlePath = `dist/${bundleName}${pathNameFolder}`;

const isVersioned = !!process.env.VERSIONED;
const randomString = 
  isVersioned ? '-' + Math.random().toString(36).substring(2,7) : "";
const bundleFileName = `bundle${randomString}.js`;

export default {
  input: 'src/index.ts',
  output: {
    file: `${bundlePath}/${bundleFileName}`,
    format: 'esm',
    sourcemap: 'inline',
    inlineDynamicImports: true
  },
  onwarn(warning) {
    if (warning.code !== 'THIS_IS_UNDEFINED') {
      console.error(`(!) ${warning.message}`);
    }
  },
  plugins: [
    isVersioned ? del({ targets: `${bundlePath}/bundle*.js` }) : undefined,
    copy({
      targets: [
        { 
          src: 'src/index.html',
          dest: bundlePath,
          transform: (contents) => {
            return contents.toString()
              .replace(/BUNDLE_PREFIX/g, pathNameFolder)
              .replace(/VERSION/g, randomString);
          },
        },
        {
          src: 'src/robots.txt',
          dest: bundlePath
        },
        {
          src: 'LICENSE',
          dest: bundlePath
        },
        ...(urlPathName ? [
            {
              src: 'src/index.html',
              dest: rootPath,
              transform: (contents) => {
                return contents.toString()
                  .replace(/BUNDLE_PREFIX/g, pathNameFolder)
                  .replace(/VERSION/g, randomString);
              },
            },
            { 
              src: 'src/robots.txt',
              dest: rootPath
            },
            {
              src: 'LICENSE',
              dest: rootPath
            }
          ] : []
        ),
        { 
          src: `config/${bundleName}.config.js`,
          dest: bundlePath,
          rename: 'config.js'
        },
        { src: `assets`, dest: bundlePath },
        { 
          src: 
            `node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js`,
          dest: `${bundlePath}/node_modules/@webcomponents/webcomponentsjs`
        },
        { 
          src: `node_modules/lit/polyfill-support.js`,
          dest: `${bundlePath}/node_modules/lit`
        }
      ]
    }),
    commonjs(),
    rollupTypescript(),
    replace({'Reflect.decorate': 'undefined'}),
    resolve(),
    terser({
      ecma: 2017,
      module: true,
      warnings: true,
      mangle: {
        properties: {
          regex: /^__/,
        },
      },
    }),
    json(),
  ],
};
