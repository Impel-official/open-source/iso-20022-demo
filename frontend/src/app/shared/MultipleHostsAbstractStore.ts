import {ReactiveController, ReactiveControllerHost} from 'lit';
import {AppConfig} from './config';

export abstract class MultipleHostsAbstractStore implements ReactiveController {
  protected hosts: Array<ReactiveControllerHost> = [];

  constructor(protected config: AppConfig) {}
  
  bindComponentToPropertyChanges(host: ReactiveControllerHost) {
    host.addController(this);
    this.hosts.push(host);
  }
  
  unbindComponentFromPropertyChanges(host: ReactiveControllerHost) {
    this.hosts = this.hosts.filter(it => it != host);
  }
  
  /*
   * By default lit updates bound properties when reference changes. 
   * Use this function when you mutate o property and you want to notify components.
   */
  updatePropertiesToBoundHosts(){
    this.hosts.forEach(it=>it.requestUpdate())
  }

  hostConnected(): void {}
  hostDisconnected(): void {}
  hostUpdate(): void {}
  hostUpdated(): void {}
}
