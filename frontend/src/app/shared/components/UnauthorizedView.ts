import {css, html, LitElement} from 'lit';
import {customElement} from 'lit/decorators.js';
import {AppConfig, config} from "../config";
import {cardCss, commonCss} from "../config/styles";

const styles = css`
${commonCss}
${cardCss}
#title {
  font-size: 1.8rem;
  line-height: 2.5rem;
  font-weight: 300;
  margin-top: 0;
  margin-bottom: 1.8rem;
}
#error-icon {
    width: 1.25rem;
    height: 1.25rem;
}
.description {
  font-size: 1.25rem;
  line-height: 1.8rem;
  font-weight: 300;
}
#description-text1 {
  margin-bottom: 2.4rem;
}
#description-text2 {
  margin-bottom: 2.4rem;
}
#visit-button {
  margin-bottom: 2.4rem;
  width: 12rem;
  height: 4rem;
}

/* Desktop menu */
@media all and (min-width: 960px) {
  #unauthorized-container {
    margin-right: 8rem;
    padding-top: 1rem;
    background: transparent;
  }
  #title {
    font-size: 2rem;
    line-height: 2.5rem;
  }
  #error-icon {
    width: 1.5rem;
    height: 1.5rem;
  }
  .description {
    font-size: 1.25rem;
    line-height: 1.75rem;
  }
}
`;

@customElement('unauthorized-view')
export class UnauthorizedView extends LitElement {
  static override styles = styles;

  private config: AppConfig;

  constructor() {
    super();
    this.config = config;
  }

  visitImpelPage() {
    window.open("https://www.impel.global", "_blank");
  }

  override render() {
    return html`
        <div id="unauthorized-container" class="card light">
            <div id="title" class="title">
                <ui5-icon id="error-icon" name="error" class="text-danger"></ui5-icon>
                Impel's financial messaging demo authorization <span class="text-danger">link failed.</span>
            </div>
            <div class="column">
                <div>
                    <div id="description-text1" class="description">
                        If you have not registered for demo access, visit
                        <a class="link-plain bold" href="https://www.impel.global" target="_blank">impel.global</a>
                        and click ISO 20022 demo button to gain access.
                    </div>
                    <ui5-button id="visit-button"
                                design="Emphasized"
                                @click=${() => this.visitImpelPage()}>
                        Visit impel.global
                    </ui5-button>
                    <div id="description-text2" class="description">
                        If you have registered, then use the link provided in the demo access
                        email. If you cannot locate the email, then resubmit the demo form.
                    </div>
                </div>
            </div>
        </div>
    `;
  }
}
