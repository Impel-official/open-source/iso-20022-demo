import {colorsConfig} from "./styles";

export class StyleHelper {

  maybeStyleFormFields(formFields: HTMLCollection) {
    for (const field of formFields) {
      const error = ('error' == `${field.getAttribute("value-state")}`.toLowerCase());
      this.styleFormField(field, false, error);
    }
  }

  styleFormField(field: Element, appliedOuter: boolean, error: boolean) {
    this.applyFormFieldStyle(field, appliedOuter, error);
    const tagNames = ['input', 'textarea', 'div.ui5-textarea-root', 'div.ui5-input-focusable-element', 'ui5-input', 'ui5-icon'];
    const tagPaths = ['div', 'div div'];
    tagPaths.forEach((path: any) => {
      tagNames.forEach((tagName: any) => {
        const innerFields = field.shadowRoot?.querySelectorAll(`${path} ${tagName}`) || [];
        for (const innerField of innerFields) {
          //this.bordersRemoved = true;
          if (tagName == 'ui5-icon') {
            this.applyIconStyle(innerField);
            continue;
          }
          if (tagName == 'ui5-input') this.styleFormField(innerField, true, error);
          this.applyFormFieldStyle(innerField, true, error);
        }

      });
    });
  }

  async applyIconStyle(field: Element) {
    const htmlElem = field as HTMLElement
    htmlElem.style.color = `${colorsConfig.link}`;
  }

  applyFormFieldStyle(field: Element, appliedOuter: boolean, error: boolean) {
    const tagName = field.tagName.toLowerCase()
    const htmlElem = field as HTMLElement
    if (field.classList.contains('out-of-card'))
      htmlElem.style.backgroundColor = `${colorsConfig.cardBg}`;
    else
      htmlElem.style.backgroundColor = '#FFF';

    htmlElem.style.borderTopColor = '#FFF';
    htmlElem.style.borderRightColor = '#FFF';
    htmlElem.style.borderLeftColor = '#FFF';
    if (appliedOuter || (!tagName.startsWith('ui5') && !tagName.startsWith('input-suggestion'))) htmlElem.style.borderBottomColor = '#FFF';
    else {
      if (error) {
        htmlElem.style.borderBottomColor = `${colorsConfig.error}`;
      } else {
        htmlElem.style.borderBottomColor = `${colorsConfig.link}`;
      }
      htmlElem.style.borderBottomWidth = `1px`;
      htmlElem.style.borderBottomStyle = `solid`;
    }

    htmlElem.setAttribute('meta-applied-style', 'true');
  }

}

export const styleHelper = new StyleHelper();
