package io.lab577.xinfin.iso20022.demo.utils

import java.time.OffsetDateTime

fun OffsetDateTime.startOfDay(): OffsetDateTime = this.withHour(0).withMinute(0).withSecond(0)
fun OffsetDateTime.endOfDay(): OffsetDateTime = this.withHour(23).withMinute(59).withSecond(59)
