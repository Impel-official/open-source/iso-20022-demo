import {customElement, query, queryAll, state} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {TransferRequest} from './models/TransferRequest';
import {AppConfig, config} from '../shared/config';
import {messagesStore, MessagesStore} from '../shared/messages/MessagesStore';
import {ReferenceDataStore, referenceDataStore} from '../transactions/ReferenceDataStore';
import {Suggestion} from '../shared/components/InputSuggestion';
import {RoutingService, routingService} from '../shared/routing/RoutingService';
import {TransferRequestConfirmationDialog} from "./TransferRequestConfirmationDialog";
import {TransferRequestStore, transferRequestStore} from "./TransferRequestStore";
import {repeat} from "lit/directives/repeat.js";
import {RestAdapter, restAdapter} from "../shared/rest/RestAdapter";
import {RestError, RestFieldError} from "../shared/rest/RestError";
import {PropertyValues} from "@lit/reactive-element";
import {cardCss, commonCss, formCss, typography} from "../shared/config/styles";
import {StyleHelper, styleHelper} from "../shared/config/StyleHelper";
import {AppToast} from "../shared/components/AppToast";
import {formatNumber, round} from "../shared/utils";
import {Countdown} from "../shared/components/Countdown";
import {MessageCollateral} from '../shared/api';

const styles = css`
${commonCss}
${formCss}
${cardCss}
${typography}

ui5-select{
    flex:1
    min-width: auto;
    width: 100%;
}

#sendButton {
    margin-top: 1rem;
    height: 3em;
}

ui5-select:required:invalid {
  color: #666;
}
ui5-option[value=""][hidden] {
  display: none;
}
.amount-currency-panel {
  display: flex;
  justify-content: space-between;
  gap: 2rem;
}
#reference-characters-remaining {
  display: flex;
  justify-content: flex-end;
}
#collateralInfo {
  width: 100%;
  display: flex;
  flex-direction: column;
}
`;

const COLLATERAL_QUOTE_EXPIRY_AFTER_SEC = 30;

@customElement('transfer-request-view')
export class TransferRequestView extends LitElement {
  static override styles = styles;

  private messagesStore: MessagesStore;
  private referenceStore: ReferenceDataStore;
  private routingService: RoutingService;
  private transferRequestStore: TransferRequestStore;
  private styleHelper: StyleHelper;
  private restAdapter: RestAdapter;
  private config: AppConfig;

  constructor() {
    super();
    this.restAdapter = restAdapter;
    this.messagesStore = messagesStore;
    this.referenceStore = referenceDataStore;
    this.routingService = routingService;
    this.transferRequestStore = transferRequestStore;
    this.styleHelper = styleHelper;
    this.config = config;
  }

  @query('#toBic')
  private toBicInput: any;
  @query('#fromAccount')
  private fromAccountInput: any;
  @query('#toAccount')
  private toAccountInput: any;
  @query('#amount')
  private amountInput: any;
  @query('#currency')
  private currencyInput: any;
  @query('#reference')
  private referenceInput: any;
  @query('#collateralSource')
  private collateralSourceInput: any;

  @query('#sendButton')
  private sendButton: any;

  @query('#collateralBusyIndicator')
  private collateralBusyIndicator: any;
  @query('#collateralInfo')
  private collateralInfo: any;
  @query('#dialog')
  private dialog: TransferRequestConfirmationDialog | undefined;
  @query('#toast')
  private toast: AppToast | undefined;
  @queryAll('ui5-input, ui5-step-input, ui5-select, ui5-textarea, input-suggestion')
  private formFields: HTMLCollection | undefined;
  @query('#countdown')
  private countdown: Countdown | undefined;

  @state()
  private allRequiredFieldsValid = false;
  @state()
  private collateralEnabled = false;
  @state()
  private exchangePrice = 0;
  @state()
  private collateralAmount = 0;
  @state()
  private transfersAmount: number = 0.05;
  @state()
  private exchangeCurrencySelected: string = 'USD';
  @state()
  private collateralTypeSelected: AppConfig['collateralTypes'][0] = {
    type: 'NATIVE',
    symbol: '',
    name: ''
  };
  @state()
  private referenceInputCharacterCount = 0;
  @state()
  private loading = false;

  protected override update(changedProperties: PropertyValues) {
    super.update(changedProperties);
    if (this.formFields?.length) {
      this.styleHelper.maybeStyleFormFields(this.formFields);
    }
  }

  override connectedCallback() {
    super.connectedCallback();
    this.referenceStore.bindComponentToPropertyChanges(this);
    this.transferRequestStore.bindComponentToPropertyChanges(this);
  }

  async sendCreateTransferRequest(delayedTracking = false) {
    if (!this.validateForm()) {
      return;
    }
    const messageCollateral: MessageCollateral | undefined = this.collateralEnabled ? {
        type: this.collateralTypeSelected.type,
        amount: round(this.collateralAmount),
        exchangePrice: round(this.exchangePrice),
        exchangeCurrency: this.currencyInput.selectedOption?.value,
        tokenSymbol: this.collateralTypeSelected.symbol,
      } : undefined;

    const transferRequest = new TransferRequest(
        config.bic,
        this.toBicInput.selectedOption?.value,
        this.fromAccountInput.selectedOption?.value,
        this.toAccountInput.selectedOption?.value,
        round(this.amountInput.selectedOption?.value).toString(),
        this.currencyInput.selectedOption?.value,
        this.referenceInput.value || '-',
        delayedTracking,
        messageCollateral
    );
    try {
      this.loading = true;

      this.transferRequestStore.sendCreateTransferRequest(transferRequest)
          .then(() => {
            this.loading = false;

            this.clearForm();

            this.transferRequestStore.isDetailedProgress()
                ? this.dialog?.show()
                : this.toast?.show();

          })
          .catch(error => {
            console.warn('sendCreateTransferRequest rejected', error);
            this.loading = false;
            // Validation errors?
            if (error.isError && error.fieldErrors) this.applyFieldErrors(error as RestError);
          });
    } catch (e) {
      console.error(e);

      this.loading = false;
    }
  }

  applyFieldErrors(error: RestError) {
    if (!error.fieldErrors) return;

    const fieldsByKey = new Map<string, any>();
    fieldsByKey.set("recipientAccount", this.toAccountInput);
    fieldsByKey.set("recipientBic", this.toBicInput);
    fieldsByKey.set("senderAccount", this.fromAccountInput);

    for (let i = 0; i < error.fieldErrors.length; i++) {
      const fieldError: RestFieldError = error.fieldErrors[i];
      const field = fieldsByKey.get(fieldError.property);

      if (field && fieldError) {
        field.valueState = 'Error';
        // TODO: slot is reset by ui5
        field.valueStateMessage = fieldError.message;
      }
    }
  }

  clearForm() {
    this.toBicInput._select(0);
    this.fromAccountInput._select(0);
    this.toAccountInput._select(0);
    this.amountInput._select(0);
    this.currencyInput._select(0);
    this.referenceInput.value = '';
    this.collateralSourceInput?._select(0);
    this.updateAllRequiredFieldsValid();

    this.collateralEnabled = false;
    this.exchangePrice = 0;
    this.collateralAmount = 0;
    this.transfersAmount = parseFloat(this.amountInput.selectedOption?.value);
    this.exchangeCurrencySelected = this.currencyInput.selectedOption?.value;
  }

  async amountChange() {
    this.updateAllRequiredFieldsValid();

    this.transfersAmount = parseFloat(this.amountInput.selectedOption?.value);
    this.recalculateCollateral();
  }

  async currencyChange() {
    this.updateAllRequiredFieldsValid();
    this.exchangeCurrencySelected = this.currencyInput.selectedOption?.value;

    await this.reloadCollateralData();
  }

  async collateralChange(e: CustomEvent) {
    const index = e.detail.selectedOption.value;
    this.collateralEnabled = !!(index && index != "-1");
    this.collateralTypeSelected = this.config.collateralTypes[index];
    
    await this.reloadCollateralData();
  }

  async reloadCollateralData() {
    const symbol = this.collateralTypeSelected.symbol;
    const baseCurrency = this.exchangeCurrencySelected;
    if(!symbol || !baseCurrency) return;
    if (this.collateralEnabled) {
      this.collateralBusyIndicator.active = true;
      const prices = await this.restAdapter.getPrice(baseCurrency, symbol);
      this.exchangePrice = parseFloat(prices.data[symbol]);
      this.countdown?.reset();
      this.collateralBusyIndicator.active = false;
      this.recalculateCollateral();
    }
  }

  recalculateCollateral() {
    if (this.collateralEnabled) {
      this.collateralAmount = this.transfersAmount / (this.exchangePrice || 0);
    } else {
      this.exchangePrice = 0;
      this.collateralAmount = 0;
    }
  }

  fieldChanged(event: Event) {
    this.validateField(event.target);
    this.updateAllRequiredFieldsValid();
    this.updateReferenceInputCharacterCount();
  }

  private updateReferenceInputCharacterCount(): void {
    this.referenceInputCharacterCount = (this.referenceInput.value as string).length;
  }

  updateAllRequiredFieldsValid() {
    this.allRequiredFieldsValid = this.hasValue(this.toBicInput)
        && this.hasValue(this.fromAccountInput)
        && this.hasValue(this.toAccountInput)
        && this.hasValue(this.amountInput)
        && this.hasValue(this.currencyInput);
  }

  hasValue(field: any): boolean {
    return !!field.value || !!field.selectedOption?.value;
  }

  validateField(field: any): boolean {
    let valid = true;
    if (field.required) {
      valid = this.hasValue(field)
    }
    if (field.min) {
      valid = parseFloat(field.value) >= field.min
    }
    // Set value-state
    field.valueState = valid ? 'None' : 'Error';
    styleHelper.styleFormField(field, false, !valid);
    return valid;
  }

  validateForm(): boolean {
    let allValid = true;
    const fields = [
      this.toBicInput,
      this.fromAccountInput,
      this.toAccountInput,
      this.amountInput,
      this.currencyInput
    ];
    for (let i = 0; i < fields.length; i++) {
      if (!this.validateField(fields[i])) allValid = false;
    }
    return allValid;
  }

  override render() {
    const toBicOptions: Array<Suggestion> = this.referenceStore.parties
        .filter(it => it.bic !== config.bic)
        .map(it => ({value: it.bic, description: it.name, extra: null}));
    const toIbanOptions: Array<Suggestion> = this.referenceStore.accounts
        .filter(it => it.bic !== config.bic)
        .map(it => ({value: it.iban, description: it.holder, extra: it.bic}));
    const fromIbanOptions: Array<Suggestion> = this.referenceStore.accounts
        .filter(it => it.bic === config.bic)
        .map(it => ({value: it.iban, description: it.holder, extra: it.bic}));
    return html`
        <div class="card form" style="margin: auto">
            <ui5-card>
                <ui5-card-header title-text="Create Transfer Request">
                </ui5-card-header>
                <div class="column">
                    <div class="label body1">To BIC<sup>*</sup></div>
                    <ui5-select id="toBic"
                                invalid
                                @change=${this.fieldChanged}
                                .disabled=${this.loading}
                                required>
                        <ui5-option value=""></ui5-option>
                        ${repeat(toBicOptions,
                                (suggestion) => suggestion.value,
                                (suggestion) => {
                                    return html`
                                        <ui5-option .value="${suggestion.value}">${suggestion.description}</ui5-option>
                                    `
                                })}
                    </ui5-select>
                </div>
                <div class="column">
                    <div class="label body1">To IBAN<sup>*</sup></div>
                    <ui5-select id="toAccount"
                                @change=${this.fieldChanged}
                                .disabled=${this.loading}
                                required>
                        <ui5-option .value=""></ui5-option>
                        ${repeat(toIbanOptions,
                                (suggestion) => suggestion.value,
                                (suggestion) => {
                                    return html`
                                        <ui5-option .value="${suggestion.value}">${suggestion.description}</ui5-option>`
                                })}
                    </ui5-select>
                </div>
                <div class="column">
                    <div class="label body1">From IBAN<sup>*</sup></div>
                    <ui5-select id="fromAccount"
                                @change=${this.fieldChanged}
                                .disabled=${this.loading}
                                required>
                        <ui5-option .value=""></ui5-option>
                        ${repeat(fromIbanOptions,
                                (suggestion) => suggestion.value,
                                (suggestion) => {
                                    return html`
                                        <ui5-option .value="${suggestion.value}">${suggestion.description}</ui5-option>`
                                })}
                    </ui5-select>
                </div>
                <div class="column">
                    <div class="label body1">Special Instructions</div>
                    <ui5-textarea id="reference"
                                  placeholder="Input Special Instructions"
                                  .disabled=${this.loading}
                                  .maxlength=${35}
                                  @input=${this.fieldChanged}
                    ></ui5-textarea>
                    <div id="reference-characters-remaining">
                        <characters-remaining
                                max=35
                                .count=${this.referenceInputCharacterCount}
                        ></characters-remaining>
                    </div>
                </div>
                <div class="column" style="margin-top: -1rem;">
                    <div class="label body1">Amount<sup>*</sup></div>
                    <div class="amount-currency-panel">
                        <ui5-select id="amount"
                                    placeholder="Choose Amount"
                                    .disabled=${this.loading}
                                    @change=${this.amountChange}
                                    required>
                            <ui5-option value="0.05">0.05</ui5-option>
                            <ui5-option value="0.10">0.10</ui5-option>
                            <ui5-option value="0.15">0.15</ui5-option>
                            <ui5-option value="0.20">0.20</ui5-option>
                            <ui5-option value="0.25">0.25</ui5-option>
                        </ui5-select>
                        <ui5-select
                                id="currency"
                                .disabled=${this.loading}
                                @change=${this.currencyChange}>
                            <ui5-option value="USD">USD</ui5-option>
                            <ui5-option value="EUR">EUR</ui5-option>
                            <ui5-option value="GBP">GBP</ui5-option>
                        </ui5-select>
                    </div>
                </div>
                <div class="column">
                    <div class="label body1">Instant Settlement (send collateral w/message)</div>
                    <ui5-select id="collateralSource"
                                @change=${this.collateralChange}
                    >
                        <ui5-option value='-1'>No Collateral</ui5-option>
                        ${this.config.collateralTypes.map((it, i) => {
                          return html`
                            <ui5-option value=${i}>
                              ${it.name} - ${this.referenceStore.party?.xinFinAddress}
                            </ui5-option>
                          `
                        })}
                    </ui5-select>
                </div>
                <div class="column ${this.collateralEnabled ? '' : 'hidden'}">
                    <ui5-busy-indicator id="collateralBusyIndicator" size="Medium">
                        ${this.collateralEnabled ? this.renderCollateralInfo() : ''}
                    </ui5-busy-indicator>
                </div>
                <ui5-button
                        design="Emphasized"
                        @click=${() => this.sendCreateTransferRequest(false)}
                        id="sendButton"
                        .disabled=${(!this.allRequiredFieldsValid) || (this.loading || (this.collateralBusyIndicator ? this.collateralBusyIndicator.active : false))}>
                    Send
                </ui5-button>
            </ui5-card>
        </div>
        <transfer-request-confirmation-dialog
                id="dialog"
                .transferRequest=${this.transferRequestStore.pendingTransferRequest}
                .endToEndId=${this.transferRequestStore.pendingTransactionId}
                @close=${this.clearForm}
        >
        </transfer-request-confirmation-dialog>
        <app-toast id="toast"
                   duration=${2000}
                   text= ${'Transfer Request Successfully Sent'}
        ></app-toast>
    `
  }

  renderCollateralInfo() {
    return html`
        <div id="collateralInfo" class="body2">
            <div>
              Account Address: ${this.referenceStore.party?.xinFinAddress}
            </div>
            <div>
              ${this.collateralTypeSelected.symbol} Price: 
              ${formatNumber(this.exchangePrice, 0, 8)} ${this.exchangeCurrencySelected}
            </div>
            <div>
              Collateral Required: 
              ${formatNumber(this.collateralAmount, 0, 8)} 
              ${this.collateralTypeSelected.symbol}
            </div>
            <div>Quote Good For:
                <span class="text-danger">
                    <app-countdown id="countdown"
                                   .value=${COLLATERAL_QUOTE_EXPIRY_AFTER_SEC}
                                   @finish=${this.reloadCollateralData}
                    ></app-countdown> seconds</span>
            </div>
        </div>
    `
  }
}
