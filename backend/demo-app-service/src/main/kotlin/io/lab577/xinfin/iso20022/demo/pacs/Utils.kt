package io.lab577.xinfin.iso20022.common.lib.iso.support

import io.lab577.xinfin.iso20022.demo.pacs.PacsIdBuilder
import org.apache.commons.text.StringSubstitutor
import org.springframework.core.io.ClassPathResource
import java.math.BigDecimal
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import javax.xml.datatype.XMLGregorianCalendar

fun XMLGregorianCalendar.toOffsetDateTime() =
    toGregorianCalendar().toZonedDateTime().toOffsetDateTime()

fun createPacs009(
    senderBic: String,
    recipientBic: String,
    senderAccount: String,
    recipientAccount: String,
    amount: BigDecimal,
    currency: String,
    referenceInfo: String,
    messageDate: OffsetDateTime,
    validUntil: OffsetDateTime,
    endToEndId: String,
    messageId: String = PacsIdBuilder.randomId(),
): String {
    if (endToEndId.length > 35) throw IllegalArgumentException("end2end ID can be up to 35 characters long")
    if (messageId.length > 35) throw IllegalArgumentException("messageId ID can be up to 35 characters long")
    val template = String(
        ClassPathResource("/iso20022/templates/pacs.009.001.09.template.xml").inputStream.readAllBytes()
    )
    val stringSubstitutor = StringSubstitutor(
        mapOf(
            "dateTime" to messageDate.toIsoDateTime(),
            "date" to messageDate.toIsoDate(),
            "validUntil" to validUntil.toIsoDateTime(),
            "senderBic" to senderBic,
            "recipientBic" to recipientBic,
            "senderAccount" to senderAccount,
            "recipientAccount" to recipientAccount,
            "endToEndId" to endToEndId,
            "messageId" to messageId,// TODO
            "currency" to currency,
            "amount" to amount.toString(),
            "referenceInfo" to referenceInfo,
        )
    )
    return stringSubstitutor.replace(template)
}

fun createPacs002(
    endToEndId: String,
    senderBic: String,
    recipientBic: String,
    amount: BigDecimal,
    approve: Boolean,
    currency: String = "WEI",
    messageId: String = UUID.randomUUID().toString().substring(0, 35),
): String {
    if (endToEndId.length > 35) throw IllegalArgumentException("end2end ID can be up to 35 characters long")
    if (messageId.length > 35) throw IllegalArgumentException("messageId ID can be up to 35 characters long")
    val templateFile = when (approve) {
        true -> "/iso20022/templates/pacs.002.001.12.template.xml"
        false -> "/iso20022/templates/pacs.002.001.12_rejection.template.xml"
    }
    val template = String(ClassPathResource(templateFile).inputStream.readAllBytes())
    val dateTime = Date()
    val stringSubstitutor = StringSubstitutor(
        mapOf(
            "dateTime" to dateTime.toIsoDateTime(),
            "date" to dateTime.toIsoDate(),
            "senderBic" to senderBic,
            "recipientBic" to recipientBic,
            "endToEndId" to endToEndId,
            "messageId" to messageId,// TODO
            "currency" to currency,
            "amount" to amount.toString()
        )
    )
    return stringSubstitutor.replace(template)
}

fun OffsetDateTime.toIsoDateTime(): String {
    return this.withOffsetSameInstant(ZoneOffset.UTC)
        .format(DateTimeFormatter.ISO_DATE_TIME)
}

fun OffsetDateTime.toIsoDate(): String {
    return this.withOffsetSameInstant(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE)
}

fun Date.toIsoDateTime(): String {
    val tz = TimeZone.getTimeZone("UTC")
    // Quoted "Z" to indicate UTC, no timezone offset
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        .also { it.timeZone = TimeZone.getTimeZone("UTC") }
    df.timeZone = tz
    return df.format(this)
}

fun Date.toIsoDate(): String {
    val tz = TimeZone.getTimeZone("UTC")
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    df.timeZone = tz
    return df.format(this)
}
