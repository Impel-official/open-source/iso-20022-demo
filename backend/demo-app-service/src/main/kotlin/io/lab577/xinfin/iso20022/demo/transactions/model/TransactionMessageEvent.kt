package io.lab577.xinfin.iso20022.demo.transactions.model

import com.fasterxml.jackson.annotation.JsonCreator
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageEntity
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import java.time.OffsetDateTime
import java.util.*

class TransactionMessageEvent @JsonCreator constructor(
    val transactionId: UUID,
    val status: TransactionStatus,
    val message: TransactionMessage
) {
    companion object {
        fun from(message: TransactionMessage, status: TransactionStatus) = TransactionMessageEvent(
            message = message,
            transactionId = message.transactionId,
            status = status
        )

        fun from(transactionMessage: TransactionMessageEntity) = TransactionMessageEvent(
            message = transactionMessage.toDto(),
            transactionId = transactionMessage.transaction.id,
            status = transactionMessage.transaction.status
        )

        fun from(transaction: TransactionEntity) = TransactionMessageEvent(
            transactionId = transaction.id,
            status = transaction.status,
            message = TransactionMessage(
                id = transaction.id,
                transactionId = transaction.id,
                senderBic = transaction.senderBic,
                recipientBic = transaction.recipientBic,
                counterPartyBic = transaction.counterPartyBic,
                amount = transaction.amount,
                currency = transaction.currency,
                direction = transaction.direction,
                type = PacsType.PACS_009, // ???
                messageDate = OffsetDateTime.now(),
                network = "xdc", // ???
                referenceInfo = transaction.referenceInfo,
                rollupTransactionHash = transaction.rollupTransactionHash,
            )
        )
    }
}
