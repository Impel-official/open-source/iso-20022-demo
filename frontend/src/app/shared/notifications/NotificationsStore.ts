import { SingleHostAbstractStore } from "../SingleHostAbstractStore";
import { NotificationIconElement } from "./NotificationIconElement";
import { Notifications } from "./models/Notifications";
import { Notification, NotificationType } from "./models/Notification";
import { NotificationsRepository } from "./adapters/NotificationsRepository";
import { AppConfig, config } from "../config";
import { notificationsRepository } from "./adapters/LocalStorageNotificationsRepository";
import { RoutingService, routingService } from "../routing/RoutingService";
import { TransactionMessageEvent } from "../stomp/TransactionMessageEvent";
import { stompAdapter, StompAdapter } from "../stomp/StompAdapter";
import { Transaction } from "../../transactions/model/Transaction";


export class NotificationsStore extends SingleHostAbstractStore<NotificationIconElement> {
  notifications: Notifications = Notifications.empty();
  
  constructor(
    config: AppConfig,
    initialNotifications: Notifications,
    private notificationsRepository: NotificationsRepository,
    private routingService: RoutingService,
    private stompAdapter: StompAdapter,
  ) {
    super(config);
    this.notifications = initialNotifications;
  }
  
  async init() {
    this.stompAdapter.addListener(
      this, 'TransactionChanges', this.handleTransactionChangesEvent.bind(this)
    );

    this.stompAdapter.addListener(
      this, 'NewTransactions', this.handleNewTransactionEvent.bind(this)
    );
  }
  
  add(notification: Notification<NotificationType>) {
    this.notifications = this.notifications.add(notification);
    this.notificationsRepository.save(this.notifications);
    this.updatePropertiesToBoundHost();
  }

  remove(notificationId: string) {
    this.notifications = this.notifications.remove(notificationId);
    this.notificationsRepository.save(this.notifications);
    this.updatePropertiesToBoundHost();
  }

  markAsRead(notificationId: string) {
    this.notifications = this.notifications.markAsRead(notificationId);
    this.notificationsRepository.save(this.notifications);
    this.updatePropertiesToBoundHost();
  }
  
  // websocket binding
  
  private handleTransactionChangesEvent(event: TransactionMessageEvent) {
    if(
      event.message.recipientBic === this.config.bic &&
      (event.status === 'Approved' || event.status === 'Rejected')
    ) {
      const title = 
        event.status === 'Approved' ? 
          `Payment request accepted` : `Payment request rejected` ;
      const isApproved = event.status === 'Approved';

      const amount = event.message.amount;
      const currency = event.message.currency;
      
      const description =
        `${amount} ${currency} has been responded by counterparty`;

      const transferRequestResponseNotification = new Notification(
        Notification.newId(),
        'TransferRequestResponse',
        title,
        description,
        event.message.counterPartyBic,
        event.message.messageDate2.getTime(),
        {transactionId: event.transactionId, isApproved },
        'UNREAD'
      )
      notificationsStore.add(transferRequestResponseNotification);
    }
  }

  private handleNewTransactionEvent(transaction: Transaction) {
    if(transaction.status === 'Received') {
      const title = 'Payment request needs confirmation';

      const amount = transaction.amount;
      const currency = transaction.currency;
      const description = `${amount} ${currency} has been requested to be transferred`;
      
      const unapprovedTransactionNotification = new Notification(
        Notification.newId(),
        'TransferRequest',
        title,
        description,
        transaction.counterPartyBic,
        transaction.requestDate2.getTime(),
        {transactionId: transaction.id},
        'UNREAD'
      )
      notificationsStore.add(unapprovedTransactionNotification);
    }
  }
  
  // actions
  
  handleNotificationAction(notification: Notification<NotificationType>) {
    this.markAsRead(notification.id);
    if(
      notification.type === 'TransferRequestResponse' ||
      notification.type === 'TransferRequest'
    ) {
      const url = this.routingService.urlForName(
        'transaction-details',
        {id: notification.payload.transactionId}
      );
      this.routingService.navigate(url);
    }
  }
}

const initialNotifications = notificationsRepository.get();
export const notificationsStore = new NotificationsStore(
  config,
  initialNotifications,
  notificationsRepository,
  routingService,
  stompAdapter
)