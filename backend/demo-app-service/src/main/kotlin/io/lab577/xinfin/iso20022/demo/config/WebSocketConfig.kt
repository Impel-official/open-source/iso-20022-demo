package io.lab577.xinfin.iso20022.demo.config

import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.server.RequestUpgradeStrategy
import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy
import org.springframework.web.socket.server.support.DefaultHandshakeHandler

@Configuration
@EnableWebSocketMessageBroker
@Order(Ordered.HIGHEST_PRECEDENCE + 99)
class WebSocketConfig : WebSocketMessageBrokerConfigurer {

    companion object {
        const val STOMP_ENDPOINT = "/ws"
        const val WEBSOCKET_TOPIC = "/topic"
        const val WEBSOCKET_QUEUE = "/queue"
        const val WEBSOCKET_TOPIC_TRANSACTIONS = "$WEBSOCKET_TOPIC/transactions"
        const val WEBSOCKET_TOPIC_TRANSACTION_MESSAGES = "$WEBSOCKET_TOPIC/transaction-messages"
        const val WEBSOCKET_TOPIC_TRANSACTION_METRICS = "$WEBSOCKET_TOPIC/transaction-metrics"
    }

    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker(WEBSOCKET_TOPIC, WEBSOCKET_QUEUE)
        config.setApplicationDestinationPrefixes("/app")
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        val upgradeStrategy: RequestUpgradeStrategy = TomcatRequestUpgradeStrategy()
        registry.addEndpoint(STOMP_ENDPOINT)
            .setHandshakeHandler(DefaultHandshakeHandler(upgradeStrategy))
            .setAllowedOriginPatterns("*")
    }
}
