# API Definition

mockup: https://drive.google.com/file/d/1h5K0bYTGnuMUvNosH0M5sqjL9yQ4oTue/view

## REST
GET /references => currencies, states

GET /transactions?pageNumber=...&pageSize=.. => sort reverse chronological, paging

GET /transactions?id=..&type=... => RSSQL, paging
```
[
    {
    "id": "8e741967-33d2-4a22-b775-6c9a78a73bf9",
    "counterPartyBIC": "AXISSGSGIPB",
    "requestDate": "13/02/2022 22:50",
    "currency": "SGD",
    "amount": "200000",
    "type": "Receiver",
    "state": "Accepted"
    }
]
```

GET /transactions/:id
```
{
    "id": "8e741967-33d2-4a22-b775-6c9a78a73bf9",
    "fromBIC": "AXISSGSGIPB",
    "toBIC": "AXISSGSGIPB",
    "requestDate": "13/02/2022 22:50",
    "currency": "SGD",
    "amount": "200000",
    "type": "Receiver",
    "state": "Accepted",
    "messages": [
        {"type": "pacs009", "id": "", "payload": "xml"},
        {"type": "pacs002", "id": ""}
    ]
},
```

GET /transactions/unapproved => named txs filter: state received

POST /transactions/:id => {approve: boolean}

POST /transfer-requests => {create pacs009 + reference, look mockup}

GET /dashboard => look mockup

```
[
    {"metricName": "totalSendMtD, amountUSD: "", txCount: ""},
    ...
]
```

## Websockets:

new tx event => {tx}

new tx message event => {tx_id, status, message:{...}}

update dashboard event => {metricName, ...newValues}
