export class Account {
  public constructor(
    public holder: string,
    public iban: string,
    public bic: string,
  ) {}
  
  static fromJson(json: any): Account {
    // TODO: this needs json schema validation
    return new Account(json.holder, json.iban, json.bic)
  }
}