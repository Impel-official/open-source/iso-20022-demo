package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.transactions.model.OutgoingTransactionMessage
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.http.ResponseEntity

interface PacsMessagesController {
    @Operation(
        summary = "Send PACS 009 message",
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "PACS 009.001.10 message data",
            content = [Content(schema = Schema(implementation = OutgoingTransactionMessage.TransferRequest::class))],
            required = true
        ),
        responses = [
            ApiResponse(description = "Accepted", responseCode = "202"),
            ApiResponse(description = "Bad request", responseCode = "400"),
            ApiResponse(description = "Error", responseCode = "500")
        ]
    )
    fun sendPacs009Message(
        transferRequest: OutgoingTransactionMessage.TransferRequest
    ): ResponseEntity<SendMessageResponse>

    @Operation(
        summary = "Send PACS 002 message (confirms or rejects a transaction)",
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "PACS 002.001.12 message data",
            content = [Content(schema = Schema(implementation = OutgoingTransactionMessage.TransferResponse::class))],
            required = true
        ),
        responses = [
            ApiResponse(description = "Accepted", responseCode = "202"),
            ApiResponse(description = "Bad request", responseCode = "400"),
            ApiResponse(description = "Error", responseCode = "500")
        ]
    )
    fun sendPacs002Message(
        transferResponse: OutgoingTransactionMessage.TransferResponse
    ): ResponseEntity<SendMessageResponse>
}
