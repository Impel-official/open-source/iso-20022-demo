import {html, LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators.js';

@customElement('app-countdown')
export class Countdown extends LitElement {
  @property()
  value: number = 30;
  @property()
  intervalMs: number = 1000;

  @state()
  private countdownValue: number = 0;

  private interval: any;

  override connectedCallback() {
    super.connectedCallback();

    this.setCountdown();
  }

  private setCountdown() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const host = this;
    this.countdownValue = this.value;
    this.interval = setInterval(() => {
      if (this.countdownValue <= 0) {
        clearInterval(host.interval);

        this.emitFinishEvent();
        return;
      }

      this.countdownValue = this.countdownValue - 1;
    }, this.intervalMs);
  }

  private emitFinishEvent() {
    this.dispatchEvent(new CustomEvent<any>('finish'));
  }

  reset() {
    clearInterval(this.interval);

    this.setCountdown();
  }

  override render() {
    return html`<span>${this.countdownValue}</span>`;
  }
}
