var config = {
  host: 'https://sandbox.impel-lab577.co.uk/demo',
  baseUrl: "",
  branding : {
    logoUrl: "/assets/logo-banka.svg",
    icoUrl: "/assets/banka-favicon.ico",
    orgName: "Bank A"
  },
  api: {
    requestTimeoutMs: 30000, // 30s
    retryAttempts: 3,
    retryTimeIntervalMs: 200, // 0.2s
  },
  rest: {
    references: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/gateway/iso20022/transaction-options",
    transactions: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/gateway/iso20022/transactions",
    transactionMessages: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/gateway/iso20022/transaction-messages",
    pacs009: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/gateway/iso20022/pacs/messages/pacs-009-001-10",
    transactionMetrics: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/gateway/iso20022/transaction-metrics",
    prices: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/kucoin/price",
    hubSpotContacts: "https://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/api/hubspot/contact"
  },
  stomp: {
    url: "wss://api-sandbox.impel-lab577.co.uk/demo/BBBBCAXXXXX/ws",
    newTransactionsTopic: "/topic/transactions",
    transactionsChangesTopic: "/topic/transaction-messages",
    transactionsMetricsTopic: "/topic/transaction-metrics"
  },
  hubSpotAnalyticsEnabled: true,
  auth: {
    type: "HUB_SPOT",
    keycloak: {
      url: 'https://auth-banka-sandbox.impel-lab577.co.uk/auth',
      realm: 'BBBBCAXXXXX',
      clientId: 'gateway-ui',
    },
    hubSpot: {
      unauthorizedMessage: 'Please use a link from registration e-mail or visit ',
      unauthorizedMessageLink: "https://impel.global"
    }
  },
  xdc: {
    txPrefixUrl: 'https://explorer.apothem.network/txs/'
  },
  bic: "BBBBCAXXXXX",
  progressUpdatesIntervalMs: 750,
  progressUpdatesDelayedIntervalMs: 750 * 4,
  shareDemoExperience: {
    url: "https://impel.global",
  },
  defaultView: 'transfer-request',
  reportingNetworkTableDisabled: true,
  reportingReceivedTableDisabled: true,
  featureFlags: {
    transactionsView: {
      showAcceptRejectButtons: false, // https://gitlab.com/lab577/xinfin-iso20022/-/issues/269
      showSelectCheckbox: false // https://gitlab.com/lab577/xinfin-iso20022/-/issues/307
    }
  },
  collateralTypes: [
    { type: 'NATIVE', symbol: 'XDC', name: 'XDC'},
    { type: 'ERC20', symbol: 'BTC', name: 'xBTC (Wrapped BTC)'},
    { type: 'ERC20', symbol: 'USDC', name: 'xUSDC (Wrapped USDC)'}
  ]
}
