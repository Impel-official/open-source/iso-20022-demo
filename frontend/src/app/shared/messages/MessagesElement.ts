import {customElement, queryAll} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {messagesStore, MessagesStore} from './MessagesStore';
import {repeat} from 'lit/directives/repeat.js';

const FADE_DURATION_SEC = 0.5;

const styles = css`
ui5-message-strip {
  margin-top: 4px;
  transition: all ${FADE_DURATION_SEC}s ease-out;
  opacity: 0;
}
.show {
  opacity: 1;
}
`

// `messages` is not a valid custom name
@customElement('user-messages')
export class MessagesElement extends LitElement {
  static override styles = styles;
  
  private messagesStore: MessagesStore;

  @queryAll('ui5-message-strip')
  private messagesStripes: any;
  
  constructor() {
    super();
    // TODO: this needs to provided as constructor parameter
    this.messagesStore = messagesStore;
  }

  override connectedCallback() {
    super.connectedCallback();
    this.messagesStore.bindComponentToPropertyChanges(this);
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    this.messagesStore.unbindComponentFromPropertyChanges(this);
  }
  
  override updated(){
    if(this.messagesStripes && this.messagesStripes.length == 0) return;
    setTimeout( () => {
      const messagesStripes = this.messagesStripes as Array<HTMLElement>;
      messagesStripes.forEach(m => m.classList.add('show'));
    })
  }
  
  private removeMessage(event: CustomEvent) {
    const selectedMessageStrip = event.target as HTMLElement;
    if(selectedMessageStrip === null) return;
    selectedMessageStrip.classList.remove('show');
    const selectedMessageId = selectedMessageStrip.attributes.getNamedItem('data-id')?.value;
    if(selectedMessageId === undefined) return;
    setTimeout(
      () => this.messagesStore.removeMessage(selectedMessageId),
      FADE_DURATION_SEC * 1000
    )
  }
  
  override render() {
    return html`
      ${repeat(this.messagesStore.messages.getMessages(),
              (tx) => tx.id, 
              (tx) => {
                return html`
                  <ui5-message-strip design=${tx.type}
                                     @close=${this.removeMessage}
                                     data-id=${tx.id}
                  >
                    ${tx.description}
                  </ui5-message-strip>
                `;
      })}
    `
  }
}