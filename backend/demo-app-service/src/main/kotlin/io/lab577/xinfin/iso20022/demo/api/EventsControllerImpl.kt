package io.lab577.xinfin.iso20022.demo.api

import io.cloudevents.CloudEvent
import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntity
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.EventDto
import io.lab577.xinfin.iso20022.demo.service.GatewayEventHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/gateway/iso20022/demo/event")
class EventsControllerImpl(
    private val gatewayEventHandler: GatewayEventHandler,
) : EventsController {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(EventsControllerImpl::class.java)

        const val DEMO_EVENT_API = "/api/gateway/iso20022/demo/event"

    }

    @PostMapping
    override fun handleEvent(@RequestBody cloudEvent: CloudEvent): ResponseEntity<Void> {
        logger.info("Received an event with type ${cloudEvent.type}")
        val eventType = EventDto.TypeEnum.values().find { it.name == cloudEvent.type }
        if (eventType == null) {
            logger.warn("Received event with type ${cloudEvent.type} is not supported")
        } else {
            gatewayEventHandler.handleEvent(
                GatewayEventEntity(
                    id = UUID.fromString(cloudEvent.id),
                    timestamp = cloudEvent.time,
                    type = eventType,
                    subjectId = cloudEvent.subject!!,
                    data = cloudEvent.data!!.toBytes()
                )
            )
        }

        return ResponseEntity.ok().build()
    }

}
