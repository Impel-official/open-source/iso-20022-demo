package io.lab577.xinfin.iso20022.demo.api.dto

data class CountResponse (
    val count: Long
)
