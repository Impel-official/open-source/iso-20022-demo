package io.lab577.xinfin.iso20022.demo.domain.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum class PacsType(val isoName: String) {
    PACS_002("pacs.002.001.12"),
    PACS_004("pacs.004.001.11"),
    PACS_007("pacs.007.001.11"),
    PACS_009("pacs.009.001.10");

    @JsonValue
    override fun toString(): String {
        return this.name.removePrefix(PREFIX)
    }

    companion object {
        private const val PREFIX = "PACS_"

        val SUPPORTED_ISO20022_MESSAGE_TYPES: List<String> by lazy {
            values().map { it.isoName }
        }

        @JsonCreator
        fun fromString(value: String): PacsType {
            val normalized = value.uppercase()
                .let { if (it.startsWith(PREFIX)) it else "$PREFIX$it" }
            return valueOf(normalized)
        }

        fun fromIsoNameString(isoName: String): PacsType? {
            return values().find { it.isoName == isoName }
        }
    }
}
