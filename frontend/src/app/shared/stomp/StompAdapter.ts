import {Client, Message, StompSubscription} from '@stomp/stompjs';
import {AppConfig, config} from '../config';
import {Transaction} from '../../transactions/model/Transaction';
import {TransactionMessageEvent} from './TransactionMessageEvent';
import {TransactionAllMetrics} from "../api";


type NewTransaction = 'NewTransactions';
type TransactionChanges = 'TransactionChanges';
type TransactionsChanges = 'TransactionsChanges';
type MetricsUpdates = 'MetricsUpdates';
type TopicName = NewTransaction | TransactionChanges | MetricsUpdates;

type TransactionsChangesTopicCallback = 
  (transactionMessageEvent: TransactionMessageEvent) => void;
type NewTransactionsTopicCallback = (transaction: Transaction) => void;
type MetricsUpdatesTopicCallback = (metrics: TransactionAllMetrics) => void;
type TopicCallback<T extends TopicName> = 
  T extends NewTransaction ? NewTransactionsTopicCallback : 
    T extends TransactionChanges ? TransactionsChangesTopicCallback : 
      T extends MetricsUpdates ? MetricsUpdatesTopicCallback : never;

export class StompAdapter {
  
  private client: Client;
  private stompSubscriptions: Array<StompSubscription> = [];
  private listeners = new Array<[unknown, TopicName, TopicCallback<TopicName>]>();
  
  constructor(private config: AppConfig) {
    this.client = new Client({
      brokerURL: config.stomp.url,
      reconnectDelay: 5000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000,
    });
  }
  
  async start(){ 
    await this.client.activate();
    this.client.onConnect = this.onConnect.bind(this);
  }
  
  private onConnect(){
    const transactionsTopicSubscription = this.client.subscribe(
      this.config.stomp.newTransactionsTopic,
      this.notifyNewTransactionsSubscribers.bind(this)
    );
    const transactionsChangesTopicSubscription = this.client.subscribe(
      this.config.stomp.transactionsChangesTopic,
      this.notifyTransactionMessageSubscribers.bind(this)
    );
    const metricsUpdatesTopicSubscription = this.client.subscribe(
      this.config.stomp.transactionsMetricsTopic,
      this.notifyMetricsUpdatesSubscribers.bind(this)
    );
    this.stompSubscriptions.push(transactionsTopicSubscription);
    this.stompSubscriptions.push(transactionsChangesTopicSubscription);
    this.stompSubscriptions.push(metricsUpdatesTopicSubscription);
  }
  
  async stop(){
    await this.client.deactivate();
    this.stompSubscriptions.forEach(it=>it.unsubscribe());
  }
  
  addListener<T extends TopicName>(id: unknown, topic: T, callback: TopicCallback<T>) {
    this.listeners.push([id, topic, callback])
  }
  
  removeListener(id: unknown, topicName: TopicName) {
    this.listeners.filter(it=> id !== it[0] && topicName !== it[1])
  }

  private notifyNewTransactionsSubscribers(message: Message) {
    for(const listenerEntry of this.listeners.values()) {
      const topic = listenerEntry[1];
      if(topic === 'NewTransactions') {
        const callback = listenerEntry[2] as NewTransactionsTopicCallback;
        const newTransactionJSON = JSON.parse(message.body);
        const newTransaction = Transaction.fromJson(newTransactionJSON);
        callback(newTransaction);
      }
    }
  }

  private notifyTransactionMessageSubscribers(message: Message) {
    for(const listenerEntry of this.listeners.values()) {
      const topic = listenerEntry[1];
      if(topic === 'TransactionChanges') {
        const callback = listenerEntry[2] as TransactionsChangesTopicCallback;
        const transactionUpdateJSON = JSON.parse(message.body);
        const transactionMessageEvent = 
          TransactionMessageEvent.fromJson(transactionUpdateJSON);
        callback(transactionMessageEvent);
      }
    }
  }

  private notifyMetricsUpdatesSubscribers(message: Message) {
    for(const listenerEntry of this.listeners.values()) {
      const topic = listenerEntry[1];
      if(topic === 'MetricsUpdates') {
        const callback = listenerEntry[2] as MetricsUpdatesTopicCallback;
        const metrics = JSON.parse(message.body) as TransactionAllMetrics;
        callback(metrics);
      }
    }
  }
  
}


export const stompAdapter = new StompAdapter(config);
