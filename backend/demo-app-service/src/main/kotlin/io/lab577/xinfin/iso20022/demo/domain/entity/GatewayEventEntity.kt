package io.lab577.xinfin.iso20022.demo.domain.entity

import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.EventDto
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Lob
import javax.persistence.Table

@Entity
@Table(name = "gateway_event")
class GatewayEventEntity(
    id: UUID? = null,
    @Column(name = "timestamp", nullable = false)
    var timestamp: OffsetDateTime? = null,
    @Column(name = "type", nullable = false, updatable = false)
    var type: EventDto.TypeEnum,
    @Column(name = "subject_id", nullable = true, updatable = false)
    var subjectId: String,
    @Lob
    @Column(name = "data")
    val data: ByteArray,
) : AbstractAuditableEntity(id) {

    override fun toString(): String {
        return "GatewayEventEntity(" +
            "id=$id, " +
            "type=$type, " +
            "timestamp=$timestamp, " +
            "subjectId=$subjectId" +
            ")"
    }
}


