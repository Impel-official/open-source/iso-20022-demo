# DEMO ISO 20022

**Impel ISO 20022 Demo** is an example app which integrates with **Impel ISO 20022 platform**.
It consists of two components:
* DEMO ISO 20022 - APP
* DEMO ISO 20022 - UI

## DEMO ISO 20022 - APP

### Overview

You can run the `DEMO ISO 20022 APP`. It has configuration to connect to a sandbox environment that
runs the **Impel ISO 20022 platform**.

### Requirements

* JDK 11

### Install Dependencies
In order to run an app we have to build it first. Below command builds and runs tests:
```
cd backend
./gradlew clean build
```

### Start Bank A Demo App
Once app is built we can run **Bank A Demo App** with below command:
(if you are already in backend directory there is no need to enter it again)
```
cd backend
SPRING_PROFILES_ACTIVE=sandbox-party-a ./gradlew bootRun
```

Wait a few seconds until app is started. After startup, application will process all events from
**Impel ISO 20022 platform** in order to be consistent with the platform.
Swagger UI of an app is available [here](http://localhost:8095/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)

### Start Bank B Demo App
Once app is built we can run **Bank B Demo App** with below command:
(if you are already in backend directory there is no need to enter it again)
```
cd backend
SPRING_PROFILES_ACTIVE=sandbox-party-b ./gradlew bootRun
```

Wait a few seconds until app is started. After startup, application will process all events from
**Impel ISO 20022 platform** in order to be consistent with the platform.
Swagger UI of an app is available [here](http://localhost:8096/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)


## DEMO ISO 20022 - UI

### Overview

You can run the `DEMO ISO 20022 UI`. It is configured to connect to a sandbox environment that
runs the `DEMO ISO 20022 APP`.

### Requirements

* npm (^6.14.0)

#### Install Dependencies

```
cd frontend
npm install
```

#### Start Bank A Demo UI
When **Bank A Demo App** is running we can run **Bank A Demo UI** with below command:
(if you are already in frontend directory there is no need to enter it again)
```
cd frontend
npm run workflow:local:bank-a
```

Wait a few seconds until bundle is created and open this
[link](http://localhost:8001/?xcx=ed4cd260-4f80-4fde-8a0d-a0950a9ec43d)

#### Start Bank B Demo UI
When **Bank B Demo App** is running we can run **Bank B Demo UI** with below command:
(if you are already in frontend directory there is no need to enter it again)
```
cd frontend
npm run workflow:local:bank-b
```

Wait a few seconds until bundle is created and open this
[link](http://localhost:8002/?xcx=ed4cd260-4f80-4fde-8a0d-a0950a9ec43d)
