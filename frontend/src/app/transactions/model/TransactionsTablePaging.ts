export class TransactionsTablePaging {
  static INITIAL_PAGE_NUMBER = 0;
  static PER_SIZE = 10;

  public constructor(
    public readonly pageNumber: number,
    public readonly pageSize: number
  ) {
    this.validate();
  }

  validate(){
    // TODO: validate fields and throw error events
  }

  static fromUrlParams(urlSearchParams: URLSearchParams): TransactionsTablePaging {
    const pageNumberString = urlSearchParams.get('pageNumber');
    const pageSizeString = urlSearchParams.get('pageSize');

    const pageNumber = pageNumberString ?
      parseInt(pageNumberString) : this.INITIAL_PAGE_NUMBER;
    const pageSize = pageSizeString ? parseInt(pageSizeString) : this.PER_SIZE;

    return new TransactionsTablePaging(
      isNaN(pageNumber) ? this.INITIAL_PAGE_NUMBER : pageNumber,
      isNaN(pageSize) ? this.INITIAL_PAGE_NUMBER : pageSize,
    )
  }

  static toUrlParams(filter: TransactionsTablePaging): URLSearchParams {
    const queryParams =  new URLSearchParams();
    queryParams.set('pageNumber', filter.pageNumber.toString());
    queryParams.set('pageSize', filter.pageSize.toString());
    return queryParams;
  }

  static default(pageNumber: number = this.INITIAL_PAGE_NUMBER): TransactionsTablePaging{
    return new TransactionsTablePaging(pageNumber, this.PER_SIZE)
  }

}