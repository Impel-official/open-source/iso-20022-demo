package io.lab577.xinfin.iso20022.demo.utils

import com.prowidesoftware.swift.model.mx.AbstractMX
import com.prowidesoftware.swift.model.mx.JaxbContextCacheImpl
import com.prowidesoftware.swift.model.mx.JaxbContextLoader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.xml.bind.UnmarshalException

object IsoXml {

    private val logger: Logger = LoggerFactory.getLogger(IsoXml::class.java)

    init {
        JaxbContextLoader.INSTANCE.cacheImpl = JaxbContextCacheImpl()
    }

    fun xmlToMx(xml: String): AbstractMX {
        val mxMessage: AbstractMX? = AbstractMX.parse(xml)
        when {
            mxMessage?.document() == null -> throw UnmarshalException("XML has no <Document> element with known ISO20022 namespace")
        }
        return mxMessage!!
    }

}
