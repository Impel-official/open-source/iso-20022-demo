import { ProgressUpdate } from "./models/ProgressUpdate";
import { ProgressUpdates, ProgressUpdateState } from "./models/ProgressUpdates";
import { ProgressUpdateAction, ProgressUpdatesActions } from "./models/ProgressUpdatesActions";
import { AppConfig, config } from "../config";
import { SingleHostAbstractStore } from "../SingleHostAbstractStore";
import { ReactiveElement } from "lit";

export class ProgressUpdatesActionsStore extends SingleHostAbstractStore<ReactiveElement> {
  private progressUpdatesActions: ProgressUpdatesActions = [];
  private interval: NodeJS.Timer | undefined = undefined;
  
  progressUpdates: ProgressUpdates = ProgressUpdates.empty();

  constructor(config: AppConfig) {
    super(config);
  }

  pushAddAction(progressUpdate: ProgressUpdate) {
    this.apply({name: 'ADD', progressUpdate});
  }

  pushChangeAction(progressUpdate: ProgressUpdate) {
    this.apply({name: 'CHANGE', progressUpdate});
  }

  pushEmptyAction() {
    this.apply({name: 'EMPTY'});
  }

  pushStateChangeAction(state: ProgressUpdateState) {
    this.apply({name: 'STATE', state});
  }
  
  clearActions() {
    this.progressUpdatesActions = [];
    this.progressUpdates = ProgressUpdates.empty();
  }
  
  applyActionsPeriodically(
    hostToBeUpdated: ReactiveElement,
    processUpdateEndCallback: (progressUpdateState: ProgressUpdateState) => void,
    interval: number = this.config.progressUpdatesIntervalMs
  ) {
    this.bindComponentToPropertyChanges(hostToBeUpdated);
    this.interval = setInterval(
      ()=> {
        const nextAction = this.progressUpdatesActions.pop();
        if(!nextAction) {
          if(this.progressUpdates.getState() !== 'Pending') {
            this.unbindComponentFromPropertyChanges(hostToBeUpdated);
            processUpdateEndCallback(this.progressUpdates.getState());
            this.stopInterval();
          }
          return;
        }
        switch (nextAction.name){
          case 'ADD':
            this.progressUpdates =
              this.progressUpdates.addUpdate(nextAction.progressUpdate);
            this.updatePropertiesToBoundHost();
            break;
          case 'CHANGE':
            this.progressUpdates =
              this.progressUpdates.changeUpdate(nextAction.progressUpdate);
            this.updatePropertiesToBoundHost();
            break;
          case 'STATE':
            this.progressUpdates =
              this.progressUpdates.changeState(nextAction.state);
            this.updatePropertiesToBoundHost();
            break;
          case 'EMPTY':
            this.progressUpdates = this.progressUpdates.emptyUpdates();
            this.updatePropertiesToBoundHost();
            break;
        }
      },
      interval
    )
  }

  private apply(action: ProgressUpdateAction) {
    this.progressUpdatesActions.unshift(action);
  }

  private stopInterval() {
    if(this.interval) clearInterval(this.interval);
  }
  
}

export const progressUpdatesActionsStore = new ProgressUpdatesActionsStore(config);