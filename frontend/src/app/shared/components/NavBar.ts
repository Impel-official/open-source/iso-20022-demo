import {Router} from "@vaadin/router";
import {css, html, LitElement, PropertyValues} from 'lit';
import {customElement, property, query, queryAll, state} from 'lit/decorators.js';
import {config} from '../config';
import {userStore, UserStore} from "../user/UserStore";
import {MenuItemSelectedEvent} from "../user/UserAvatarElement";
import {AuthService, authService} from "../auth/AuthService";
import {TransactionsStore, transactionsStore} from "../../transactions/TransactionsStore";
import {classMap} from "lit/directives/class-map.js";
import {RoutingService, routingService} from '../routing/RoutingService';
import {colorsConfig, commonCss, typography} from "../config/styles";
import {RouteName} from "../routing/routeConfig";

const styles = css`
${commonCss}
${typography}
#navbar-container {
  padding: 2rem 1rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}
.navbar {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
}
.logo {
  display: flex;
  align-items: center;
}
.logo a {
 display: flex;
 flex-direction: row;
 align-items: center;
}
.impel-logo {
  margin-right: 25px;
  height: 38px;
}
.xdc-logo {
  margin-top: 6px;
  height: 53px;
}
.menu-panel {
  display: none;
  flex-direction: column;
  align-items: flex-start;
  order: 1;
  flex-basis: 100%;
  gap: 2rem;
  position: absolute;
  width: 100%;
  top: 5rem;
  left: 0;
  z-index: 100;
  background: ${colorsConfig.panelBg};
  padding: 2rem;
  box-shadow: ${colorsConfig.card} 0px 1rem 1rem -0.75rem;
}
.menu-panel.open {
  display: flex;
}
.menu {
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-around;
  margin: 0px;
  padding: 0px;
  list-style-type: none;
  gap: 1rem;
}
.menu-item {
  display: flex;
  align-items: center;
}
.menu-item:not(.button) a {
  color: ${colorsConfig.linkDark};
}
.menu-item:not(.button):hover a {
  color: ${colorsConfig.linkActive};
}
.menu-item.active {
  border-bottom: 2px solid ${colorsConfig.linkActive};
}
.menu-item-link {
  padding: 0.5rem;
}
.menu-toggle {
  display: flex;
}
/* Desktop menu */
@media all and (min-width: 960px) {
  .navbar {
    flex-wrap: nowrap;
  }
  .menu-panel {
    display: flex;
    order: 0;
    flex-direction: row;
    align-items: center;
    gap: unset;
    width: unset;
    position: unset;
    background: unset;
    padding: unset;
    box-shadow: unset;
  }
  .menu {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin: 0 auto;
    max-width: 504px;
  }
  .menu-toggle {
    display: none;
  }
}
.badge {
  cursor: pointer;
  background: ${colorsConfig.linkActive};
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
}
.one-digit {
  top: -5px;
  right: -8px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
}
.two-digits {
  top: -5px;
  right: -10px;
  width: 22px;
  height: 20px;
  border-radius: 50%;
}
.three-digits {
  top: -5px;
  right: -20px;
  width: 32px;
  height: 20px;
  border-radius: 45%;
}
`

@customElement('nav-bar')
export class NavBar extends LitElement {
  static override styles = styles;

  private userStore: UserStore;
  private transactionsStore: TransactionsStore
  private authService: AuthService;
  private routingService: RoutingService

  @query('.menu-panel')
  menuPanel: any;
  @query('.menu')
  menu: any;
  @queryAll('.menu-item')
  items: any;
  @queryAll('.menu-item-link')
  menuItemLinks: any;

  @property()
  menuOpen = false;

  @state()
  menuVisible = false;

  constructor() {
    super();
    this.userStore = userStore;
    this.transactionsStore = transactionsStore;
    this.authService = authService;
    this.routingService = routingService;
  }

  protected override update(changedProperties: PropertyValues) {
    this.highlightActiveMenuItem();
    super.update(changedProperties);
  }

  override connectedCallback() {
    super.connectedCallback();
    this.userStore.bindComponentToPropertyChanges(this);
    this.transactionsStore.bindComponentToPropertyChanges(this);
    this.routingService.addLocationChangesListener(
        this,
        this.locationChangeHandler.bind(this)
    );
  }

  override async disconnectedCallback() {
    super.disconnectedCallback();
    this.userStore.unbindComponentFromPropertyChanges(this);
    this.transactionsStore.unbindComponentFromPropertyChanges(this);
    this.routingService.removeLocationChangesListener(this);
  }

  async locationChangeHandler(location: Router.Location) {
    this.highlightActiveMenuItem();

    const routeName = location.route?.name as RouteName
    this.menuVisible = !(routeName === 'unauthorized')
  }

  /**
   * Highlights active menu item depending on URL path.
   * Subpaths will also activate menu items, e.g. both /foo and /foo/bar will activate menu
   * item foo.
   */
  private highlightActiveMenuItem() {
    if (this.menuItemLinks?.length) {
      this.menuItemLinks?.forEach((it: any) => {
        it.parentElement.classList.remove('active');

        const metaUrl = it.getAttribute('meta-url')
        if (window.location.href.includes(`/${metaUrl}`)) {
          it.parentElement.classList.add('active');
        }
      });
    }
  }

  private selectMenuItem(routeName: RouteName) {
    this.closeMenuPanel();
    this.routingService.navigateToRoute(routeName);
  }

  private profileMenuItemSelected(event: MenuItemSelectedEvent) {
    const menuItemName = event.detail;
    switch (menuItemName) {
      case('Logout'): {
        this.closeMenuPanel();
        this.authService.logout();
        break;
      }
      case('Settings'): {
        this.closeMenuPanel();
        this.routingService.navigateToRoute('settings');
        break;
      }
    }
  }

  private toggleMenuPanel() {
    if (this.menuOpen) {
      this.closeMenuPanel();
    } else {
      this.menuOpen = true;
      this.menuPanel.classList.add('open');
    }
  }

  private closeMenuPanel() {
    this.menuOpen = false;
    this.menuPanel.classList.remove('open');
  }

  override render() {
    return html`
        <div id="navbar-container">
            <nav class="navbar">
                <div class="logo">
                    <a href="https://www.impel.global/" target="_blank">
                        <img class="impel-logo" src="${config.baseUrl}/assets/impel-logo.svg"/>
                    </a>
                    <a href="https://www.xdc.org/" target="_blank">
                        <img class="xdc-logo" src="${config.baseUrl}/assets/xdc-logo.png"/>
                    </a>
                </div>
                ${this.renderMenuPanel()}
            </nav>
        </div>
    `;
  }

  renderMenuPanel() {
    const unapprovedCountDigits = this.transactionsStore.unapprovedCount === 0 ? 0
        : this.transactionsStore.unapprovedCount.toString().length;
    const badgeClasses = {
      'badge': true,
      'hidden': unapprovedCountDigits === 0,
      'one-digit': unapprovedCountDigits === 1,
      'two-digits': unapprovedCountDigits === 2,
      'three-digits': unapprovedCountDigits > 2,
    };

    const menu = html`
        <ul class="menu">
            <li class="menu-item">
                <a class="menu-item-link menu-item-text" meta-url="transfer-request"
                   @click=${() => this.selectMenuItem('transfer-request')}>
                    Transfer Request
                </a>
            </li>
            <li class="menu-item " style="display: flex; flex-direction: row; gap: 4px;">
                <a class="menu-item-link menu-item-text" meta-url="transactions"
                   @click=${() => this.selectMenuItem('transactions')}>
                    Transactions
                </a>
                <span @click="${() => this.selectMenuItem('unapproved')}"
                      class=${classMap(badgeClasses)}>${this.transactionsStore.unapprovedCount}</span>
            </li>
            <li class="menu-item">
                <a class="menu-item-link menu-item-text" meta-url="reporting"
                   @click=${() => this.selectMenuItem('reporting')}>
                    Reporting
                </a>
            </li>
        </ul>
    `;

    return this.menuVisible
        ? html`
                <div class="menu-panel">
                    ${menu}
                    <user-avatar class="user-avatar" .user=${this.userStore.user}
                                 @menuItemSelectedEvent=${this.profileMenuItemSelected}
                    ></user-avatar>
                </div>
                <div class="menu-toggle">
                    <ui5-button
                            icon="${this.menuOpen ? 'decline' : 'menu2'}"
                            design="Transparent"
                            @click=${() => this.toggleMenuPanel()}>
                    </ui5-button>
                </div>
        `
        : '';
  }
}


