import {html, LitElement} from 'lit';
import {customElement} from 'lit/decorators.js';
import { RoutingService, routingService } from '../routing/RoutingService';
import {defaultView} from "../routing/routeConfig";

@customElement('not-found-view')
export class NotFoundView extends LitElement {

  private readonly routingService: RoutingService;

  constructor(){
    super();
    this.routingService = routingService;
  }

  override render() {

    this.routingService.navigateToRoute(defaultView);

    return html`
      <p>Page not found</p>
      <slot></slot>
    `;
  }
}
