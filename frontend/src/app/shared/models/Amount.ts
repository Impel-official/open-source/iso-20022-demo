

export class Amount {
  // TODO: add big number
  private readonly amount: number;
  
  // TODO: currency not used
  private constructor(amount: string, private currency: string) {
    this.amount = parseFloat(amount);
  }
  
  static from(amount: string, currency: string): Amount {
    return new Amount(amount, currency);
  }
  
  getFormatted(decimals = 2): string {
    const detectedBrowserTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const detectedBrowserLocale = navigator.languages[0];
    const formatter = new Intl.NumberFormat(
      detectedBrowserLocale, 
      {
        style: 'decimal',
        //currency: this.currency,
        minimumFractionDigits: decimals,
        maximumFractionDigits: decimals,
    });

    return formatter.format(this.amount);
  }

  getAmount(): number{
    return this.amount
  }
  getCurrency(): string{
    return this.currency
  }
}