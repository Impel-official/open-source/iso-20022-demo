package io.lab577.xinfin.iso20022.demo.domain.entity

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class AbstractAuditableEntity(
    id: UUID? = null,

    @CreatedDate
    @Column(name = "created", nullable = false)
    var created: OffsetDateTime? = null,

    @LastModifiedDate
    @Column(name = "updated", nullable = false)
    var updated: OffsetDateTime? = null

) : AbstractBaseEntity(id)
