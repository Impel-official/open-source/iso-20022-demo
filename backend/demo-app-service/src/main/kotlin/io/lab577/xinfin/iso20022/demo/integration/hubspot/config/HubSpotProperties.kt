package io.lab577.xinfin.iso20022.demo.integration.hubspot.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "app.integration.hubspot")
data class HubSpotProperties (
  val enabled: Boolean,
  val url: String,
  val apiKey: String,
  val idPropertyName: String,
  val cacheTtlMinutes: Long,
  val retryMaxAttempts: Long
)
