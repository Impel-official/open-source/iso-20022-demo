import {ProgressUpdate} from './ProgressUpdate';

export type ProgressUpdateState = 'Pending' | 'Failed' | 'Success';

export class ProgressUpdates {
  private readonly progressUpdates: Array<ProgressUpdate>;
  private readonly state: ProgressUpdateState;

  static empty() {
    return new ProgressUpdates([]);
  }
  
  constructor(
    initialMessages: Array<ProgressUpdate> = [],
    initialState: ProgressUpdateState = 'Pending'
  ) {
    this.progressUpdates = initialMessages;
    this.state = initialState;
  }
  
  addUpdate(progressUpdate: ProgressUpdate): ProgressUpdates {
    if(this.hasProgressUpdate(progressUpdate)) {
      throw new Error('Progress update already exists')
    }
    return new ProgressUpdates(
      [...this.progressUpdates, progressUpdate],
      this.state
    );
  }
  
  changeUpdate(progressUpdate: ProgressUpdate): ProgressUpdates {
    if(!this.hasProgressUpdate(progressUpdate)) {
      throw new Error('Progress update does not exist')
    }
    const progressUpdateArrayIndex = 
      this.progressUpdates.findIndex(it=>it.id === progressUpdate.id);
     const newProcessUpdates = Object.assign(
       [], this.progressUpdates, {[progressUpdateArrayIndex]: progressUpdate}
     );
     return new ProgressUpdates(newProcessUpdates, this.state);
  }
  
  emptyUpdates(): ProgressUpdates {
    return ProgressUpdates.empty();
  }
  
  changeState(state: ProgressUpdateState) {
    return new ProgressUpdates([...this.progressUpdates], state);
  }
  
  getProgressUpdates() {
    return [...this.progressUpdates]
  }
  
  getState() {
    return this.state;
  }

  private hasProgressUpdate(progressUpdate: ProgressUpdate): boolean {
    return this.progressUpdates.find(it => it.equals(progressUpdate)) !== undefined;
  }
}