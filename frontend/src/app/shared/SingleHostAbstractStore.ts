import {ReactiveController, ReactiveControllerHost} from 'lit';
import {AppConfig} from './config';

export abstract class SingleHostAbstractStore<T extends ReactiveControllerHost> 
  implements ReactiveController 
{
  protected host: T | undefined;

  constructor(protected config: AppConfig) {}
  
  bindComponentToPropertyChanges(host: T) {
    host.addController(this);
    this.host = host;
  }
  
  unbindComponentFromPropertyChanges(host: T) {
    this.host = undefined;
  }
  
  updatePropertiesToBoundHost(){
    if(this.host) this.host.requestUpdate();
  }

  hostConnected(): void {}
  hostDisconnected(): void {}
  hostUpdate(): void {}
  hostUpdated(): void {}
}
