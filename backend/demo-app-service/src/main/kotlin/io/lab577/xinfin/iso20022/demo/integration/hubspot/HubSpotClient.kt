package io.lab577.xinfin.iso20022.demo.integration.hubspot

import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.FindContactRequestBody
import io.lab577.xinfin.iso20022.demo.integration.hubspot.model.FindContactResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient(name = "hubspot", url = "\${app.integration.hubspot.url}")
interface HubSpotClient {

    @PostMapping("/crm/v3/objects/contacts/search")
    fun findContact(
        @RequestBody requestBody: FindContactRequestBody,
        @RequestHeader(value = "Authorization", required = true) authorizationHeader: String
    ): FindContactResponse
}
