package io.lab577.xinfin.iso20022.demo.mapper

import io.lab577.xinfin.iso20022.common.lib.iso.support.createPacs002
import io.lab577.xinfin.iso20022.common.lib.iso.support.createPacs009
import io.lab577.xinfin.iso20022.demo.pacs.PacsIdBuilder
import io.lab577.xinfin.iso20022.demo.transactions.model.OutgoingTransactionMessage
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class PacsMapper(
    @Value("\${app.approvalTimeoutSeconds}")
    private val approvalTimeoutSeconds: Long
) {
    fun mapToPacs009(transferRequest: OutgoingTransactionMessage.TransferRequest): String {
        val messageDate = transferRequest.requestDate ?: OffsetDateTime.now()
        val validUntil = transferRequest.validUntil ?: messageDate.plusSeconds(approvalTimeoutSeconds)

        return createPacs009(
            senderBic = transferRequest.senderBic,
            recipientBic = transferRequest.recipientBic,
            senderAccount = transferRequest.senderAccount,
            recipientAccount = transferRequest.recipientAccount,
            amount = transferRequest.amount,
            currency = transferRequest.currency,
            referenceInfo = transferRequest.referenceInfo ?: PacsIdBuilder.randomId(),
            messageDate = messageDate,
            validUntil = validUntil,
            endToEndId = transferRequest.endToEndId,
        )
    }

    fun mapToPacs002(transferResponse: OutgoingTransactionMessage.TransferResponse): String {
        return createPacs002(
            senderBic = transferResponse.senderBic,
            recipientBic = transferResponse.recipientBic,
            amount = transferResponse.amount,
            currency = transferResponse.currency,
            endToEndId = transferResponse.endToEndId,
            approve = !transferResponse.reject,
        )
    }
}
