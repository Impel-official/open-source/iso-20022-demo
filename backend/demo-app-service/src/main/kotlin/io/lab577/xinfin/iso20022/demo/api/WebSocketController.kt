package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.config.WebSocketConfig.Companion.WEBSOCKET_TOPIC_TRANSACTION_METRICS
import io.lab577.xinfin.iso20022.demo.metrics.TransactionAllMetrics
import io.lab577.xinfin.iso20022.demo.service.TransactionMetricsService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.messaging.handler.annotation.MessageExceptionHandler
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.messaging.simp.annotation.SubscribeMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class WebSocketController(
    val transactionMetricsService: TransactionMetricsService
) {

    @SubscribeMapping(WEBSOCKET_TOPIC_TRANSACTION_METRICS)
    @Operation(summary = "Get, and subscribe to, transaction metrics",
        description = "Get, and subscribe to, the collection of monthly/yearly transaction metrics to date as a list of cards. ")
    fun getAllMetrics(): TransactionAllMetrics = transactionMetricsService.getAllMetrics()

    @MessageExceptionHandler
    @SendToUser(destinations = ["/queue/notifications"], broadcast = false)
    fun sendNotification(exception: Throwable): String? {
        return exception.message
    }

    @MessageExceptionHandler
    @SendToUser(destinations = ["/queue/errors"], broadcast = false)
    fun handleException(exception: Throwable): String? {
        return exception.message
    }
}
