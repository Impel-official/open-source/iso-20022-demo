import {customElement, property, query, state} from 'lit/decorators.js';
import {repeat} from 'lit/directives/repeat.js';
import {css, html, LitElement} from 'lit';
import {Transaction} from '../../model/Transaction';
import {RoutingService, routingService} from '../../../shared/routing/RoutingService';
import {cardCss, commonCss, kvTableCss, tableCss, typography} from "../../../shared/config/styles";
import {ReferenceDataStore, referenceDataStore} from "../../ReferenceDataStore";
import {TransactionDtoStatusEnum} from "../../../shared/api";
import {formatDateTime} from "../../../shared/date-utils";
import {TransactionSummary} from '../../model/TransactionSummary';
import {restAdapter, RestAdapter} from "../../../shared/rest/RestAdapter";
import {getCollateralText} from "../../../shared/utils";

const styles = css`
${commonCss}
${cardCss}
${tableCss}
${kvTableCss}
${typography}

#transaction-details-container {
  display: flex;
  flex-direction: column;
  width: 100%;
}

.dialog-footer {
  padding: 1rem;
}
`;

@customElement('transaction-details')
export class TransactionDetails extends LitElement {
  static override styles = styles;

  @property()
  transaction?: Transaction;

  @query('#detailsTable')
  private detailsTable: any;
  @query('#transactionMessageDialog')
  private dialog: any;

  @state()
  private transactionMessageXML: string = '';

  private routingService: RoutingService;
  private restAdapter: RestAdapter;
  private referenceDataStore: ReferenceDataStore;

  constructor() {
    super();
    this.routingService = routingService;
    this.restAdapter = restAdapter;
    this.referenceDataStore = referenceDataStore;
  }


  override connectedCallback() {
    super.connectedCallback();
    this.referenceDataStore.bindComponentToPropertyChanges(this);
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    this.referenceDataStore.unbindComponentFromPropertyChanges(this);
  }

  private async openTransactionMessageDialog(selectedMessageId: string) {
    this.transactionMessageXML = await restAdapter.getMessageXML(selectedMessageId);
    this.dialog.show();
  }

  private closeTransactionMessageDialog() {
    this.dialog.close();
  }

  override render() {
    if (this.transaction) {

      const txSummary = TransactionSummary.from(this.transaction);
      const currency = this.referenceDataStore.currencies.find(currency => currency.code === this.transaction?.currency);
      const currencySymbol = currency?.symbol || '?';
      const currencyCode = this.transaction?.currency;

      const collateralText = getCollateralText(this.transaction.collateral, currencySymbol);
      const createdBy = this.referenceDataStore.parties
          .find(it => it.bic === this.transaction?.senderBic)
          ?.name
      const reviewedBy = this.transaction?.status === TransactionDtoStatusEnum.Approved || this.transaction?.status === TransactionDtoStatusEnum.Rejected
          ? this.referenceDataStore.parties.find(it => it.bic === this.transaction?.recipientBic)?.name
          : '-';

      const fromIban = this.referenceDataStore.accounts
          .find(it => it.iban === this.transaction?.senderAccount)
          ?.holder
      const toIban = this.referenceDataStore.accounts
          .find(it => it.iban === this.transaction?.recipientAccount)
          ?.holder


      return html`
          <div id="transaction-details-container">
              <div class="h5 mt-2">Transaction Details</div>
              <table id="detailsTable" class="kv-table">
                  <tr>
                      <td class="label vertical-th">Status</td>
                      <td class="value">${txSummary.buildStatusLabel()}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Created By</td>
                      <td class="value">${createdBy}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Approved/Rejected By</td>
                      <td class="value">${reviewedBy}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Transaction ID</td>
                      <td class="value">${this.transaction.endToEndId}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Transfer Request Date</td>
                      <td class="value">${formatDateTime(this.transaction.requestDate).replace(',', '')}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Valid Until</td>
                      <td class="value">${formatDateTime(this.transaction.validUntil).replace(',', '')}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">From BIC/IBAN</td>
                      <td class="value">${this.transaction.senderBic} / ${fromIban}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">To BIC/IBAN</td>
                      <td class="value">${this.transaction.recipientBic} / ${toIban}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Special Instructions</td>
                      <td class="value">${this.transaction.referenceInfo}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Amount</td>
                      <td class="value">${currencySymbol}${this.transaction.amount2.getFormatted()} ${currencyCode}</td>
                  </tr>
                  <tr>
                      <td class="label vertical-th">Collateral</td>
                      <td class="value">${collateralText}</td>
                  </tr>
              </table>
              <div class="h5 mt-2">Message Details with Raw Data</div>
              <ui5-table no-data-text="No Data" mode="None">
                  <ui5-table-column slot="columns">
                      <div>ID</div>
                  </ui5-table-column>
                  <ui5-table-column slot="columns">
                      <div>PACS Type</div>
                  </ui5-table-column>
                  <ui5-table-column slot="columns">
                      <div>Date</div>
                  </ui5-table-column>
                  <ui5-table-column slot="columns">
                      <div>Direction</div>
                  </ui5-table-column>
                  <ui5-table-column slot="columns">
                      <div>XML</div>
                  </ui5-table-column>
                  ${repeat(this.transaction.messages, msg => msg.id, msg => {
                      return html`
                          <ui5-table-row data-id=${msg.id}>
                              <ui5-table-cell>${msg.id}</ui5-table-cell>
                              <ui5-table-cell>${msg.type}</ui5-table-cell>
                              <ui5-table-cell>${msg.messageDate2.getFormatted()}</ui5-table-cell>
                              <ui5-table-cell>${msg.direction2}</ui5-table-cell>
                              <ui5-table-cell>
                                  <ui5-link class="link" @click=${() => this.openTransactionMessageDialog(msg.id)}>
                                      <ui5-icon class="link" name="sap-icon://message-information"></ui5-icon>
                                  </ui5-link>
                              </ui5-table-cell>
                          </ui5-table-row>
                      `;
                  })}
              </ui5-table>
              <ui5-dialog id="transactionMessageDialog">
                  <pre class="code">${this.transactionMessageXML}</pre>
                  <div slot="footer" class="dialog-footer">
                      <ui5-button design="Emphasized" @click=${this.closeTransactionMessageDialog}>Close
                      </ui5-button>
                  </div>
              </ui5-dialog>
          </div>
      `
    }
  }
}
