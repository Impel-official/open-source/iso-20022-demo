package io.lab577.xinfin.iso20022.demo.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class SecurityHelper {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(SecurityHelper::class.java)

        const val SYSTEM_USER = "SYSTEM"
    }

    fun getCurrentUserName(): String {
        return when (val authentication = SecurityContextHolder.getContext().authentication) {
            null -> SYSTEM_USER
            else -> authentication.name
        }
    }
}
