package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.integration.kucoin.KuCoinClient
import io.lab577.xinfin.iso20022.demo.integration.kucoin.model.Price
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/kucoin")
class KuCoinController(
    val kuCoinClient: KuCoinClient
) {

    @GetMapping("price")
    fun getPrices(
        @RequestParam(required = true)
        base: String,
        @RequestParam(required = false, defaultValue = "XDC")
        currencies: Set<String>,
    ): Price {
        return kuCoinClient.getPrices(base, currencies)
    }

}
