import {customElement} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {transactionsStore, TransactionsStore} from '../TransactionsStore';
import {referenceDataStore, ReferenceDataStore} from '../ReferenceDataStore';
import {TransactionsTableFilterEvent} from './table/TransactionsTableFilters';
import {TransactionSelectedEvent} from './table/TransactionsTable';
import {RoutingService, routingService} from '../../shared/routing/RoutingService';
import {Router} from '@vaadin/router';


const styles = css`
.filters {
  margin-bottom: 2em;
  max-width:1140px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
}
`

@customElement('transactions-view')
export class TransactionsView extends LitElement {
  static override styles = styles;
  
  private transactionsStore: TransactionsStore;
  private referenceDataStore: ReferenceDataStore;
  private routingService: RoutingService;
  
  constructor() {
    super();
    // TODO: this needs to provided as constructor parameter
    this.transactionsStore = transactionsStore;
    this.referenceDataStore = referenceDataStore;
    this.routingService = routingService;
  }

  override async connectedCallback() {
    super.connectedCallback();
    this.transactionsStore.bindComponentToPropertyChanges(this);
    this.referenceDataStore.bindComponentToPropertyChanges(this);
    this.transactionsStore.setTransactionsTableFilterFromQueryParams();
    this.transactionsStore.setTransactionsPagingFromQueryParams();
    await this.transactionsStore.getFilteredTransactionsTable();
    this.transactionsStore.addLocationChangesListener(
      this,
      this.locationChangeHandler.bind(this)
    );
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    this.transactionsStore.unbindComponentFromPropertyChanges(this);
    this.referenceDataStore.unbindComponentFromPropertyChanges(this);
    this.transactionsStore.removeLocationChangesListener(this);
  }

  async setTransactionsTableFilter(event: TransactionsTableFilterEvent) {
    if(event.detail.isSelected) {
      this.transactionsStore.setFilter(event.detail.filter);
    } else {
      this.transactionsStore.setTransactionsTableFilter(event.detail.filter)
    }
  }
  
  async transactionSelected(event: TransactionSelectedEvent) {
    const transactionDetailsUrl = 
      this.routingService.urlForName('transaction-details', {id: event.detail.id});
    this.routingService.navigate(transactionDetailsUrl);
  }

  private async locationChangeHandler(location: Router.Location) {
    if (location?.route?.name == 'transactions') {
      this.transactionsStore.setTransactionsPagingFromQueryParams();
      this.transactionsStore.setTransactionsTableFilterFromQueryParams();
      await this.transactionsStore.getFilteredTransactionsTable();
    }
  }
  
  private togglePanel(){
    this.transactionsStore.isTransactionFiltersPanelOpen = 
      !this.transactionsStore.isTransactionFiltersPanelOpen;
  }
  
  override render() {
    return html`
      <transactions-table-filters class="filters"
              .currencies=${this.referenceDataStore.currencies}
              .states=${this.referenceDataStore.states}
              .bics=${this.referenceDataStore.parties}
              .transactionsTableFilter=${this.transactionsStore.transactionsTableFilter}
              @filter=${this.setTransactionsTableFilter}
      ></transactions-table-filters>
      <transactions-table
              .transactionsTable=${this.transactionsStore.transactionsTable}
              @transactionSelected=${this.transactionSelected}
      >
      </transactions-table>
    `
  }
}
