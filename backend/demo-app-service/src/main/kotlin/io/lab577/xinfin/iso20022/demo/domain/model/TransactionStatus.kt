package io.lab577.xinfin.iso20022.demo.domain.model

import io.lab577.xinfin.iso20022.demo.pacs.PacsOverview

enum class TransactionProofStatus {
    Outdated, None, Signing, Signed;
}
enum class TransactionStatus {
    New, Requested, Received, Approved, Rejected, Expired, Failed;

    companion object {
        /**
         * Helper for selecting the current [TransactionStatus]
         * based on [TransactionMessageDirection] and [PacsType]
         */
        fun getStatus(
            overview: PacsOverview,
            transactionMessageDirection: TransactionMessageDirection
        ): TransactionStatus = overview.statusOverride ?: when (transactionMessageDirection) {
            TransactionMessageDirection.INBOX -> getInboxStatus(overview.type)
            TransactionMessageDirection.OUTBOX -> getOutboxStatus(overview.type)
        }

        /**  Helper for selecting inbox [TransactionStatus] for the given [PacsType] */
        private fun getInboxStatus(pacsType: PacsType): TransactionStatus = when (pacsType) {
            PacsType.PACS_002 -> Approved
            PacsType.PACS_009 -> Received
            else -> throw IllegalStateException("UNo status mapped for inbox $pacsType")
        }

        /**  Helper for selecting outbox [TransactionStatus] for the given [PacsType] */
        private fun getOutboxStatus(pacsType: PacsType): TransactionStatus = when (pacsType) {
            PacsType.PACS_002 -> Approved
            PacsType.PACS_009 -> Requested
            else -> throw IllegalStateException("UNo status mapped for inbox $pacsType")
        }
    }
}
