/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import {exists, mapValues} from '../runtime';
/**
 *
 * @export
 * @interface TransactionMessageStatsData
 */
export interface TransactionMessageStatsData {
  /**
   *
   * @type {number}
   * @memberof TransactionMessageStatsData
   */
  count: number;
}

export function TransactionMessageStatsDataFromJSON(
  json: any
): TransactionMessageStatsData {
  return TransactionMessageStatsDataFromJSONTyped(json, false);
}

export function TransactionMessageStatsDataFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean
): TransactionMessageStatsData {
  if (json === undefined || json === null) {
    return json;
  }
  return {
    count: json['count'],
  };
}

export function TransactionMessageStatsDataToJSON(
  value?: TransactionMessageStatsData | null
): any {
  if (value === undefined) {
    return undefined;
  }
  if (value === null) {
    return null;
  }
  return {
    count: value.count,
  };
}
