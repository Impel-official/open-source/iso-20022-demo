package io.lab577.xinfin.iso20022.demo.domain.model

enum class TransactionMessageCollateralType {
    NATIVE, ERC20
}
