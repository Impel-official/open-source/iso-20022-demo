package io.lab577.xinfin.iso20022.demo.domain.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.PagingAndSortingRepository

@NoRepositoryBean
interface BaseRepository<T, ID> : PagingAndSortingRepository<T, ID>, JpaSpecificationExecutor<T>
