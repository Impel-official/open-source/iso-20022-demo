import {TransactionSummary} from './TransactionSummary';

export class TransactionsTable {
  constructor(
    public transactions: Array<TransactionSummary>,
    public totalPages: number,
    public totalElements: number,
    public pageNumber: number,
    public pageSize: number,
  ) {
    this.validate()
  }

  private validate() {}

  static empty(): TransactionsTable {
    return new TransactionsTable([], 0, 0, 0, 10);
  }

  findTransactionSummaryById(id: string): TransactionSummary | undefined {
    return this.transactions.find(it=>it.id === id);
  }

  upsertTransaction(transaction: TransactionSummary): TransactionsTable {
    const clonedTransactionTable = this.clone();
    const lastIndex = clonedTransactionTable.transactions.length - 1;
    let isUpdated = false;
    for (let i=0; i <= lastIndex; i++) {
      const loopTransaction = clonedTransactionTable.transactions[i];
      if(loopTransaction.equals(transaction)) {
        clonedTransactionTable.transactions[i] = transaction;
        isUpdated = true;
      }
    }
    if(!isUpdated) {
      if(this.transactions.length >= this.pageSize) {
        // page is full
        clonedTransactionTable.transactions.unshift(transaction);
        clonedTransactionTable.transactions.pop();
      } else {
        clonedTransactionTable.transactions.unshift(transaction);
      }
    }

    return clonedTransactionTable;
  }

  private clone(): TransactionsTable {
    return new TransactionsTable(
      [...this.transactions],
      this.totalPages,
      this.totalElements,
      this.pageNumber,
      this.pageSize,
    )
  }
}