import {customElement, property, query, queryAll} from  'lit/decorators.js'
import {css, html, LitElement} from 'lit';
import {TransactionsTable} from '../../model/TransactionsTable';
import {TransactionsTablePaging} from '../../model/TransactionsTablePaging';
import {TransactionSummary} from "../../model/TransactionSummary";
import {colorsConfig, commonCss, formCss, mobileCss } from "../../../shared/config/styles";
import {PropertyValues} from "@lit/reactive-element/development/reactive-element";
import {styleHelper} from "../../../shared/config/StyleHelper";
export type ChangePagingEvent = CustomEvent<TransactionsTablePaging>;
export type AcceptOrRejectTransactionsEvent = CustomEvent<boolean>;
import { config } from "../../../shared/config";

const styles = css`
${commonCss}
${formCss}

  ul.menu li.item{
    margin-left: 4px;
    margin-right: 4px;
    padding: 4px 0px;
  }

  ul.menu li.item:first-of-type{
    margin-right: 12px;
  }

  ul.menu li.item:last-of-type{
    margin-left: 12px;
  }

  ul.menu li.item.active,
  ul.menu li.item.enabled {
    border-bottom: 2px solid ${colorsConfig.linkActive};
  }
   ul.menu li.item.disabled {
    border-bottom: 2px solid ${colorsConfig.linkDisabled};
  }

  .actions-area {
     margin-right: 2em;
  }
  #multiTxActionsBusyIndicator{
    margin: 8px 0px;
  }
  #multiTxActionsBusyIndicator ui5-button{
    margin-right: 10px;
  }

  .paging-area {
     flex-flow: row wrap-reverse;
     flex-grow: 1;
     align-items: center;
  }

  .paging-subarea{
     display: flex;
     flex-direction: row;
     justify-content: flex-end;
     align-items: center;
  }
  .paging-area .per-page {
    margin: 16px 0px 16px 2em;
  }

  .menu {
    list-style-type: none;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding-inline-start: 0; // reset some default value
  }

  @media (max-width: 600px) {

    .actions-container {
      row-gap: 20px;
    }

    .actions-container > .action-button {
      height: 60px;
    }

  }

  ${mobileCss}

`

@customElement('table-paging')
export class TablePaging extends LitElement {
  static override styles = styles;

  @property()
  transactionsTable: TransactionsTable = TransactionsTable.empty();
  @property()
  private selectedTransactions: Array<TransactionSummary> = [];
  @property()
  busy = false;

  @property()
  showActionButtonsOnMobile = true;

  @query('#pageSizeSelect')
  private pageSizeInput: any;
  @queryAll('ui5-input, ui5-step-input, ui5-daterange-picker, ui5-select, ui5-textarea, input-suggestion')
  formFields: HTMLCollection | undefined;


  protected override update(changedProperties: PropertyValues) {
    super.update(changedProperties);
    if(this.formFields?.length) {
      styleHelper.maybeStyleFormFields(this.formFields);
    }
  }

  private isFirst(pgNumber?: number): boolean {
    const pageNumber = pgNumber !== undefined ? pgNumber : this.transactionsTable.pageNumber;
    return pageNumber === 0;
  }

  private isLast(pgNumber?: number): boolean {
    const pageNumber = pgNumber || this.transactionsTable.pageNumber;
    return this.transactionsTable.totalPages == 0 || pageNumber === this.transactionsTable.totalPages - 1;
  }

  private async firstPage() {
    if(this.isFirst()) return;
    this.changePaging(new TransactionsTablePaging(1, this.transactionsTable.pageSize));
  }

  private async previousPage(){
    if(this.isFirst()) return;
    this.changePaging(
      new TransactionsTablePaging(
        this.transactionsTable.pageNumber - 1,
        this.transactionsTable.pageSize
      )
    )
  }

  private async nextPage() {
    if(this.isLast()) return;
    this.changePaging(
      new TransactionsTablePaging(
        this.transactionsTable.pageNumber + 1,
        this.transactionsTable.pageSize
      )
    )
  }

  private async lastPage(){
    if(this.isLast()) return;
    this.changePaging(
      new TransactionsTablePaging(
        this.transactionsTable.totalPages,
        this.transactionsTable.pageSize
      )
    );
  }

  changePageNumber(pageNumber: number, pageSize?: number){
    const pgSize = pageSize || parseInt(this.pageSizeInput.selectedOption.value);
    this.changePaging(new TransactionsTablePaging(pageNumber, pgSize));
  }

  private changePaging(e: CustomEvent | TransactionsTablePaging) {
    let transactionsTablePaging;
    if(e instanceof TransactionsTablePaging) {
      transactionsTablePaging = e;
    } else {
      const pageNumber = 0;
      const pageSize = parseInt(this.pageSizeInput.selectedOption.value);
      transactionsTablePaging = new TransactionsTablePaging(pageNumber, pageSize)
    }
    this.hasPagingChanged(transactionsTablePaging);
    const eventOptions = {
      detail: transactionsTablePaging,
      bubbles: true,
      composed: true
    };
    const changePagingEvent: ChangePagingEvent =
      new CustomEvent<TransactionsTablePaging>('changePagingEvent', eventOptions);
    this.dispatchEvent(changePagingEvent);
  }

  private acceptOrReject(e: CustomEvent, accept: boolean) {
    const event: AcceptOrRejectTransactionsEvent =
        new CustomEvent<boolean>('acceptOrRejectEvent', {
          detail: accept,
          bubbles: true,
          composed: true
        });
    this.dispatchEvent(event);
  }
  private accept(e: CustomEvent) {
    this.acceptOrReject(e, true)
  }

  private reject(e: CustomEvent) {
    this.acceptOrReject(e, false)
  }

  private hasPagingChanged(paging: TransactionsTablePaging): boolean {
    return this.transactionsTable.pageNumber === paging.pageNumber &&
        this.transactionsTable.pageSize === paging.pageSize;
  }

  pageNumberVisible(page: number): boolean {
    if(this.transactionsTable.totalPages == 1) return false;
    const maxPageNumbersBackwards = this.transactionsTable.pageNumber - 6;
    const maxPageNumbersForward = this.transactionsTable.pageNumber + 6;

    return maxPageNumbersBackwards < page && page < maxPageNumbersForward || this.isFirst(page) || this.isLast(page);

  }

  override render() {
    const { totalPages, pageSize, totalElements, transactions } = this.transactionsTable;

    let pageNumber = 0;
    const links = [];

    // If there are elements for the given filters, but list came back empty, most likely it's due to bad routing
    // Fallback to the first page
    if(totalElements && !transactions.length){
      this.changePageNumber(0, pageSize);
    }

    // FIXME: We're iterating over (potentially) a big number.
    // Best to just pick the numbers we want to show and render accordingly
    while(pageNumber < totalPages && totalPages > 1){

      const currentPageIsVisible = this.pageNumberVisible(pageNumber);
      const currentPage = pageNumber;
      const content = currentPageIsVisible ? html`<a @click=${() => this.changePageNumber(currentPage)} class="link">${pageNumber + 1}</a>` : html`...`

      links.push(html`<li class="item mobile-hidden ${pageNumber == this.transactionsTable.pageNumber ? ' active' : ''}">${content}</li>`);

      // If we're showing a placeholder for the pages we're not displaying on the paginator,
      // omit the following values until we need to show a page number again
      while(!this.pageNumberVisible(++pageNumber) && !currentPageIsVisible && pageNumber < totalPages){
        // Nothing to do here. Move along.
      }

    }

    const showAcceptRejectButtons = config.featureFlags.transactionsView.showAcceptRejectButtons;

    return html`
      <div class="paging-area flex ${ showAcceptRejectButtons ? "flex-content-space-between" : "flex-content-end"}">
        <div id="multiTxActionsBusyIndicator" class="${!this.showActionButtonsOnMobile ? 'mobile-hidden' : ''} actions-container justify-self-start mobile-flex-grow flex flex-row mobile-flex-wrap mobile-flex-one-item-per-row ${ showAcceptRejectButtons ? "" : "hidden" }" size="Large" .active=${this.busy} >
            <ui5-button design="Emphasized" class="action-button mobile-flex-grow mobile-flex-one-item-per-row" .disabled=${this.selectedTransactions.length == 0 || this.busy} @click=${this.accept}>Approve</ui5-button>
            <ui5-button design="Default" class="action-button mobile-flex-grow mobile-flex-one-item-per-row" .disabled=${this.selectedTransactions.length == 0 || this.busy} @click=${this.reject}>Reject</ui5-button>
        </div>
        <div class="flex">
          <div class="paging-paginator justify-self-end mobile-justify-self-start mobile-flex-grow mobile-flex-one-item-per-row">
            <ul class="menu">
              <li class=${this.isFirst() ? 'item disabled' : 'item enabled'}>
                <a @click=${() => this.previousPage()}
                   class=${this.isFirst() ? 'link disabled' : 'link enabled'}
                >< Previous page</a>
              </li>
                ${links}
              <li class=${this.isLast() ? 'item disabled' : 'item enabled'}>
                <a @click=${() => this.nextPage()}
                   class=${this.isLast() ? 'link disabled' : 'link enabled'}
                >Next page ></a>
              </li>
            </ul>
          </div>
          <div class="paging-subarea justify-self-end mobile-hidden mobile-flex-grow">
            <div class="per-page">
              <ui5-select @change=${this.changePaging} id="pageSizeSelect" class="out-of-card">
                <ui5-option value="1"   .selected="${pageSize == 1}">1 Items</ui5-option>
                <ui5-option value="10"  .selected="${pageSize == 10}">10 Items</ui5-option>
                <ui5-option value="25"  .selected="${pageSize == 25}">25 Items</ui5-option>
                <ui5-option value="50"  .selected="${pageSize == 50}">50 Items</ui5-option>
                <ui5-option value="100" .selected="${pageSize == 100}">100 Items</ui5-option>
              </ui5-select>
            </div>
          </div>
        </div>
      </div>
    `
  }
}
