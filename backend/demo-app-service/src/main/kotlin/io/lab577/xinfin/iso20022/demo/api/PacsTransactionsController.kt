package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.api.dto.ReferenceDataDto
import io.lab577.xinfin.iso20022.demo.metrics.TransactionAllMetrics
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionReviewRequestBody
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionsReviewRequestBody
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionsReviewResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.http.ResponseEntity
import java.util.*

interface PacsTransactionsController {

    @Operation(summary = "Get transaction metrics")
    fun getAllMetrics(): TransactionAllMetrics

    @Operation(
        summary = "Get the reference data",
        description = "Get the reference data, i.e. currencies, statuses etc."
    )
    fun getReferenceData(): ReferenceDataDto

    @Operation(
        summary = "Approve or reject transactions.",
        description = "Approve or reject transactions, effectively sending the corresponding PACS message to the counterparty.",
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(schema = Schema(implementation = TransactionsReviewRequestBody::class))],
            required = true
        ),
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "Bad request", responseCode = "400"),
            ApiResponse(description = "Error", responseCode = "500")
        ]
    )
    fun review(
        transactionsReviewRequestBody: TransactionsReviewRequestBody
    ): ResponseEntity<TransactionsReviewResponse>

    @Operation(
        summary = "Approve or reject a transaction.",
        description = "Approve or reject a transaction, effectively sending the corresponding PACS message to the counterparty.",
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(schema = Schema(implementation = TransactionReviewRequestBody::class))],
            required = true
        ),
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "Bad request", responseCode = "400"),
            ApiResponse(description = "Error", responseCode = "500")
        ]
    )
    fun review(
        @Parameter(description = "ID of transaction", required = true)
        transactionId: UUID,
        transactionReviewRequestBody: TransactionReviewRequestBody
    ): ResponseEntity<Unit>
}
