import {css, html, LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {colorsConfig, commonCss} from "../config/styles";
import {AppConfig, config} from "../config";

const styles = css`
${commonCss}
#share-demo-experience-panel-container {
  position: fixed;
  bottom: 0%;
  left: 50%;
  transform: translateX(-50%);
  background-color: #ffffff;
  color: ${colorsConfig.label};
  border: 1px solid ${colorsConfig.link};
  display: flex;
  flex-direction: column;
  z-index: 1000;
  padding: 1rem;
  margin: 0.5rem auto;
  border-radius: 0.5rem;
  box-shadow: 0 0 0.75rem 0 ${colorsConfig.link};
  width: 98%;
  box-sizing: border-box;
}
.header-container {
  display: flex;
  flex-direction: row;
  align-items: baseline;
  justify-content: space-between;
  gap: 0.5rem;
}
.title-container {
  display: flex;
  flex-direction: column;
  align-items: baseline;
  gap: 0.5rem;
}
.title-container > .title {
  font-size: 1.25rem;
  font-weight: 400;
  margin-right: 0.5rem;
}
.title-container > .sub-title {
  font-size: 1rem;
  font-weight: 300;
}
.social-icons {
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  padding-top: 1rem;
}
/* Desktop menu */
@media all and (min-width: 960px) {
  #share-demo-experience-panel-container {
    position: absolute;
    bottom: unset;
    left: unset;
    transform: unset;
    top: 5rem;
    right: 1rem;
    gap: 0.5rem;
    padding: 1rem;
    margin: 3rem auto;
    width: 12.5rem;
  }
  .close-button {
    display: none;
  }
  .social-icons {
    padding-top: 1rem;
  }
}
`

const messagingContext = {
  text: `Impel offers ISO 20022 compliant financial messaging w/ optional collateral using the XDC Network – try their demo like I just did, you’ll love it too!\n\nTry the ISO 20022 demo here: https://impel.global\n\n`,
  email: {
    subject: `Try Impel's ISO 20022 Demo`
  },
  twitter: {
    hashTags: "XDCNetwork,ISO20022,Payments,XDC"
  }
}

@customElement('share-demo-experience-panel')
export class ShareDemoExperiencePanel extends LitElement {
  static override styles = styles;

  private config: AppConfig;

  @property()
  private closed = false;

  constructor() {
    super();
    this.config = config;
  }

  override render() {

    const url = encodeURIComponent(this.config.shareDemoExperience.url);
    const text = encodeURIComponent(messagingContext.text);
    const twitterHashTags = encodeURIComponent(messagingContext.twitter.hashTags);

    const hrefs = {
      linkedIn: `https://www.linkedin.com/sharing/share-offsite/?url=${url}`,
      email: `mailto:?subject=${messagingContext.email.subject}&body=${text}`,
      twitter: `https://twitter.com/share?hashtags=${twitterHashTags}&text=${text}`
    };

    return html`
        <div id="share-demo-experience-panel-container" class="${this.closed ? 'hidden' : ''}">
            </ui5-button>
            <div class="header-container">
                <div class="title-container">
                    <div class="title">Love the demo?</div>
                    <div class="sub-title">Share the experience...</div>
                </div>
                <ui5-button class="close-button"
                            icon="decline"
                            design="Transparent"
                            @click=${() => this.closed = true}
                >
            </div>
            <div class="social-icons">
                <ui5-link href="${hrefs.linkedIn}" target="_blank">
                    <img src="${this.config.baseUrl}/assets/images/linkedin.svg" alt="LinkedIn"/>
                </ui5-link>
                <ui5-link href="${hrefs.twitter}" target="_blank">
                    <img src="${this.config.baseUrl}/assets/images/twitter.svg" alt="Twitter"/>
                </ui5-link>
                <ui5-link href="${hrefs.email}" target="_blank">
                    <img src="${this.config.baseUrl}/assets/images/email.svg" alt="Email"/>
                </ui5-link>
            </div>
        </div>
    `;
  }
}


