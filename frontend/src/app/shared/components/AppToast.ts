import {css, html, LitElement} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';
import {colorsConfig, commonCss} from "../config/styles";

const styles = css`
${commonCss}

#toast-container {
  background: ${colorsConfig.linkActive};
  width: 100%;
  position: fixed;
  margin: auto;
  bottom: 0;
  z-index: 1100;
  right: 0;
  text-align: center;
  padding: 1.2rem 0;
  display: none;
}
#toast-container.open {
  display: block;
}
`

@customElement('app-toast')
export class AppToast extends LitElement {
  static override styles = styles;

  @property()
  text = '';
  @property()
  duration = 2000;

  @query('#toast-container')
  private toastContainer: HTMLElement | undefined;


  show() {
    this.toastContainer?.classList.add('open');

    setTimeout(() => {
      this.toastContainer?.classList.remove('open');
    }, this.duration)
  }


  override render() {
    return html`
        <div id="toast-container">
            ${this.text}
        </div>
    `;
  }
}
