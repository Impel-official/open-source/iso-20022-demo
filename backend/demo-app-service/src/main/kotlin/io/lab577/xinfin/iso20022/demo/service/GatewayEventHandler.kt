package io.lab577.xinfin.iso20022.demo.service

import com.fasterxml.jackson.databind.ObjectMapper
import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntity
import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntityRepository
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime
import java.util.*

@Service
@Transactional(readOnly = true)
class GatewayEventHandler(
    repository: GatewayEventEntityRepository,
    private val gatewayMessageEventHandler: GatewayMessageEventHandler,
    private val gatewayProofEventHandler: GatewayProofEventHandler,
    private val objectMapper: ObjectMapper,
) : BaseService<GatewayEventEntity, UUID, GatewayEventEntityRepository>(repository) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(GatewayEventHandler::class.java)
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    fun handleEvent(gatewayEventEntity: GatewayEventEntity) {
        if (repository.existsById(gatewayEventEntity.id)) {
            logger.debug("Gateway event ${gatewayEventEntity.id} already exists - ignoring duplicated event {event: $gatewayEventEntity}")
            return
        }

        logger.info("Saving gateway event ${gatewayEventEntity.id} {event: $gatewayEventEntity}")
        repository.save(gatewayEventEntity)
        
        when (gatewayEventEntity.type) {
            EventDto.TypeEnum.MESSAGE_CREATED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, MessageCreatedEvent::class.java)
                gatewayMessageEventHandler.handleMessageCreatedEvent(event)
            }
            EventDto.TypeEnum.MESSAGE_SENT -> Unit
            EventDto.TypeEnum.MESSAGE_RECEIVED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, MessageReceivedEvent::class.java)
                gatewayMessageEventHandler.handleMessageReceivedEvent(event)
            }
            EventDto.TypeEnum.MESSAGE_ACKNOWLEDGED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, MessageAcknowledgedEvent::class.java)
                gatewayMessageEventHandler.handleMessageAcknowledgedEvent(event)
            }
            EventDto.TypeEnum.MESSAGE_NOT_ACKNOWLEDGED -> {
                val event =
                    objectMapper.readValue(gatewayEventEntity.data, MessageNotAcknowledgedEvent::class.java)
                gatewayMessageEventHandler.handleMessageNotAcknowledgedEvent(event)
            }
            EventDto.TypeEnum.PROOF_CREATED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, ProofCreatedEvent::class.java)
                gatewayProofEventHandler.handleProofCreatedEvent(event)
            }
            EventDto.TypeEnum.PROOF_SENT -> Unit
            EventDto.TypeEnum.PROOF_RECEIVED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, ProofReceivedEvent::class.java)
                gatewayProofEventHandler.handleProofReceivedEvent(event)
            }
            EventDto.TypeEnum.PROOF_ACKNOWLEDGED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, ProofAcknowledgedEvent::class.java)
                gatewayProofEventHandler.handleProofAcknowledgedEvent(event)
            }
            EventDto.TypeEnum.PROOF_NOT_ACKNOWLEDGED -> Unit
            EventDto.TypeEnum.PROOF_COMMITTED -> {
                val event = objectMapper.readValue(gatewayEventEntity.data, ProofCommittedEvent::class.java)
                gatewayProofEventHandler.handleProofCommittedEvent(event)
            }
            EventDto.TypeEnum.PROOF_APPROVED -> Unit
            EventDto.TypeEnum.PROOF_REJECTED -> Unit
        }
    }

    fun getLastEventDateTime(): OffsetDateTime? = repository.getLastEventDateTime()

}
