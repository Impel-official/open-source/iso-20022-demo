import {customElement} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {transactionsStore, TransactionsStore} from '../TransactionsStore';
import {RoutingService, routingService} from '../../shared/routing/RoutingService';
import {cardCss, commonCss} from "../../shared/config/styles";

const styles = css`
${commonCss}
${cardCss}
.buttons-area {
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 1rem 0;
  box-sizing: border-box;
}
/* Desktop menu */
@media all and (min-width: 960px) {
  .buttons-area {
    flex-direction: row;
  }
}
`;

@customElement('transactions-details-view')
export class TransactionsView extends LitElement {
  static override styles = styles;

  private transactionsStore: TransactionsStore;
  private routingService: RoutingService;

  constructor() {
    super();
    this.transactionsStore = transactionsStore;
    this.routingService = routingService;
  }

  override async connectedCallback() {
    super.connectedCallback();
    this.transactionsStore.bindComponentToPropertyChanges(this);
    await this.transactionsStore.getTransactionDetailsFromUrl();
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    this.transactionsStore.unbindComponentFromPropertyChanges(this);
  }

  renderButtons() {
    if (this.transactionsStore.selectedTransaction?.isUnapproved()) {
      return html`
          <div class="buttons-area">
              <unapproved-transaction-confirmation-buttons></unapproved-transaction-confirmation-buttons>
          </div>
      `
    } else {
      return html`
          <div class="mt-2 buttons-area">
              <ui5-button design="Emphasized" @click=${() => this.routingService.navigateToRoute('transactions')}>
                  Back to Transactions
              </ui5-button>
          </div>
      `
    }
  }

  override render() {
    return html`
        <div class="transaction-details">
            <div class="card-center">
                <transaction-details .transaction=${this.transactionsStore.selectedTransaction}>
                </transaction-details>
                ${this.renderButtons()}
            </div>
        </div>
    `
  }
}
