package io.lab577.xinfin.iso20022.demo.integration.kucoin.model

data class Price(
    val code: String,
    val data: Map<String, String>
)
