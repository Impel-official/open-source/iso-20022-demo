import {AppConfig} from "../config";
import Keycloak, {KeycloakConfig, KeycloakInitOptions, KeycloakInstance} from "keycloak-js";
import {RestAdapter} from "../rest/RestAdapter";
import {User} from "../user/User";
import {UserStore} from "../user/UserStore";
import {AuthService} from "./AuthService";

export class KeycloakAuthService implements AuthService {
    private keycloak?: KeycloakInstance;

    constructor(
        private config: AppConfig,
        private restAdapter: RestAdapter,
        private userStore: UserStore
    ) {
        // TODO: add to config to match realm settings?
        const intervalMs = 60 * 1000;
        this.refreshAuthTokenEvery(intervalMs);
    }

    private refreshAuthTokenEvery(intervalMs: number) {
        setInterval(
            async () => {
                // Make sure the minimum validity
                // is less than the interval
                const minValidity = (intervalMs / 1000) / 2
                try {
                    await this.maybeRefreshToken(minValidity);
                } catch (e) {
                    console.error('Error refreshing token');
                    throw e;
                }
            },
            intervalMs
        )
    }

    async init() {
        const keycloakConfig: KeycloakConfig = {
            url: this.config.auth.keycloak.url,
            realm: this.config.auth.keycloak.realm,
            clientId: this.config.auth.keycloak.clientId,
        };
        const keycloakInitConfig: KeycloakInitOptions = {
            onLoad: 'login-required',
            enableLogging: true
        };
        try {
            this.keycloak = Keycloak(keycloakConfig);
            await this.getKeycloak().init(keycloakInitConfig);
        } catch (e) {
            console.error('Failed to initialize keycloak');
            throw e;
        }
    }

    public redirectToLoginPage(redirectPath: string) {
        const redirectUri = `${this.config.host}/${redirectPath}`;
        this.getKeycloak().login({redirectUri});
    }

    public isAuthenticated(): boolean {
        if (this.getKeycloak().authenticated === undefined) return false;
        return this.getKeycloak().authenticated!;
    }

    public getToken(): string {
        if (this.getKeycloak().token === undefined
            || this.getKeycloak().isTokenExpired()) {
            this.redirectToLoginPage(`${this.config.baseUrl}`);
        }
        return `Bearer ${this.getKeycloak().token!}`
    }

    async maybeRefreshToken(minValidity: number): Promise<void> {
        if (!this.keycloak || !this.getKeycloak().token) {
            console.debug('Token refresh skipped (unauthorized or not initialized) on ' + new Date());
            return;
        }
        console.debug('Token refresh initiated on ' + new Date());
        try {
            const refreshed = await this.getKeycloak().updateToken(minValidity);
            if (refreshed) {
                console.debug('Token refreshed on ' + new Date());
                this.restAdapter.authenticateHttpRequests(this.getToken());
            } else {
                console.debug('Token still valid on ' + new Date());
            }
        } catch (e) {
            console.error('Token refresh failed on ' + new Date());
        }
    }

    private getKeycloak() {
        if (!this.keycloak) throw new Error('Keycloak object not initialized.');
        return this.keycloak;
    }

    async initUser() {
        const profile = await this.getKeycloak().loadUserProfile();
        const id = profile.id;
        const username = profile.username;
        if (!id || !username)
            throw new Error('Profile data nor properly formatted');
        return new User(id, username, "KEYCLOAK", profile.email);
    }

    getUser() {
        if (!this.userStore.hasUser()) throw 'No user is set';
        return this.userStore.user!;
    }

    async authenticate() {
        if (!this.restAdapter.areRequestsAuthenticated()) {
            this.restAdapter.authenticateHttpRequests(this.getToken());
        }
        if (!this.userStore.hasUser()) {
            const user = await this.initUser();
            this.userStore.setUser(user);
        }
    }

    logout() {
        this.getKeycloak().logout();
    }
}
