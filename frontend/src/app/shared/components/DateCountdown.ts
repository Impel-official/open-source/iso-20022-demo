import dayjs from 'dayjs';
import {css, html, LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators.js';
import {commonCss} from "../config/styles";
import _ from "lodash";

const styles = css`
${commonCss}
ul.countdown {
  display: flex;
  flex-direction: row;
  list-style: none;
  margin: 0;
  padding: 0;
}

ul.countdown li {
  display: flex;
  flex-direction: column;
  margin: 0 0.5rem;
  color: #8fbfe0;
}
`

@customElement('date-countdown')
export class DateCountdown extends LitElement {
  static override styles = styles;

  @property()
  date = '';
  @property()
  inline = false;
  interval: any;

  @state()
  daysLeft = 0;
  @state()
  hoursLeft = 0;
  @state()
  minutesLeft = 0;
  @state()
  secondsLeft = 0;

  override connectedCallback() {
    super.connectedCallback();

    const end = dayjs(this.date);

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const host = this;
    this.interval = setInterval(function () {
      const now = dayjs();
      const diff = end.diff(now, 'seconds');

      if (diff < 0) {
        clearInterval(host.interval);
        return;
      }

      const diffDuration = dayjs.duration(diff, 'seconds');
      host.daysLeft = diffDuration.days();
      host.hoursLeft = diffDuration.hours();
      host.minutesLeft = diffDuration.minutes();
      host.secondsLeft = diffDuration.seconds();
    }, 1000);
  }


  override disconnectedCallback() {
    super.disconnectedCallback();
    clearInterval(this.interval);
  }

  override render() {
    const daysLeft = _.padStart(`${this.daysLeft}`, 2, '0');
    const hoursLeft = _.padStart(`${this.hoursLeft}`, 2, '0');
    const minutesLeft = _.padStart(`${this.minutesLeft}`, 2, '0');
    const secondsLeft = _.padStart(`${this.secondsLeft}`, 2, '0');

    return this.inline
        ? html`
                <span class="countdown">
            <span class="${this.daysLeft == 0 ? 'hidden' : ''}"><span id="days">${daysLeft}</span>:</span
            ><span>${hoursLeft}</span>:</span
                ><span><span id="minutes">${minutesLeft}</span>:</span
                ><span><span id="seconds">${secondsLeft}</span></span>
                </span>
        `
        : html`
                <ul class="countdown">
                    <li class="${this.daysLeft == 0 ? 'hidden' : ''}"><span id="days">${this.daysLeft}</span> Days</li>
                    <li class="${this.daysLeft == 0 && this.hoursLeft == 0 ? 'hidden' : ''}"
                    <span id="hours">${this.hoursLeft}</span> Hours</li>
                    <li class="${this.hoursLeft == 0 && this.minutesLeft == 0 ? 'hidden' : ''}"><span
                            id="minutes">${this.minutesLeft}</span> Minutes
                    </li>
                    <li class="${this.minutesLeft == 0 && this.secondsLeft == 0 ? 'hidden' : ''}"><span
                            id="seconds">${this.secondsLeft}</span> Seconds
                    </li>
                </ul>
        `;
  }
}
