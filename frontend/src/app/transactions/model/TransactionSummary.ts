import {Transaction} from './Transaction';
import {TransactionsTableFilter, TransactionsTableFilterFieldType} from './TransactionsTableFilter';
import {Date2} from '../../shared/models/Date2';
import {Amount} from '../../shared/models/Amount';
import {TransactionDtoDirectionEnum, TransactionDtoStatusEnum} from "../../shared/api";
import dayjs from "dayjs";

const isEmpty = (data:any) => data == '';
export class TransactionSummary {
  public readonly requestDate2: Date2;
  public readonly validUntilDate2: Date2;
  public readonly amount2: Amount;

  constructor(
    public readonly id: string,
    public readonly endToEndId: string,
    public readonly counterPartyBic: string,
    public readonly senderAccount: string,
    public readonly recipientAccount: string,
    public readonly requestDate: string,
    public readonly validUntil: string,
    public readonly currency: string,
    public readonly amount: string,
    public readonly direction: TransactionDtoDirectionEnum,
    public readonly status: TransactionDtoStatusEnum,
    public readonly rollupTransactionHash: string,
  ) {
    this.requestDate2 = Date2.from(requestDate);
    this.validUntilDate2 = Date2.from(validUntil);
    this.amount2 = Amount.from(amount, currency);
  }

  isPositive():boolean { return this.status == 'Approved'; }
  isNegative(): boolean { return this.status == 'Rejected' || this.status == 'Failed'; }
  isFinal(): boolean { return this.status == 'Approved' || this.status == 'Rejected' || this.status == 'Failed' }
  isActionable(): boolean { return this.direction == 'INBOX' && this.status == 'Received'; }
  isPending(): boolean { return !this.isFinal(); }
  isExpired(): boolean { return this.validUntilDate2.valueOf() > new Date().valueOf(); }
  isPendingExpiry(): boolean { return this.isPending()/* && !this.isExpired();*/ }

  buildStatusLabel(): string {
    const direction = this.direction == 'OUTBOX' ? 'Delivered' : 'Received';
    const status = this.status == 'Received' ? 'Waiting Approval' : this.status;
    return `${direction} - ${status}`;
  }

  static fromJson(json: any): TransactionSummary {
    // TODO: this needs json schema validation
    return new TransactionSummary(
      json.id,
      json.endToEndId,
      json.counterPartyBic,
      json.senderAccount,
      json.recipientAccount,
      json.requestDate,
      json.validUntil,
      json.currency,
      json.amount,
      json.direction,
      json.status,
      json.rollupTransactionHash
    )
  }

  static from(transaction: Transaction): TransactionSummary {
    return new TransactionSummary(
      transaction.id,
      transaction.endToEndId,
      transaction.counterPartyBic,
      transaction.senderAccount,
      transaction.recipientAccount,
      transaction.requestDate,
      transaction.validUntil,
      transaction.currency,
      transaction.amount,
      transaction.direction,
      transaction.status,
      transaction.rollupTransactionHash
    )
  }

  equals(transactionSummary: TransactionSummary) {
    return this.id === transactionSummary.id;
  }

  private doesFilterMatch(filter: TransactionsTableFilter): boolean {
    if ((isEmpty(filter.fieldType) || isEmpty(filter.fieldValue)) && isEmpty(filter.status)) {
      return true
    } else {
      let matchesFilter = true;
      const fieldType = filter.fieldType;
      const fieldValue = filter.fieldValue;
      if (filter.status) {
        const {status, direction} = filter.getStatusAndDirection()
        matchesFilter = matchesFilter && this.status == status;
        if (direction) {
          matchesFilter = matchesFilter && this.direction == direction;
        }
      }
      if (fieldType == TransactionsTableFilterFieldType.endToEndId) {
        matchesFilter = matchesFilter && this.endToEndId == fieldValue;
      }
      if (fieldType == TransactionsTableFilterFieldType.counterPartyBic) {
        matchesFilter = matchesFilter && this.counterPartyBic == fieldValue;
      }
      if (fieldType == TransactionsTableFilterFieldType.recipientAccount) {
        matchesFilter = matchesFilter && this.recipientAccount == fieldValue;
      }
      if (fieldType == TransactionsTableFilterFieldType.senderAccount) {
        matchesFilter = matchesFilter && this.senderAccount == fieldValue;
      }
      if (fieldType == TransactionsTableFilterFieldType.amount) {
        matchesFilter = matchesFilter && this.amount == fieldValue;
      }
      return matchesFilter;
    }
  }

  doesMatchTo(filter: TransactionsTableFilter): boolean {
    const doesFilterMatch = this.doesFilterMatch(filter) ;
    const isStartDateGEToRequestDate = isEmpty(filter.startDate) ?
      true : dayjs.utc(`${filter.startDate} UTC`).unix() <= dayjs.utc(this.requestDate).unix()

    const isEndDateLEToRequestDate = isEmpty(filter.endDate) ?
      true : dayjs.utc(`${filter.endDate} UTC`).unix() >= dayjs.utc(this.requestDate).unix()
    return doesFilterMatch && isStartDateGEToRequestDate && isEndDateLEToRequestDate;
  }

  updateStatus(status: TransactionDtoStatusEnum): TransactionSummary {
    return  this.copy(status);
  }

  updateRollupTransactionHash(rollupTransactionHash: string): TransactionSummary {
    return  this.copy(undefined, rollupTransactionHash);
  }


  private copy(status?: TransactionDtoStatusEnum, rollupTransactionHash?: string) {
    return new TransactionSummary(
      this.id,
      this.endToEndId,
      this.counterPartyBic,
      this.senderAccount,
      this.recipientAccount,
      this.requestDate,
      this.validUntil,
      this.currency,
      this.amount,
      this.direction,
      status || this.status,
      rollupTransactionHash || this.rollupTransactionHash
    )
  }
}
