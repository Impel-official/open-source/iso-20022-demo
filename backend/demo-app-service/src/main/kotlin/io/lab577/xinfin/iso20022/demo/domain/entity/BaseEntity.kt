package io.lab577.xinfin.iso20022.demo.domain.entity

interface BaseEntity<T> {
    var id: T
}
