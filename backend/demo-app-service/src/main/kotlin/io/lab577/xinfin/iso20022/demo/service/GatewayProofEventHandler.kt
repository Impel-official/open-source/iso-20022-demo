package io.lab577.xinfin.iso20022.demo.service

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionProofStatus
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.ProofAcknowledgedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.ProofCommittedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.ProofCreatedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.ProofReceivedEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class GatewayProofEventHandler(
    private val transactionService: TransactionService,
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(GatewayProofEventHandler::class.java)
    }

    @Transactional(readOnly = false)
    fun handleProofCreatedEvent(event: ProofCreatedEvent) {
        logger.info("Handling ${ProofCreatedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndIds: ${event.endToEndIds}}")

        transactionService.updateProofStatusByMessagesIds(TransactionProofStatus.Signing, event.messagesIds)
    }

    @Transactional(readOnly = false)
    fun handleProofReceivedEvent(event: ProofReceivedEvent) {
        logger.info("Handling ${ProofReceivedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndIds: ${event.endToEndIds}}")

        transactionService.updateProofStatusByMessagesIds(TransactionProofStatus.Signed, event.messagesIds)
    }

    @Transactional(readOnly = false)
    fun handleProofAcknowledgedEvent(event: ProofAcknowledgedEvent) {
        logger.info("Handling ${ProofAcknowledgedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndIds: ${event.endToEndIds}}")

        transactionService.updateProofStatusByMessagesIds(TransactionProofStatus.Signed, event.messagesIds)
    }

    @Transactional(readOnly = false)
    fun handleProofCommittedEvent(event: ProofCommittedEvent) {
        logger.info("Handling ${ProofCommittedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndIds: ${event.endToEndIds}}")

        transactionService.updateRollupTransactionHashByMessagesIds(event.transactionHash, event.messagesIds)
    }
}
