package io.lab577.xinfin.iso20022.demo.service

import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntity
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class GatewayEventFetchTask(
    private val iso20022ClientService: Iso20022ClientService,
    private val gatewayEventHandler: GatewayEventHandler,
    @Value("\${app.integration.iso20022.fetchEventsScheduledTaskPageSize:10}")
    private var fetchEventsScheduledTaskPageSize: Int
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(GatewayEventFetchTask::class.java)
    }

    @Scheduled(fixedDelayString = "\${app.integration.iso20022.fetchEventsScheduledTaskFixedDelayMs:5000}")
    fun fetchGatewayMessages() {
        val startDateTime = gatewayEventHandler.getLastEventDateTime()?.plusNanos(1)
            ?: OffsetDateTime.now().withYear(2000)
        val endDateTime = OffsetDateTime.now()
        logger.debug("Fetching gateway events {startDateTime: $startDateTime, endDateTime: $endDateTime, pageSize: $fetchEventsScheduledTaskPageSize}")
        iso20022ClientService.getEvents(startDateTime, endDateTime, fetchEventsScheduledTaskPageSize)
            .sortedBy { it.timestamp }
            .forEach {
                gatewayEventHandler.handleEvent(
                    GatewayEventEntity(
                        id = it.id,
                        timestamp = it.timestamp,
                        type = it.type,
                        subjectId = it.subjectId,
                        data = it.data,
                    )
                )
            }
    }
}
