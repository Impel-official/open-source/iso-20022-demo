import {TransactionSummary} from '../../transactions/model/TransactionSummary';
import {config} from '../config';
import {TransactionsTableFilter} from '../../transactions/model/TransactionsTableFilter';
import {TransactionsTablePaging} from '../../transactions/model/TransactionsTablePaging';
import {Transaction} from '../../transactions/model/Transaction';
import {TransactionsTableSorting} from '../../transactions/model/TransactionsTableSorting';
import {TransferRequest} from '../../tranfer-request/models/TransferRequest';
import {TransactionsTable} from '../../transactions/model/TransactionsTable';
import {httpClient, HttpClient} from "./HttpClient";
import {KuCoinPrice} from "../models/KuCoinPrice";
import {CountResponse} from "./CountResponse";
import {ReferenceData} from "../../transactions/model/ReferenceData";
import {SendMessageResponse, TransactionAllMetrics, TransactionAllMetricsFromJSON} from "../api";

export class RestAdapter {

  constructor(
    private httpClient: HttpClient
  ) {}

  authenticateHttpRequests(token: string) {
    this.httpClient.setAuthToken(token);
  }

  areRequestsAuthenticated() {
    return this.httpClient.getAuthToken() !== undefined;
  }

  async getFilteredTransactionSummariesPage(
    transactionsTableSorting: TransactionsTableSorting,
    transactionsTablePaging: TransactionsTablePaging,
    transactionsTableFilter: TransactionsTableFilter
  ): Promise<TransactionsTable> {
    const querySortParams = this.buildSortQueryParams(transactionsTableSorting);
    const queryPageParams = this.buildPagingQueryParams(transactionsTablePaging);
    const queryFilterParams = this.buildFiltersQueryParams(transactionsTableFilter);

    const requestUrl = `${config.rest.transactions}?${queryPageParams}&${querySortParams}&${queryFilterParams}`;
    const responseObject =  await this.httpClient.get(requestUrl);
    const transactions = responseObject['content'].map(
      (it: any) => { return TransactionSummary.fromJson(it)}
    );
    const totalPages = responseObject['totalPages'];
    const totalElements = responseObject['totalElements'];
    const pageNumber = responseObject['number'];
    const pageSize = responseObject['size'];
    return new TransactionsTable(
      transactions, totalPages, totalElements, pageNumber, pageSize
    );
  }

  async getTransaction(transactionId: string): Promise<Transaction> {
    const responseObject =
        await this.httpClient.get(config.rest.transactions + '/' + transactionId);
    return Transaction.fromJson(responseObject);
  }

  async getPrice(base: string, currencies: string): Promise<KuCoinPrice> {
    const params = this.buildPriceParams(base, currencies);
    const requestUrl = `${config.rest.prices}?${params}`;
    const responseObject = await this.httpClient.get(requestUrl);
    return KuCoinPrice.fromJson(responseObject);
  }

  async getReferenceData(): Promise<ReferenceData> {
    const response = await this.httpClient.get(config.rest.references);
    return ReferenceData.fromJson(response)
  }

  async confirmTransferRequest(transactionId: string, approve: boolean) {
    const url = config.rest.transactions + '/' + transactionId + '/review/';
    await this.httpClient.post(url, {approve});
  }

  async countInboundUnapproved(): Promise<CountResponse> {
    const url = config.rest.transactions + '/unapproved/count';
    return await this.httpClient.get(url);
  }

  async confirmTransferRequests(
      transactionIds: Array<string>,
      approve: boolean
  ) {
    const url = config.rest.transactions + '/review/';
    await this.httpClient.post(url, {transactionIds, approve});
  }

  async sendCreateTransferRequest(request: TransferRequest): Promise<string> {
    const body = {
      senderBic: request.senderBic,
      recipientBic: request.recipientBic,
      senderAccount: request.senderAccount,
      recipientAccount: request.recipientAccount,
      amount: request.amount,
      currency: request.currency,
      collateral: request.collateral,
      referenceInfo: request.referenceInfo,
    }
    const createTransferRequestResponse: SendMessageResponse = await this.httpClient.post(config.rest.pacs009, body);
    return createTransferRequestResponse.assignedId;
  }

  async getMetrics(): Promise<TransactionAllMetrics> {
    const json = await this.httpClient.get(config.rest.transactionMetrics);
    return TransactionAllMetricsFromJSON(json);
  }

  async getMessageXML(id: string): Promise<string> {
    const urlEncodedId = encodeURIComponent(id);
    const messageXmlUrl =
      config.rest.transactionMessages + '/' + urlEncodedId + "/raw";
    return await this.httpClient.get(messageXmlUrl, true);
  }

  async getHubSpotContact(id: string): Promise<{ id: string, email: string }> {
    return await this.httpClient.get(`${config.rest.hubSpotContacts}/${id}`);
  }

  // private

  private buildSortQueryParams(
    transactionsTableSorting: TransactionsTableSorting
  ): string {
    const field = transactionsTableSorting.sort;
    const direction = transactionsTableSorting.direction;
    const queryPrams =  new URLSearchParams();
    queryPrams.set('sort', field);
    queryPrams.set('direction', direction);
    return queryPrams.toString()
  }

  private buildPagingQueryParams(
    transactionsTablePaging: TransactionsTablePaging
  ): string {
    const pageNumberAsIndex = transactionsTablePaging.pageNumber;
    const perPage = transactionsTablePaging.pageSize;
    const queryPrams =  new URLSearchParams();
    queryPrams.set('pn', pageNumberAsIndex.toString());
    queryPrams.set('ps', perPage.toString());
    return queryPrams.toString()
  }

  private buildFiltersQueryParams(
    transactionsTableFilter: TransactionsTableFilter
  ): string {
    const rsql = transactionsTableFilter.toRSQL();
    if(rsql == "") return ""
    const queryPrams =  new URLSearchParams();
    queryPrams.set('filter', rsql);
    return queryPrams.toString();
  }

  private buildPriceParams(
      base: string,
      currency: string
  ): string {
    const queryPrams =  new URLSearchParams();
    queryPrams.set('base', base);
    queryPrams.set('currencies', currency);
    return queryPrams.toString();
  }
}

export const restAdapter = new RestAdapter(httpClient);
