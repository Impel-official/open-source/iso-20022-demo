package io.lab577.xinfin.iso20022.demo.integration.iso20022

import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.*
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service
import java.time.OffsetDateTime
import java.util.UUID

@Service
class Iso20022ClientService(
    private val iso20022Client: Iso20022Client,
    private val iso20022EventClient: Iso20022EventClient,
    private val iso20022IdentityClient: Iso20022IdentityClient,
    private val iso20022MetricsClient: Iso20022MetricsClient
) {

    fun sendMessage(sendMessageRequest: SendMessageRequest): SendMessageResponse {
        val response = iso20022Client.sendMessage(sendMessageRequest)
        if (response.statusCode.is2xxSuccessful) {
            return response.body!!
        } else {
            error("Failed to send a message")
        }
    }

    @Retryable
    fun getMessage(messageId: UUID, withPayload: Boolean): MessageInfo {
        val response = iso20022Client.getMessage(messageId, withPayload)
        if (response.statusCode.is2xxSuccessful) {
            return response.body!!
        } else {
            error("Failed to get a message")
        }
    }

    @Retryable
    fun getEvents(startDateTime: OffsetDateTime, endDateTime: OffsetDateTime, pageSize: Int = 10): List<EventDto> {
        val response = iso20022EventClient.getEvents(startDateTime, endDateTime, 0, pageSize)
        if (response.statusCode.is2xxSuccessful) {
            return response.body?.content ?: emptyList()
        } else {
            error("Failed to get events")
        }
    }

    @Retryable
    fun getPartyByBic(bic: String): PartyDto {
        val response = iso20022IdentityClient.getPartyByBic(bic)
        if (response.statusCode.is2xxSuccessful) {
            return response.body ?: error("No party found for BIC: $bic")
        } else {
            error("Failed to get party by BIC code")
        }
    }

    @Retryable
    fun findParties(
        filter: String? = null,
        pn: Int = 0,
        ps: Int = 50
    ): List<PartyDto> {
        val response = iso20022IdentityClient.getParties(filter ?: "", pn, ps)
        if (response.statusCode.is2xxSuccessful) {
            return response.body?.content ?: emptyList()
        } else {
            error("Failed to get parties")
        }
    }

    @Retryable
    fun findPartyAccounts(
        filter: String? = null, pn: Int = 0, ps: Int = 50
    ): List<PartyAccountDto> {
        val response = iso20022IdentityClient.getPartyAccounts(filter ?: "", pn, ps)
        if (response.statusCode.is2xxSuccessful) {
            return response.body?.content ?: emptyList()
        } else {
            error("Failed to get party accounts")
        }
    }

    @Retryable
    fun getProofMetrics(): ProofMetricsDto? {
        val response = iso20022MetricsClient.getProofMetrics()
        if (response.statusCode.is2xxSuccessful) {
            return response.body
        } else {
            error("Failed to get proof metrics")
        }
    }

    @Retryable
    fun getNetworkAccountData(): NetworkAccountDto? {
        val response = iso20022MetricsClient.getNetworkAccount()
        if (response.statusCode.is2xxSuccessful) {
            return response.body
        } else {
            error("Failed to get network account info")
        }
    }
}
