package io.lab577.xinfin.iso20022.demo.service

import io.lab577.xinfin.iso20022.demo.domain.repository.TransactionStatsRepository
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import io.lab577.xinfin.iso20022.demo.metrics.*
import io.lab577.xinfin.iso20022.demo.utils.endOfDay
import io.lab577.xinfin.iso20022.demo.utils.startOfDay
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime
import java.time.ZoneOffset.UTC
import java.time.temporal.ChronoUnit

@Service
@Transactional(readOnly = true)
class TransactionMetricsService(
    @Value("\${app.bic}")
    var bic: String,
    val transactionStatsRepository: TransactionStatsRepository,
    val iso20022ClientService: Iso20022ClientService,
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(TransactionMetricsService::class.java)
    }

    @Transactional(readOnly = true)
    fun getAllMetrics(): TransactionAllMetrics {
        val now = OffsetDateTime.now(UTC).truncatedTo(ChronoUnit.SECONDS)
        val todayStartOfDay = now.startOfDay()
        val todayEndOfDay = now.endOfDay()

        val last24HoursDates = Pair(
            now.minusHours(24),
            todayEndOfDay
        )
        val mtdDates = Pair(
            todayStartOfDay.withDayOfMonth(1),
            todayEndOfDay
        )
        val ytdDates = Pair(
            todayStartOfDay.withMonth(1).withDayOfMonth(1),
            todayEndOfDay
        )
        val allDates = Pair(
            todayStartOfDay.withYear(2000).withMonth(1).withDayOfMonth(1),
            todayEndOfDay
        )

        return TransactionAllMetrics(
            sentTransactionStats = TransactionStats(
                last24Hours = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getSentTransactionStatsDataProjections(
                        bic, last24HoursDates.first, last24HoursDates.second
                    ),
                    transactionStatsRepository.getSentTransactionCollateralStatsDataProjections(
                        bic, last24HoursDates.first, last24HoursDates.second
                    )
                ), mtd = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getSentTransactionStatsDataProjections(
                        bic, mtdDates.first, mtdDates.second
                    ),
                    transactionStatsRepository.getSentTransactionCollateralStatsDataProjections(
                        bic, mtdDates.first, mtdDates.second
                    )
                ), ytd = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getSentTransactionStatsDataProjections(
                        bic, ytdDates.first, ytdDates.second
                    ),
                    transactionStatsRepository.getSentTransactionCollateralStatsDataProjections(
                        bic, ytdDates.first, ytdDates.second
                    )
                ), all = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getSentTransactionStatsDataProjections(
                        bic, allDates.first, allDates.second
                    ),
                    transactionStatsRepository.getSentTransactionCollateralStatsDataProjections(
                        bic, allDates.first, allDates.second
                    )
                )
            ),
            receivedTransactionStats = TransactionStats(
                last24Hours = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getReceivedTransactionStatsDataProjections(
                        bic, last24HoursDates.first, last24HoursDates.second
                    ),
                    transactionStatsRepository.getReceivedTransactionCollateralStatsDataProjections(
                        bic, last24HoursDates.first, last24HoursDates.second
                    )
                ), mtd = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getReceivedTransactionStatsDataProjections(
                        bic, mtdDates.first, mtdDates.second
                    ),
                    transactionStatsRepository.getReceivedTransactionCollateralStatsDataProjections(
                        bic, mtdDates.first, mtdDates.second
                    )
                ), ytd = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getReceivedTransactionStatsDataProjections(
                        bic, ytdDates.first, ytdDates.second
                    ),
                    transactionStatsRepository.getReceivedTransactionCollateralStatsDataProjections(
                        bic, ytdDates.first, ytdDates.second
                    )
                ), all = TransactionStatsData.mapFromProjections(
                    transactionStatsRepository.getReceivedTransactionStatsDataProjections(
                        bic, allDates.first, allDates.second
                    ),
                    transactionStatsRepository.getReceivedTransactionCollateralStatsDataProjections(
                        bic, allDates.first, allDates.second
                    )
                )
            ),
            transactionMessagesStats = TransactionMessageStats(
                last24Hours = TransactionMessageStatsData(
                    count = transactionStatsRepository.countTransactionMessages(
                        last24HoursDates.first,
                        last24HoursDates.second
                    )
                ),
                mtd = TransactionMessageStatsData(
                    transactionStatsRepository.countTransactionMessages(
                        mtdDates.first,
                        mtdDates.second
                    )
                ),
                ytd = TransactionMessageStatsData(
                    transactionStatsRepository.countTransactionMessages(
                        ytdDates.first,
                        ytdDates.second
                    )
                ),
                all = TransactionMessageStatsData(
                    transactionStatsRepository.countTransactionMessages(
                        allDates.first,
                        allDates.second
                    )
                )
            ),
            proofMetrics = iso20022ClientService.getProofMetrics(),
            networkAccount = iso20022ClientService.getNetworkAccountData()
        )
    }
}
