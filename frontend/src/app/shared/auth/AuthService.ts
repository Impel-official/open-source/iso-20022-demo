import {config} from "../config";
import {restAdapter} from "../rest/RestAdapter";
import {userStore} from "../user/UserStore";
import {KeycloakAuthService} from "./KeycloakAuthService";
import {HubSpotAuthService} from "./HubSpotAuthService";
import {User} from "../user/User";

declare global {
  interface WindowEventMap {
    'user-authenticated': CustomEvent<User>;
  }
}

export interface AuthService {
  init(): void,

  isAuthenticated(): boolean,

  authenticate(): void,

  redirectToLoginPage(redirectPath: string): void,

  logout(): void,

  getUser(): User
}

export let authService: AuthService;
switch (config.auth.type) {
  case "HUB_SPOT":
    authService = new HubSpotAuthService(config, restAdapter, userStore, localStorage);
    break;
  default:
    authService = new KeycloakAuthService(config, restAdapter, userStore);
    break;
}
