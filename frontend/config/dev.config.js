var config = {
  host: 'http://localhost:8000',
  baseUrl: "dist/dev",
  branding : {
    logoUrl: "/assets/logo-dev.svg",
    icoUrl: "/assets/dev-favicon.ico",
    orgName: "Impel"
  },
  rest: {
    references: "http://localhost:8080/api/gateway/iso20022/transaction-options",
    transactions: "http://localhost:8080/api/gateway/iso20022/transactions",
    transactionMessages: "http://localhost:8080/api/gateway/iso20022/transaction-messages",
    pacs009: "http://localhost:8080/api/gateway/iso20022/pacs/messages/pacs-009-001-10",
    transactionMetrics: "http://localhost:8080/api/gateway/iso20022/transaction-metrics",
    prices: "http://localhost:8080/api/kucoin/price"
  },
  stomp: {
    url: "ws://localhost:8080/ws",
    newTransactionsTopic: "/topic/transactions",
    transactionsChangesTopic: "/topic/transaction-messages",
    transactionsMetricsTopic: "/topic/transaction-metrics"
  },
  auth: {
    isEnabled: true,
    keycloak: {
      url: 'http://localhost:8081/auth',
      realm: 'dev',
      clientId: 'gateway-service-ui',
    }
  },
  xdc: {
    txPrefixUrl: 'https://explorer.apothem.network/txs/'
  },
  bic: "CCCCESXXXXX",
  progressUpdatesIntervalMs: 750,
  reportingNetworkTableDisabled: ('false' === 'true'),
  reportingReceivedTableDisabled: ('false' === 'true'),
  collateralTypes: [
    { type: 'NATIVE', symbol: 'XDC', name: 'XDC'},
    { type: 'ERC20', symbol: 'BTC', name: 'xBTC (Wrapped BTC)'},
    { type: 'ERC20', symbol: 'USDC', name: 'xUSDC (Wrapped USDC)'}
  ]
}
