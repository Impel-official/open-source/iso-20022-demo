import {customElement, state} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {stompAdapter} from '../shared/stomp/StompAdapter';
import {RestAdapter, restAdapter} from "../shared/rest/RestAdapter";
import {cardCss, colorsConfig, commonCss, tableCss, typography} from "../shared/config/styles";
import {ProofMetricsData, TransactionAllMetrics, TransactionStatsData} from "../shared/api";
import {formatNumber} from "../shared/utils";
import {AppConfig, config} from "../shared/config";
import _ from 'lodash';

const styles = css`
${commonCss}
${tableCss}
${cardCss}
${typography}

ui5-table-column::part(column),
ui5-table-column::part(column), th{
  text-align: left;
  font-weight: 400;
}
ui5-table-cell, td {
    text-align: left;
}

.amount {
    text-align: left;
}

.wallet-messages-row {
  align-items: self-start;
  flex-direction: column;
}

ui5-table-cell:first-of-type, td:first-of-type {
    font-weight: 400;
    text-align: right;
    min-width: 103px;
    border-right: 1px ${colorsConfig.tableBorderFirstColumn} solid;
}

ui5-table-row:nth-child(even) > ui5-table-cell:first-child {
    background: ${colorsConfig.trAccentBg};
}

ui5-table-row:nth-child(odd) > ui5-table-cell:first-child {
    background: ${colorsConfig.trAltBg};
}

.title {
  margin-bottom: 0.75rem;
}

/* Desktop menu */
@media all and (min-width: 960px) {
  .wallet-messages-row {
    flex-direction: row;
  }
}
`

@customElement('dashboard-view')
export class DashboardView extends LitElement {
  static override styles = styles;

  @state()
  metrics?: TransactionAllMetrics;

  private config: AppConfig;
  private restAdapter: RestAdapter;

  constructor() {
    super();
    this.config = config;
    this.restAdapter = restAdapter;
  }

  override async connectedCallback() {
    super.connectedCallback();
    this.metrics = await this.restAdapter.getMetrics();
    stompAdapter.addListener(
        this,
        'MetricsUpdates',
        this.handleMetricsUpdated.bind(this)
    );
  }

  override async disconnectedCallback() {
    super.disconnectedCallback();
    stompAdapter.removeListener(this, 'MetricsUpdates');
  }

  private handleMetricsUpdated(metrics: TransactionAllMetrics) {
    this.metrics = metrics;
    this.requestUpdate();
  }

  override render() {
    return html`
        <div class="column">
            ${(this.config.reportingNetworkTableDisabled ? '' : html`
                <div class="row">
                    ${this.renderNetworkStatsTable()}
                </div>
            `)}
            <div class="row wallet-messages-row">
                ${this.renderXdcWalletStatsTable()}
                ${this.renderTotalTransactionsStatsTable()}
            </div>
            <div class="row">
                ${this.renderSentTransactionsStatsTable()}
            </div>
            ${this.config.reportingReceivedTableDisabled ? '' : html`
                <div class="row">
                    ${this.renderReceivedTransactionsStatsTable()}
                </div>
            `}
        </div>
    `
  }

  private renderNetworkStatsTable() {
    return html`
        <div class="card light">
            <div class="title">Network</div>
            <ui5-table no-data-text="No Data" mode="None">
                <ui5-table-column slot="columns">
                    <div class="text-right">Period</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>Avg. Gas Fee (XDC)</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>Total Gas Fee (XDC)</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>#Commits</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>Avg. TX / Commit</div>
                </ui5-table-column>
                ${this.renderNetworkStatsRow('Last 24 hrs', this.metrics?.proofMetrics?.last24Hours)}
                ${this.renderNetworkStatsRow('MTD', this.metrics?.proofMetrics?.mtd)}
                ${this.renderNetworkStatsRow('YTD', this.metrics?.proofMetrics?.ytd)}
                ${this.renderNetworkStatsRow('Lifetime', this.metrics?.proofMetrics?.all)}
            </ui5-table>
        </div>
    `
  }

  private renderNetworkStatsRow(title: string, data?: ProofMetricsData) {
    return html`
        <ui5-table-row>
            <ui5-table-cell class="reporting-table-label">${title}</ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.avgGasUsedXdc, 0, 12)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.totalGasUsedXdc, 0, 12)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.count, 0, 0)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.avgMessagesCount, 0, 0)}
            </ui5-table-cell>
        </ui5-table-row>
    `;
  }

  private renderTotalTransactionsStatsTable() {
    const last24HoursCount = (this.metrics?.sentTransactionStats?.last24Hours?.count ?? 0)
        + (this.metrics?.receivedTransactionStats?.last24Hours?.count ?? 0);
    const mtdCount = (this.metrics?.sentTransactionStats?.mtd?.count ?? 0)
        + (this.metrics?.receivedTransactionStats?.mtd?.count ?? 0);
    const ytdCount = (this.metrics?.sentTransactionStats?.ytd?.count ?? 0)
        + (this.metrics?.receivedTransactionStats?.ytd?.count ?? 0);
    const allCount = (this.metrics?.sentTransactionStats?.all?.count ?? 0)
        + (this.metrics?.receivedTransactionStats?.all?.count ?? 0);
    return html`
        <div class="card light">
            <div class="title">Total Transactions</div>
            <ui5-table no-data-text="No Data" mode="None">
                <ui5-table-column slot="columns">
                    <div class="text-right">Period</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div># of Transactions</div>
                </ui5-table-column>
                <ui5-table-row>
                    <ui5-table-cell class="reporting-table-label">Last 24 hrs</ui5-table-cell>
                    <ui5-table-cell class="amount">
                        ${formatNumber(last24HoursCount, 0, 0)}
                    </ui5-table-cell>
                </ui5-table-row>
                <ui5-table-row>
                    <ui5-table-cell class="reporting-table-label">MTD</ui5-table-cell>
                    <ui5-table-cell class="amount">
                        ${formatNumber(mtdCount, 0, 0)}
                    </ui5-table-cell>
                </ui5-table-row>
                <ui5-table-row>
                    <ui5-table-cell class="reporting-table-label">YTD</ui5-table-cell>
                    <ui5-table-cell class="amount">
                        ${formatNumber(ytdCount, 0, 0)}
                    </ui5-table-cell>
                </ui5-table-row>
                <ui5-table-row>
                    <ui5-table-cell class="reporting-table-label">Lifetime</ui5-table-cell>
                    <ui5-table-cell class="amount">
                        ${formatNumber(allCount, 0, 0)}
                    </ui5-table-cell>
                </ui5-table-row>
            </ui5-table>
        </div>
    `
  }

  private renderXdcWalletStatsTable() {
    return html`
        <div class="card light">
            <div class="title">Wallet Balances for Collateral</div>
            <ui5-table no-data-text="No Data" mode="None">
                <ui5-table-column slot="columns">
                    <div>Account Owner</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>Digital Asset</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div>Wallet Address</div>
                </ui5-table-column>
                <ui5-table-column slot="columns">
                    <div class="text-right">Amount</div>
                </ui5-table-column>
                <ui5-table-row>
                    <ui5-table-cell>${this.metrics?.networkAccount?.owner}</ui5-table-cell>
                    <ui5-table-cell class="text-center">${this.metrics?.networkAccount?.currency}</ui5-table-cell>
                    <ui5-table-cell>${this.metrics?.networkAccount?.address}</ui5-table-cell>
                    <ui5-table-cell class="text-center">
                        ${formatNumber(this.metrics?.networkAccount?.amount, 0)}
                    </ui5-table-cell>
                </ui5-table-row>
            </ui5-table>
        </div>
    `
  }

  private renderSentTransactionsStatsTable() {
    const erc20TokenSymbols = _.keys(this.metrics?.sentTransactionStats?.all?.erc20CollateralTotal || {})
    return html`
        <div class="card light">
            <div class="title">Sent Transactions</div>
            <ui5-table id="table" no-data-text="No Data" mode="None" rowHeight="80">
                ${this.renderTransactionsStatsHeader(erc20TokenSymbols, this.config.reportingNetworkTableDisabled)}
                ${this.renderTransactionsStatsRow('Last 24 hrs', this.metrics?.sentTransactionStats?.last24Hours, this.config.reportingNetworkTableDisabled ? this.metrics?.proofMetrics?.last24Hours : undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('MTD', this.metrics?.sentTransactionStats?.mtd, this.config.reportingNetworkTableDisabled ? this.metrics?.proofMetrics?.mtd : undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('YTD', this.metrics?.sentTransactionStats?.ytd, this.config.reportingNetworkTableDisabled ? this.metrics?.proofMetrics?.ytd : undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('Lifetime', this.metrics?.sentTransactionStats?.all, this.config.reportingNetworkTableDisabled ? this.metrics?.proofMetrics?.all : undefined, erc20TokenSymbols)}
            </ui5-table>
        </div>
    `
  }

  private renderReceivedTransactionsStatsTable() {
    const erc20TokenSymbols = _.keys(this.metrics?.receivedTransactionStats?.all?.erc20CollateralTotal || {})
    return html`
        <div class="card light">
            <div class="title">Received Transactions</div>
            <ui5-table id="table" no-data-text="No Data" mode="None">
                ${this.renderTransactionsStatsHeader(erc20TokenSymbols, false)}
                ${this.renderTransactionsStatsRow('Last 24 hrs', this.metrics?.receivedTransactionStats?.last24Hours, undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('MTD', this.metrics?.receivedTransactionStats?.mtd, undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('YTD', this.metrics?.receivedTransactionStats?.ytd, undefined, erc20TokenSymbols)}
                ${this.renderTransactionsStatsRow('Lifetime', this.metrics?.receivedTransactionStats?.all, undefined, erc20TokenSymbols)}
            </ui5-table>
        </div>
    `
  }

  private renderTransactionsStatsHeader(erc20TokenSymbols: string[], displayGasFeeColumns: boolean) {
    return html`
        <ui5-table-column slot="columns">
            <div class="text-right">Period</div>
        </ui5-table-column>
        <ui5-table-column slot="columns">
            <div># of Transactions</div>
        </ui5-table-column>
        <ui5-table-column slot="columns">
            <div>Amount ($)</div>
        </ui5-table-column>
        <ui5-table-column slot="columns">
            <div>Amount (£)</div>
        </ui5-table-column>
        <ui5-table-column slot="columns">
            <div>Amount (€)</div>
        </ui5-table-column>
        <ui5-table-column slot="columns">
            <div>Collateral (XDC)</div>
        </ui5-table-column>
        ${erc20TokenSymbols ?
                erc20TokenSymbols.map(tokenSymbol => html`
                    <ui5-table-column slot="columns">
                        <div>Collateral (${tokenSymbol})</div>
                    </ui5-table-column>
                `)
                : ''}
        ${displayGasFeeColumns ? html`
            <ui5-table-column slot="columns">
                <div>Avg. Gas Fee (XDC)</div>
            </ui5-table-column>
            <ui5-table-column slot="columns">
                <div>Total Gas Fee (XDC)</div>
            </ui5-table-column>
        ` : ''}
    `
  }

  private renderTransactionsStatsRow(title: string, data?: TransactionStatsData, proofMetricsData?: ProofMetricsData, erc20TokenSymbols?: string[]) {
    return html`
        <ui5-table-row>
            <ui5-table-cell class="reporting-table-label">${title}</ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.count, 0, 0)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.moneyTotal['USD'], 0)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.moneyTotal['GBP'], 0)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.moneyTotal['EUR'], 0)}
            </ui5-table-cell>
            <ui5-table-cell class="amount">
                ${formatNumber(data?.nativeCollateralAmountTotal, 0)}
            </ui5-table-cell>
            ${erc20TokenSymbols ?
                    (erc20TokenSymbols).map(tokenSymbol => html`
                      <ui5-table-cell class="amount">
                          ${formatNumber(_.get(data?.erc20CollateralTotal, tokenSymbol), 0)}
                      </ui5-table-cell>
                `)
                    : ''}
            ${proofMetricsData ? html`
                <ui5-table-cell class="amount">
                    ${formatNumber(proofMetricsData?.avgGasUsedXdc, 0, 12)}
                </ui5-table-cell>
                <ui5-table-cell class="amount">
                    ${formatNumber(proofMetricsData?.totalGasUsedXdc, 0, 12)}
                </ui5-table-cell>
            ` : ''}
        </ui5-table-row>
    `;
  }
}
