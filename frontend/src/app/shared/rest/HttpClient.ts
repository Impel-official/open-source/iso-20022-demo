import {messagesStore} from '../messages/MessagesStore';
import {RestError} from "./RestError";
import {AppConfig, config} from "../config";

export class HttpClient {

  private readonly requestTimeoutMs: number;
  private readonly retryAttempts: number;
  private readonly retryTimeIntervalMs: number;

  private token?: string;

  constructor(private config: AppConfig) {
    this.requestTimeoutMs = config.api?.requestTimeoutMs || 30000; // 30s
    this.retryAttempts = config.api?.retryAttempts || 3;
    this.retryTimeIntervalMs = config.api?.retryTimeIntervalMs || 200; // 0.2s
  }

  setAuthToken(token: string) {
    this.token = token;
  }

  getAuthToken() {
    return this.token;
  }

  async post(
      url: string,
      body: any,
      text = false
  ): Promise<any> {
    const jsonBody = JSON.stringify(body);
    return await this.httpRequest(url,{method: 'POST', body: jsonBody}, text);
  }

  async get(url: string, text = false): Promise<any> {
    return await this.httpRequestWithRetry(
      url,
      {method: 'GET'},
      text
    );
  }

  async httpRequestWithRetry(
    url: string, options: RequestInit, text = false
  ): Promise<any> {
    for (let i=1; i <= this.retryAttempts; i++) {
      try {
        return await this.httpRequest(url, options, text);
      } catch (e) {
        if(e instanceof RequestError) {
          await new Promise(r => setTimeout(r, this.retryTimeIntervalMs));
        } else {
          throw e;
        }
      }
    }
    const errorMessage =
      `Error making request, after ${this.retryAttempts} retries`
    // TODO: create class and make this a constructor dependency
    messagesStore.showMessage('Negative', errorMessage)
    throw new Error(errorMessage);
  }

  async httpRequest(
    url: string,
    options: Omit<Omit<RequestInit, 'headers'>, 'signal'>,
    text = false
  ): Promise<any> {
    const controller = new AbortController();
    const enhancedOptions = {
      ...options,
      signal: controller.signal,
      headers: {
        'Accept' : text ? 'text/plain' : 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
        ...this.getAuthorizationHeader()
      },
    };

    setTimeout(() => controller.abort(), this.requestTimeoutMs);
    const response = await fetch(url, enhancedOptions).catch(e => {
      const errorMessage = `Bad Request: ${e.message}`
      throw new RequestError(errorMessage, e)
    });
    if(![200, 201, 202].find(it => it == response.status)) {
      console.warn(`httpRequest: bad response status ${response.status} for ${url}`);
      const errorJsonResponse =  await response.json();
      return Promise.reject(new RestError(
          errorJsonResponse.code,
          errorJsonResponse.message,
          errorJsonResponse.fieldErrors,
      ));
    }
    try {
      if(text) return await response.text();
      return await response.json()
    } catch (e) {
      console.error(e);
      throw new Error('Error parsing response');
    }
  }

  private getAuthorizationHeader(): Record<string, string> | undefined {
    if(!this.token){
      console.warn('getAuthorizationHeader: no token found');
      return undefined;
    }
    return {Authorization: this.token};
  }
}


/*
 * Backend error message type
 */
class ErrorMessage {
  constructor(
    public message: string,
    public exception: string,
    public path: string,
    public status: string,
    public timestamp: string,
    public trace: string) {}
}

export class BaseError extends Error {
  constructor(message: string, public cause?: any) {super(message);}
}
/*
 * The request was made and the server responded with a status code that falls out of the
 * range of 200, Something like 4xx or 500
 */
export class BadResponseError extends BaseError {
  constructor(message: string, public error: ErrorMessage, cause?: any) {
    super(message, cause);
  }
}

/*
 * Something happened in setting up the request that triggered an Error
 */
export class RequestError extends BaseError{
  constructor(message: string, cause?: any) {
    super(message, cause);
  }
}

export const httpClient = new HttpClient(config);
