package io.lab577.xinfin.iso20022.demo.api.dto

data class HubSpotUser(
    val id: String,
    val email: String,
)
