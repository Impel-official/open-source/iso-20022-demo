import {customElement, property, query, state} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {repeat} from 'lit/directives/repeat.js';
import {TransactionsTable} from '../../model/TransactionsTable';
import {TransactionSummary} from '../../model/TransactionSummary';
import {styleMap} from 'lit-html/directives/style-map.js';
import {AcceptOrRejectTransactionsEvent, ChangePagingEvent} from "./TablePaging";
import {TransactionsStore, transactionsStore} from "../../TransactionsStore";
import {AppConfig, config} from "../../../shared/config";
import {PropertyValues} from "@lit/reactive-element";
import {cardCss, commonCss, tableCss, typography} from "../../../shared/config/styles";
import {truncate} from "../../../shared/utils";
import {formatDateTime} from "../../../shared/date-utils";


export type TransactionSelectedEvent = CustomEvent<TransactionSummary>;

const styles = css`
  ${commonCss}
  ${tableCss}
  ${cardCss}
  ${typography}

.table-header {
  display: flex;
  justify-content: space-between;
  flex: 1 1 auto;
  align-items: center;
}

.table-header .title {
  flex: 0;
  margin-bottom: 0;
}
`;

@customElement('transactions-table')
export class TransactionView extends LitElement {
  static override styles = styles;

  @property()
  transactionsTable: TransactionsTable = TransactionsTable.empty();

  @state()
  private isScreenWide = false;

  @query('#table')
  private table: any;
  @query('#tablePaging')
  private tablePaging: any;
  @state()
  private showTableButtons = false;
  @state()
  private selectedTransactions: Array<TransactionSummary> = [];

  private transactionsStore: TransactionsStore;
  private config: AppConfig;

  tableWidthObserver?: ResizeObserver;

  constructor() {
    super();
    this.transactionsStore = transactionsStore;
    this.config = config;
  }

  override firstUpdated() {
    if (!this.tableWidthObserver) this.handleTableWidthChange(1000)
  }

  protected override update(changedProperties: PropertyValues) {
    if (this.table) {
      const th = this.table.shadowRoot.querySelector('div table thead th');
      if (th) th.style.background = '#D9ECFF';
    }
    super.update(changedProperties);
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    if (this.tableWidthObserver) this.tableWidthObserver.disconnect();
  }

  async changePaging(event: ChangePagingEvent) {
    this.transactionsStore.setPaging(event.detail);
  }

  async acceptOrReject(event: AcceptOrRejectTransactionsEvent) {
    this.tablePaging.busy = true;
    await this.transactionsStore.confirmSelectedTransactions(this.selectedTransactions, event.detail);
    for (let i = 0; i < this.table.rows.length; i++) {
      const row = this.table.rows[i];
      if (row.selected) row.selected = false;
    }
    this.selectedTransactions = [];
    this.tablePaging.busy = false;
  }

  private selectionChanged(e: CustomEvent) {
    // Reset selected state
    const selectedTransactions: Array<TransactionSummary> = []
    for (let i = 0; i < e.detail.selectedRows.length; i++) {
      const selectedRow = e.detail.selectedRows[i];
      const txIdToFind = selectedRow.attributes['data-tx-id'].value;
      const transaction = this.transactionsTable.transactions
          .find(it => it.id == txIdToFind);
      if (!transaction) throw Error("Could not find selected transaction in provided transactions table");
      // Only add actionable items
      if (transaction.isActionable()) selectedTransactions.push(transaction);
    }
    this.selectedTransactions = selectedTransactions
  }

  private handleTableWidthChange(width: number) {
    this.tableWidthObserver = new ResizeObserver(
        (entries: ResizeObserverEntry[]) => {
          const tableSize = entries[0]?.borderBoxSize[0]?.inlineSize;
          if (tableSize) this.isScreenWide = tableSize >= 1000;
        })
    this.tableWidthObserver.observe(this.table);
  }

  getStatusStyleClass(tx: TransactionSummary) {
    if (tx.isPositive()) return 'positive';
    else if (tx.isNegative()) return 'negative';
    else if (tx.isActionable()) return 'actionable';
    else return 'none';
  }

  getStatusLabel(tx: TransactionSummary) {
    if (tx.isPositive()) return 'positive';
    else if (tx.isNegative()) return 'negative';
    else if (tx.isActionable()) return 'actionable';
    else return 'none';
  }

  override render() {
    const visibleInWideScreen = {display: this.isScreenWide ? '' : 'none'}
    const visibleInNarrowScreen = {display: this.isScreenWide ? 'none' : ''}
    const showSelectCheckbox = this.config?.featureFlags?.transactionsView?.showSelectCheckbox;

    return html`
        <div class="table-header">
            <div class="title">Transactions</div>
            <table-paging id="tablePaging"
                          class="flex flex-row"
                          .showActionButtonsOnMobile=${false}
                          .transactionsTable=${this.transactionsTable}
                          .selectedTransactions=${this.selectedTransactions}
                          @changePagingEvent=${this.changePaging}
                          @acceptOrRejectEvent=${this.acceptOrReject}
            >
        </div>
        </table-paging>
        <ui5-table id="table"
                   no-data-text="No Data"
                   mode="${showSelectCheckbox ? 'MultiSelect' : 'None'}"
                   @selection-change=${this.selectionChanged}
        >
            <!-- Columns -->
            <ui5-table-column slot="columns">
            <span style=${styleMap(visibleInWideScreen)}>
              Transaction ID
            </span>
                <span style="${styleMap(visibleInNarrowScreen)}">
              Transactions
            </span>
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="Status">
                Status
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="Date">
                Date/Time
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="BIC">
                BIC
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="BIC">
                From IBAN
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="BIC">
                To IBAN
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="Amount">
                Amount
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="Detail">
                Detail
            </ui5-table-column>
            <ui5-table-column slot="columns" min-width='1000' popin-text="Block Explorer">
                Block Explorer
            </ui5-table-column>
            <!-- Rows -->
            ${repeat(this.transactionsTable.transactions, (tx) => tx.id, (tx) => {
                return html`
                    <ui5-table-row data-tx-id=${tx.id} type="${tx.isActionable() ? 'Active' : 'Inactive'}">
                        <ui5-table-cell>
                            <div class="wrap" style=${styleMap(visibleInWideScreen)}>
                                <div title=${tx.endToEndId}>${truncate(tx.endToEndId)}</div>
                            </div>
                            <div class="inline-row" style=${styleMap(visibleInNarrowScreen)}>
                                <div class="inline-col">
                                    <span class="inline-col-label">ID:</span>
                                    <span title=${tx.endToEndId}>${truncate(tx.endToEndId)}</span>
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">Status: </span>
                                    ${tx.buildStatusLabel()}
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">Date/Time:</span>
                                    ${formatDateTime(tx.requestDate)}
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">To BIC: </span>
                                    ${tx.counterPartyBic},
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">From IBAN: </span>
                                    <span title=${tx.senderAccount}>${truncate(tx.senderAccount)}</span>
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">To IBAN: </span>
                                    <span title=${tx.recipientAccount}>${truncate(tx.recipientAccount)}</span>
                                </div>
                                <div class="inline-col">
                                    <span class="inline-col-label">Amount: </span>
                                    ${tx.amount2.getFormatted()} ${tx.currency}
                                </div>
                            </div>
                        </ui5-table-cell>
                        <ui5-table-cell class="${this.getStatusStyleClass(tx)}">
                            ${tx.buildStatusLabel()}
                            ${tx.isPendingExpiry() ? html`
                                <date-countdown class="negative bold" date=${tx.validUntil}
                                                inline=${true}></date-countdown>` : undefined}
                        </ui5-table-cell>
                        <ui5-table-cell>
                            ${formatDateTime(tx.requestDate)}
                        </ui5-table-cell>
                        <ui5-table-cell>
                            ${tx.counterPartyBic}
                        </ui5-table-cell>
                        <ui5-table-cell>
                            <div title=${tx.senderAccount}>${truncate(tx.senderAccount)}</div>
                        </ui5-table-cell>
                        <ui5-table-cell>
                            <div title=${tx.recipientAccount}>${truncate(tx.recipientAccount)}</div>
                        </ui5-table-cell>
                        <ui5-table-cell class="amount">
                            ${tx.amount2.getFormatted()} ${tx.currency}
                        </ui5-table-cell>
                        <ui5-table-cell>
                            <tx-details-link .transaction=${tx}></tx-details-link>
                        </ui5-table-cell>
                        <ui5-table-cell>
                            ${this.renderObservatoryCell(tx)}
                        </ui5-table-cell>
                    </ui5-table-row>
                `;
            })}
        </ui5-table>

        <table-paging id="tablePaging"
                      class="flex flex-row"
                      .transactionsTable=${this.transactionsTable}
                      .selectedTransactions=${this.selectedTransactions}
                      @changePagingEvent=${this.changePaging}
                      @acceptOrRejectEvent=${this.acceptOrReject}
        >
        </table-paging>
    `
  }

  renderObservatoryCell(transaction: TransactionSummary) {
    const busyIndicator = html`
        <ui5-busy-indicator active size="Small"></ui5-busy-indicator>
    `
    if (transaction.isNegative()) {
      return '';
    } else if (transaction.isPending()) {
      return busyIndicator
    } else {
      return transaction.rollupTransactionHash ? html`
          <ui5-link class="link"
                    target="_blank"
                    href="${config.xdc.txPrefixUrl}${transaction.rollupTransactionHash}">
              <ui5-icon class="link" name="sap-icon://search"></ui5-icon>
          </ui5-link>
      ` : busyIndicator;
    }
  }
}
