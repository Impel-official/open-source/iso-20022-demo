import {TransactionsTable} from './model/TransactionsTable';
import {MultipleHostsAbstractStore} from '../shared/MultipleHostsAbstractStore';
import {TransactionsTableFilter} from './model/TransactionsTableFilter';
import {AppConfig, config} from '../shared/config';
import {TransactionsTablePaging} from './model/TransactionsTablePaging';
import {restAdapter, RestAdapter} from "../shared/rest/RestAdapter";
import {
  LocationChangeCallback,
  routingService,
  RoutingService
} from '../shared/routing/RoutingService';
import {ReactiveControllerHost} from 'lit';
import {Transaction} from './model/Transaction';
import {TransactionsTableSorting} from './model/TransactionsTableSorting';
import {stompAdapter, StompAdapter} from '../shared/stomp/StompAdapter';
import {TransactionSummary} from './model/TransactionSummary';
import {TransactionMessageEvent} from '../shared/stomp/TransactionMessageEvent';
import {TransactionDtoStatusEnum} from "../shared/api";

export class TransactionsStore extends MultipleHostsAbstractStore {
  constructor(
    config: AppConfig,
    private routingService: RoutingService,
    private stompAdapter: StompAdapter,
    private restAdapter: RestAdapter
  ) {
    super(config);
  }

  unapprovedCount = 0;

  async init() {
    this.stompAdapter.addListener(
      this,
      'NewTransactions',
      this.handleNewTransactionEvent.bind(this)
    );
    this.stompAdapter.addListener(
      this,
      'TransactionChanges',
      this.handleTransactionMessageEvent.bind(this)
    );
    this.countInboundUnapproved().then(count => {
      transactionsStore.unapprovedCount = count ?? 0;
      if(count > 0) this.updatePropertiesToBoundHosts();
    });
  }

  override bindComponentToPropertyChanges(host: ReactiveControllerHost) {
    super.bindComponentToPropertyChanges(host);
  }

  override unbindComponentFromPropertyChanges(host: ReactiveControllerHost) {
    super.unbindComponentFromPropertyChanges(host);
  }

  transactionsTablePaging: TransactionsTablePaging = TransactionsTablePaging.default();
  transactionsTableFilter: TransactionsTableFilter = TransactionsTableFilter.empty();
  transactionsTableSorting: TransactionsTableSorting = TransactionsTableSorting.default();
  transactionsTable: TransactionsTable = TransactionsTable.empty();
  selectedTransaction?: Transaction = undefined;
  selectedTransactions: Map<string, Transaction> = new Map<string, Transaction>();
  selectedTransactionMessageRaw?: string = undefined;
  isTransactionFiltersPanelOpen = false;

  async getFilteredTransactionsTable() {
    this.transactionsTable =
      await this.restAdapter.getFilteredTransactionSummariesPage(
        this.transactionsTableSorting,
        this.transactionsTablePaging,
        this.transactionsTableFilter
      );
    this.updatePropertiesToBoundHosts();
  }

  async getTransactionMessageRaw(messageId: string) {
    this.selectedTransactionMessageRaw =
      await this.restAdapter.getMessageXML(messageId);
    this.updatePropertiesToBoundHosts();
  }

  async getTransactionDetails(transactionId: string) {
    this.selectedTransaction =
      await this.restAdapter.getTransaction(transactionId);
    this.updatePropertiesToBoundHosts();
  }

  async getTransactionDetailsFromUrl() {
    const transactionId = this.routingService.getRouter().location.params['id'];
    if(typeof transactionId !== 'string')
      throw Error('Transaction id not read correctly from url');
    await this.getTransactionDetails(transactionId);
    this.updatePropertiesToBoundHosts();
  }

  setTransactionsTableFilter(filter: TransactionsTableFilter) {
    this.transactionsTableFilter = filter;
    this.updatePropertiesToBoundHosts();
  }

  setTransactionsPagingFromQueryParams() {
    const params= this.routingService.getUrlSearchParams();
    this.transactionsTablePaging = TransactionsTablePaging.fromUrlParams(params);
    this.updatePropertiesToBoundHosts();
  }

  setTransactionsTableFilterFromQueryParams(){
    const params= this.routingService.getUrlSearchParams();
    this.transactionsTableFilter = TransactionsTableFilter.fromUrlParams(params);
  }

  setFilter(filter: TransactionsTableFilter) {
    this.transactionsTableFilter = filter;
    this.updateUrlParams();
  }

  setPaging(paging: TransactionsTablePaging) {
    this.transactionsTablePaging = paging;
    this.updateUrlParams();
  }

  updateUrlParams() {
    const params: URLSearchParams = new URLSearchParams();
    const pagingParams = TransactionsTablePaging.toUrlParams(this.transactionsTablePaging);
    const filterParams = TransactionsTableFilter.toUrlParams(this.transactionsTableFilter);
    for (const [key, val] of pagingParams.entries()) {
      params.set(key, val);
    }
    for (const [key, val] of filterParams.entries()) {
      params.set(key, val);
    }
    const urlWithFilterQueryParams = this.routingService.setQueryParamsToCurrentUrl(params);
    this.routingService.navigate(urlWithFilterQueryParams);

  }

  addLocationChangesListener(id: any, callback: LocationChangeCallback) {
    this.routingService.addLocationChangesListener(id, callback);
  }

  removeLocationChangesListener(id: any) {
    this.routingService.removeLocationChangesListener(id);
  }

  private updateAllApprovalPending(value: boolean){
    if(this.selectedTransaction)
      this.selectedTransaction = this.selectedTransaction.setIsApprovalPending(value);
    this.selectedTransactions.keys()
    const keys = this.selectedTransactions.keys()
    for(const txId in keys){
      this.updateApprovalPending(txId, value);
    }
  }

  private updateApprovalPending(txId: string, value: boolean){
    if(this.selectedTransaction?.id && this.selectedTransaction.id == txId)
      this.selectedTransaction = this.selectedTransaction.setIsApprovalPending(value);

    const tx = this.selectedTransactions.get(txId);
    if(!tx) return;
    // Remove if no longer pending
    if(!value) this.selectedTransactions.delete(txId);
    // Else update
    else this.selectedTransactions.set(txId, tx.setIsApprovalPending(value))
  }

  async confirmSelectedTransaction(approve: boolean): Promise<void> {
    return new Promise(async (resolve, reject) => {
      if(!this.selectedTransaction) {
        reject("Transaction not selected");
        return;
      }
      this.selectedTransaction = this.selectedTransaction.setIsApprovalPending(true);
      this.updatePropertiesToBoundHosts();
      this.stompAdapter.addListener(
          this, 'TransactionChanges', (transactionMessageEvent: TransactionMessageEvent) => {
            if (!this.selectedTransaction) return;
            if (transactionMessageEvent.transactionId !== this.selectedTransaction.id) return;
            this.selectedTransaction.setIsApprovalPending(false);
            if(this.selectedTransactions.size == 0)
              this.stompAdapter.removeListener(this, 'TransactionChanges');
            resolve();
            this.updatePropertiesToBoundHosts();
          }
      );
      await this.restAdapter.confirmTransferRequest(this.selectedTransaction.id, approve);
    })
  }

  async confirmSelectedTransactions(transactions: Array<TransactionSummary>, approve: boolean): Promise<void> {
    if (!transactions || transactions.length == 0) return;
    this.selectedTransactions.clear();
    for(let i = 0; i < transactions.length; i++){
      const tx = transactions[i];
      this.selectedTransactions.set(tx.id, await this.restAdapter.getTransaction(tx.id));
    }
    return new Promise(async (resolve, reject) => {
      if(!this.selectedTransactions) {
        reject("No Transactions selected");
        return;
      }
      this.updateAllApprovalPending(true);
      this.updatePropertiesToBoundHosts();
      this.stompAdapter.addListener(
          this, 'TransactionChanges', (transactionMessageEvent: TransactionMessageEvent) => {
            this.updateApprovalPending(transactionMessageEvent.transactionId, false);
            if(this.selectedTransactions.size == 0 && this.selectedTransaction)
              this.stompAdapter.removeListener(this, 'TransactionChanges');
            resolve();
            this.updatePropertiesToBoundHosts();
          }
      );
      await this.restAdapter.confirmTransferRequests(Array.from( this.selectedTransactions.keys() ), approve);
    })
  }

  async countInboundUnapproved(): Promise<number> {
    const countRes = await this.restAdapter.countInboundUnapproved();
    return countRes.count
  }

  private async handleNewTransactionEvent(transaction: Transaction) {
    let updateHosts = false;
    // Update unapproved count
    if(transaction.status == 'Received' && transaction.recipientBic == config.bic) {
      transactionsStore.unapprovedCount = transactionsStore.unapprovedCount + 1;
      updateHosts = true;
    }
    const transactionSummary = TransactionSummary.from(transaction);
    // Update Transactions table if we are on a first page and filters match
    if(this.transactionsTable.pageNumber === 0 && transactionSummary.doesMatchTo(this.transactionsTableFilter)) {
      this.transactionsTable = this.transactionsTable.upsertTransaction(TransactionSummary.from(transaction));
      updateHosts = true;
    }
    if(updateHosts) {
      this.updatePropertiesToBoundHosts();
    }
  }

  private async handleTransactionMessageEvent(event: TransactionMessageEvent) {
    let updateHosts = false;
    if(event.status != 'Received' && event.message.recipientBic == config.bic) {
      transactionsStore.unapprovedCount = transactionsStore.unapprovedCount > 0 ? transactionsStore.unapprovedCount - 1 : 0;
      updateHosts = true;
    }
    // update selected transaction if exists and is the one
    if(this.selectedTransaction) {
      if(this.selectedTransaction.id === event.transactionId) {
        this.selectedTransaction = this.selectedTransaction.addMessage(event.message);
        this.selectedTransaction = this.selectedTransaction.updateStatus(event.status).updateRollupTransactionHash(event.message.rollupTransactionHash);
        if(this.selectedTransactions.has(event.transactionId))
          this.selectedTransactions.set(event.transactionId, this.selectedTransaction);
        updateHosts = true;
      }
      if(updateHosts) this.updatePropertiesToBoundHosts();
    }
    // find transaction summary
    const transactionSummary =
      this.transactionsTable.findTransactionSummaryById(event.transactionId);
    if(transactionSummary == undefined) return;
    // transaction exists on current view
    const updatedTransactionSummary = transactionSummary.updateStatus(event.status).updateRollupTransactionHash(event.message.rollupTransactionHash);
    if(updatedTransactionSummary.doesMatchTo(this.transactionsTableFilter)) {
      // if filters match after status change, update table
      this.transactionsTable =
        this.transactionsTable.upsertTransaction(updatedTransactionSummary);
      this.updatePropertiesToBoundHosts();
    } else {
      // if filters do not match refresh because we need to remove it and we will have less
      // number of elements in page than the one's user requested with paging
      await this.getFilteredTransactionsTable();
    }

    // Add a notification
    // TODO: to be implemented
  }

}

export const transactionsStore =
  new TransactionsStore(config, routingService, stompAdapter, restAdapter);
