import {TransferRequestView} from "./TransferRequestView";
import {SingleHostAbstractStore} from "../shared/SingleHostAbstractStore";
import {TransferRequest} from "./models/TransferRequest";
import {
  progressUpdatesActionsStore,
  ProgressUpdatesActionsStore
} from "../shared/progressUpdates/ProgressUpdatesActionsStore";
import {AppConfig, config} from "../shared/config";
import {stompAdapter, StompAdapter} from "../shared/stomp/StompAdapter";
import {BadResponseError} from "../shared/rest/HttpClient";
import {messagesStore, MessagesStore} from "../shared/messages/MessagesStore";
import {ProgressUpdateState} from "../shared/progressUpdates/models/ProgressUpdates";
import {routingService, RoutingService} from "../shared/routing/RoutingService";
import * as TransferRequestProgressUpdates from "./models/TransferRequestProgressUpdates";
import {ProgressUpdate} from "../shared/progressUpdates/models/ProgressUpdate";
import {Transaction} from "../transactions/model/Transaction";
import {restAdapter, RestAdapter} from "../shared/rest/RestAdapter";
import dayjs from "dayjs";

const DETAILED_PROGRESS = "DETAILED_PROGRESS";

export type PendingTransferRequest =
    Omit<TransferRequest, 'validate'>
    & { validUntil: Date, requestDate: Date }

export class TransferRequestStore extends SingleHostAbstractStore<TransferRequestView> {
  pendingTransactionId: string | undefined;
  pendingTransferRequest: PendingTransferRequest | undefined;

  constructor(
      config: AppConfig,
      readonly periodicProgressUpdatesStore: ProgressUpdatesActionsStore,
      private readonly stompAdapter: StompAdapter,
      private readonly messagesStore: MessagesStore,
      private readonly routingService: RoutingService,
      private readonly restAdapter: RestAdapter,
      private localStorage: Storage
  ) {
    super(config);
  }

  setDetailedProgress(value: boolean) {
    return this.localStorage.setItem(DETAILED_PROGRESS, `${value}`);
  }

  isDetailedProgress() {
    const value = this.localStorage.getItem(DETAILED_PROGRESS);
    return value && value == 'true';
  }

  async sendCreateTransferRequest(transferRequest: TransferRequest): Promise<any> {
    const newTxsReceivedAfterSendTransferRequest: Array<Transaction> = [];
    return new Promise<any>(async (resolve, reject) => {
      try {
        this.pendingTransactionId = undefined;
        const requestDate = dayjs();
        // TODO: This should be get from backend or at least from configuration but for demo it is ok
        const validUntilDate = requestDate.add(5, "minutes");
        this.pendingTransferRequest = {
          ...transferRequest,
          requestDate: requestDate.toDate(),
          validUntil: validUntilDate.toDate()
        };

        const intervalMs = transferRequest.delayedTracking
            ? this.config.progressUpdatesDelayedIntervalMs
            : this.config.progressUpdatesIntervalMs
        this.openProgressUpdateDialog(intervalMs);

        this.stompAdapter.addListener(
            this, 'NewTransactions', (transaction: Transaction) => {
              newTxsReceivedAfterSendTransferRequest.push(transaction);
              if (!this.pendingTransactionId) return;
              if (transaction.id !== this.pendingTransactionId) return;

              this.stompAdapter.removeListener(this, 'NewTransactions');
              this.pushSuccessActions();
            }
        );

        this.pendingTransactionId = await this.restAdapter.sendCreateTransferRequest(transferRequest);

        const isPendingTxFoundInNewTxList = newTxsReceivedAfterSendTransferRequest
            .find(it => it.id === this.pendingTransactionId);
        if (isPendingTxFoundInNewTxList) this.pushSuccessActions();
        this.updatePropertiesToBoundHost();
        resolve(this.pendingTransactionId);
      } catch (e: any) {
        console.error('sendCreateTransferRequest errored', e);
        this.updatePropertiesToBoundHost();
        if (e instanceof BadResponseError) {
          this.pushErrorOnMessageVerify();
        } else {
          // unexpected error
          this.periodicProgressUpdatesStore.pushEmptyAction();
          this.periodicProgressUpdatesStore.pushAddAction(
              ProgressUpdate.new('Error', 'Unexpected error')
          );
          this.periodicProgressUpdatesStore.pushStateChangeAction('Failed');
        }
        reject(e);
      }
    })
  }

  private pushSuccessActions() {
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.successState[0]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.successState[1]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.successState[2]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.successState[3]
    );
    this.periodicProgressUpdatesStore.pushStateChangeAction('Success');
  }

  private pushErrorOnMessageVerify() {
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.errorState[0]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.skippedState[1]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.skippedState[2]
    );
    this.periodicProgressUpdatesStore.pushChangeAction(
        TransferRequestProgressUpdates.skippedState[3]
    );
    this.periodicProgressUpdatesStore.pushStateChangeAction('Failed');
  }

  private openProgressUpdateDialog(intervalMs = this.config.progressUpdatesIntervalMs) {
    if (!this.host) return;
    this.periodicProgressUpdatesStore.clearActions();

    this.periodicProgressUpdatesStore.applyActionsPeriodically(
        this.host,
        this.progressUpdateEndedCallback.bind(this),
        intervalMs
    );

    // show waiting
    this.periodicProgressUpdatesStore.pushAddAction(
        TransferRequestProgressUpdates.waitingState[0]
    );
    this.periodicProgressUpdatesStore.pushAddAction(
        TransferRequestProgressUpdates.waitingState[1]
    );
    this.periodicProgressUpdatesStore.pushAddAction(
        TransferRequestProgressUpdates.waitingState[2]
    );
    this.periodicProgressUpdatesStore.pushAddAction(
        TransferRequestProgressUpdates.waitingState[3]
    );
  }

  private progressUpdateEndedCallback(progressUpdateState: ProgressUpdateState) {
    this.updatePropertiesToBoundHost();
  }
}

const localStorage = window.localStorage;
export const transferRequestStore = new TransferRequestStore(
    config,
    progressUpdatesActionsStore,
    stompAdapter,
    messagesStore,
    routingService,
    restAdapter,
    localStorage
);
