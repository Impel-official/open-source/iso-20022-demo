import copy from 'rollup-plugin-copy';
import {summary} from 'rollup-plugin-summary';
import {terser} from 'rollup-plugin-terser';
import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import rollupTypescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';

// eslint-disable-next-line no-undef
const bundleName = process.env.BUNDLE_NAME

const bundlePath = 'dist/prod';

export default {
  input: 'src/index.ts',
  output: {
    file: 'dist/bundle.js',
    format: 'esm',
    sourcemap: true,
    inlineDynamicImports: true
  },
  onwarn(warning) {
    if (warning.code !== 'THIS_IS_UNDEFINED') {
      console.error(`(!) ${warning.message}`);
    }
  },
  plugins: [
    copy({
      targets: [
        { src: 'src/index.html', dest: bundlePath, transform: contents => {
            let content = contents.toString()
            content = content.replace(/META_ROBOTS_VALUE/g, bundleName.startsWith('prod') ? 'all' : 'noindex')
            return content
          }
        },
        ...(bundleName.startsWith('prod') ?
                [] :
                [{ src: 'src/robots.txt', dest: bundlePath }]
        ),
      ]
    }),
    rollupTypescript(),
    replace({'Reflect.decorate': 'undefined'}),
    resolve(),
    terser({
      ecma: 2017,
      module: true,
      warnings: true,
      mangle: {
        properties: {
          regex: /^__/,
        },
      },
    }),
    json(),
    summary()
  ],
};
