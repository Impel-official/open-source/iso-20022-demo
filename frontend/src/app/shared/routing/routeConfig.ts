import {config} from '../config';
import {Commands, Context} from '@vaadin/router';
import {authGuard} from "../auth/AuthGuard";
import {Route} from '@vaadin/router'

export type RouteName =
  'authRedirectUri' |
  'main' |
  'reporting' |
  'transactions' |
  'unapproved' |
  'transaction-details' |
  'transfer-request' |
  'settings' |
  'not-found' |
  'unauthorized';

export type RouteConfig = Route & {name: RouteName}

export const defaultView: RouteName = config.defaultView || 'transactions';

const baseUrl = config.baseUrl;

const routeConfigWithoutAuth: Array<RouteConfig> = [
  {
    path: `${baseUrl}/`,
    name: 'main',
    redirect: `${baseUrl}/${defaultView}`,
  },
  {
    path: `${baseUrl}`,
    name: 'main',
    redirect: `${baseUrl}/${defaultView}`,
  },
  {
    path: `${baseUrl}/unauthorized`,
    name:'unauthorized',
    component: 'unauthorized-view',
    action: undefined,
  },
]

let routeConfigWithOptionalAuth: Array<RouteConfig> = [
  {
    path: `${baseUrl}/reporting`,
    name:'reporting',
    component: 'dashboard-view',
    action: undefined,
  },
  {
    path:`${baseUrl}/transactions`,
    name:'transactions',
    component: 'transactions-view',
    action: undefined,
  },
  {
    path: `${baseUrl}/transactions/unapproved`,
    name:'unapproved',
    component: 'unapproved-view',
    action: undefined,
  },
  {
    path: `${baseUrl}/transactions/:id`,
    name:'transaction-details',
    component: 'transactions-details-view',
    action: undefined,
  },
  {
    path: `${baseUrl}/transfer-request`,
    name:'transfer-request',
    component: 'transfer-request-view',
    action: undefined,
  },
  {
    path: `${baseUrl}/settings`,
    name:'settings',
    component: 'settings-view',
    action: undefined,
  },
];

const authFunction = async (context: Context, commands: Commands) => {
  return await authGuard.isAuthenticated(context, commands, context.pathname);
}

if(config.auth.type !== 'NONE') {
  routeConfigWithOptionalAuth.forEach(it => it.action = authFunction);
}

export const routeConfig: Array<RouteConfig> = [
  ...routeConfigWithoutAuth,
  ...routeConfigWithOptionalAuth,
  {path: '(.*)', name:'not-found', component: 'not-found-view'}
];
