import dayjs from "dayjs";

export const DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm:ss'

export const formatDateTime = (value?: string | Date, format = DATE_TIME_FORMAT): string => {
  if (value == null) {
    return '';
  }

  return dayjs(value).format(format);
}
