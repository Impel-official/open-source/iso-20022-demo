package io.lab577.xinfin.iso20022.demo.api.dto

import java.util.*

data class TransactionsReviewRequestBody(
    val transactionIds: Set<UUID>,
    val approve: Boolean
)
