package io.lab577.xinfin.iso20022.demo.integration.hubspot.config

import com.github.benmanes.caffeine.cache.Caffeine
import org.springframework.cache.caffeine.CaffeineCache
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit

@Configuration
class HubSpotCacheConfig(
    private val hubSpotProperties: HubSpotProperties
) {
    companion object {
        const val HUBSPOT_CACHE = "hubspot-cache"
    }

    @Bean
    fun hubSpotCache(): CaffeineCache {
        return CaffeineCache(
            HUBSPOT_CACHE,
            Caffeine.newBuilder()
                .expireAfterAccess(hubSpotProperties.cacheTtlMinutes, TimeUnit.MINUTES)
                .build()
        )
    }
}
