package io.lab577.xinfin.iso20022.demo.api.dto

class TransactionReviewRequestBody(
    val approve: Boolean
)

