package io.lab577.xinfin.iso20022.demo.domain.entity

import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage

class TransactionMessageProjection(
    msg: TransactionMessageEntity
) : TransactionMessage(
    id = msg.id,
    transactionId = msg.transaction.id,
    senderBic = msg.senderBic,
    recipientBic = msg.recipientBic,
    counterPartyBic = msg.counterPartyBic,
    amount = msg.amount,
    currency = msg.currency,
    direction = msg.direction,
    type = msg.type,
    messageDate = msg.messageDate,
    network = msg.network,
    referenceInfo = msg.referenceInfo
)

