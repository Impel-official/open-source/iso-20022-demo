import { v4 as uuidv4 } from 'uuid';
import formatDistance from 'date-fns/formatDistance'


export type TransferRequest = 'TransferRequest';
export type TransferRequestResponse = 'TransferRequestResponse';

export type NotificationType = TransferRequest | TransferRequestResponse;

type TransferRequestPayload = {transactionId: string};
type TransferRequestResponsePayload = 
  {transactionId: string, isApproved: boolean};

type Payload<T extends NotificationType> =
  T extends TransferRequest ? TransferRequestPayload :
    T extends TransferRequestResponse ? TransferRequestResponsePayload 
      :never;

type NotificationState = 'UNREAD' | 'READ';

export class Notification<T extends NotificationType> {
  constructor(
    public readonly id: string,
    public readonly type: T,
    public readonly title: string,
    public readonly description: string,
    public readonly from: string,
    public readonly timestamp: number,
    public readonly payload: Payload<T>,
    public readonly state: NotificationState
  ) {}
  
  static newId(): string {
    return uuidv4();
  }
  
  markAsRead(): Notification<NotificationType> {
    return this.copy('READ');
  }
  
  copy(state?: NotificationState): Notification<NotificationType> {
    return new Notification(
      this.id,
      this.type,
      this.title,
      this.description,
      this.from,
      this.timestamp,
      this.payload,
      state ? state : this.state
    )
  }
  
  timeAgo(): string {
    return formatDistance(Date.now(), this.timestamp, { addSuffix: true });
  }
}