package io.lab577.xinfin.iso20022.demo.transactions.model

import com.prowidesoftware.swift.constraints.BicConstraint
import com.prowidesoftware.swift.constraints.IbanConstraint
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionMessageCollateralDto
import io.lab577.xinfin.iso20022.demo.pacs.PacsIdBuilder
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.OffsetDateTime
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

/** Request messages, mainly sent via the UI as alternatives to PACS XML */
sealed class OutgoingTransactionMessage private constructor(
    @field:BicConstraint
    @field:NotEmpty
    @Schema(description = "Initiating party's BIC/SWIFT number", required = true)
    val senderBic: String,
    @field:NotEmpty
    @field:BicConstraint
    @Schema(description = "Counterparty BIC/SWIFT number", required = true)
    val recipientBic: String,
    //@Schema(description = "Unique business key for this message", required = true)
    //val messageId: String,
    @field:NotEmpty
    @field:Size(min = 10, max = 200, message = "A pacs id must be up to 35 characters in length")
    @Schema(description = "Unique TX business key that spans multiple messages", required = true)
    val endToEndId: String,
    @Schema(
        description = "Request date in ISO-8601 zoned date-time",
        example = "1970-01-01T02:30:00Z",
        required = false
    )
    val requestDate: OffsetDateTime?,
    @Schema(
        description = "Additional free-text info about this TX message",
        example = "Proforma Invoice 1234",
        required = false
    )
    val referenceInfo: String?// = null,
) {
    /** Maps to a PACS 009 message */
    open class TransferRequest(
        senderBic: String,
        recipientBic: String,
        @field:NotEmpty
        @field:IbanConstraint
        @Schema(description = "Initiating party's IBAN number", required = true)
        val senderAccount: String,
        @field:NotEmpty
        @field:IbanConstraint
        @Schema(description = "Counterparty IBAN number", required = true)
        val recipientAccount: String,
        @Schema(description = "Transaction amount", required = true)
        val amount: BigDecimal,
        @Schema(description = "Transaction currency", required = true)
        val currency: String,
        //@Schema(description = "Transaction status", required = true)
        //val status: TransactionStatus,
        //messageId: String,// = PacsId.randomId(),
        endToEndId: String = PacsIdBuilder.randomId(),
        requestDate: OffsetDateTime?,
        val validUntil: OffsetDateTime?,
        referenceInfo: String?,
        val collateral: TransactionMessageCollateralDto? = null
    ) : OutgoingTransactionMessage(
        senderBic = senderBic,
        recipientBic = recipientBic,
        //messageId = messageId,
        endToEndId = endToEndId,
        requestDate = requestDate,
        referenceInfo = referenceInfo
    )

    /** Maps to a PACS 002 message */
    class TransferResponse(
        senderBic: String,
        recipientBic: String,
        @Schema(description = "Transaction amount", required = true)
        val amount: BigDecimal,
        @Schema(description = "Transaction currency", required = true)
        val currency: String,
        //messageId: String = PacsId.randomId(),
        endToEndId: String = PacsIdBuilder.randomId(),
        requestDate: OffsetDateTime = OffsetDateTime.now(),
        val validUntil: OffsetDateTime = OffsetDateTime.now().plusDays(5),
        val reject: Boolean = false,
        referenceInfo: String?,
        val collateral: TransactionMessageCollateralDto? = null
    ) : OutgoingTransactionMessage(
        senderBic = senderBic,
        recipientBic = recipientBic,
        //messageId = messageId,
        endToEndId = endToEndId,
        requestDate = requestDate,
        referenceInfo = referenceInfo
    )
}

