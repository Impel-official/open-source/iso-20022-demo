package io.lab577.xinfin.iso20022.demo.pacs

import com.prowidesoftware.swift.model.mx.MxPacs00200112
import com.prowidesoftware.swift.model.mx.MxPacs00900110
import io.lab577.xinfin.iso20022.common.lib.iso.support.toOffsetDateTime
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageInfo
import io.lab577.xinfin.iso20022.demo.mapper.CollateralMapper
import java.time.OffsetDateTime


object PacsOverviewConverter : PacsProcessor<PacsOverview>() {

    override fun process(message: MessageInfo, mx: MxPacs00200112): PacsOverview {
        return PacsOverview(
//            end2endId = mx.fiToFIPmtStsRpt.txInfAndSts.find { it.orgnlEndToEndId != null }?.orgnlEndToEndId
//                ?: error("Could not find an end-to-end id"),
            // Instead of using e2e id from a message we use pacs009 id just to avoid issues with the reused e2e id from document
            end2endId = message.endToEndId,
            senderBic = message.sender,
            recipientBic = message.recipient,
            senderAccount = null,
            recipientAccount = null,
            amount = mx.fiToFIPmtStsRpt.orgnlGrpInfAndSts.first().orgnlCtrlSum,
            currency = "USD",
            messageDate = message.created ?: OffsetDateTime.now(),
            validUntil = null,
            referenceInfo = null,
            rawXml = message.payload,
            type = PacsType.PACS_002,
            statusOverride = if (mx.fiToFIPmtStsRpt
                    ?.orgnlGrpInfAndSts?.firstOrNull()?.grpSts == "RJCT"
            ) TransactionStatus.Rejected else null,
            collateral = CollateralMapper.mapCollateralToMessageCollateral(message.collateral)
        )
    }

    override fun process(message: MessageInfo, mx: MxPacs00900110): PacsOverview {
        return PacsOverview(
//            end2endId = mx.fiCdtTrf.cdtTrfTxInf.find { it.pmtId.endToEndId != null }?.pmtId?.endToEndId
//                ?: error("Could not find an end-to-end id"),
            // Instead of using e2e id from a message we use pacs009 id just to avoid issues with the reused e2e id from document
            end2endId = message.id.toString().replaceFirst("-", ""),
            senderBic = message.sender,
            recipientBic = message.recipient,
            senderAccount = mx.fiCdtTrf.grpHdr.sttlmInf.sttlmAcct.id.iban,
            recipientAccount = mx.fiCdtTrf.grpHdr.sttlmInf.instdRmbrsmntAgtAcct.id.iban,
            amount = mx.fiCdtTrf.grpHdr.ttlIntrBkSttlmAmt.value,
            currency = mx.fiCdtTrf.grpHdr.ttlIntrBkSttlmAmt.ccy,
            messageDate = message.created ?: OffsetDateTime.now(),
            validUntil = mx.fiCdtTrf.cdtTrfTxInf.first().sttlmTmIndctn.cdtDtTm.toOffsetDateTime(),
            referenceInfo = mx.fiCdtTrf.cdtTrfTxInf.find { it.pmtId.clrSysRef != null }?.pmtId?.clrSysRef
                ?: error("Could not find an end-to-end id"),
            rawXml = message.payload,
            type = PacsType.PACS_009,
            collateral = CollateralMapper.mapCollateralToMessageCollateral(message.collateral)
        )
    }
}

