package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageRequest
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageRequestCollateral
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageResponse
import io.lab577.xinfin.iso20022.demo.mapper.PacsMapper
import io.lab577.xinfin.iso20022.demo.transactions.model.OutgoingTransactionMessage
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("api/gateway/iso20022/pacs/messages")
class PacsMessagesControllerImpl(
    private val iso20022ClientService: Iso20022ClientService,
    private val pacsMapper: PacsMapper
) : PacsMessagesController {

    @PostMapping("/pacs-009-001-10")
    override fun sendPacs009Message(@RequestBody transferRequest: OutgoingTransactionMessage.TransferRequest): ResponseEntity<SendMessageResponse> {
        val pacs009 = pacsMapper.mapToPacs009(transferRequest)
        val request = SendMessageRequest()
        request.endToEndId = UUID.randomUUID().toString()
        request.sender = transferRequest.senderBic
        request.recipient = transferRequest.recipientBic
        request.payloadType = SendMessageRequest.PayloadTypeEnum.ISO20022
        request.payload = pacs009
        request.collateral = transferRequest.collateral?.let {
            val collateral = SendMessageRequestCollateral()
            collateral.type = when (it.type) {
                TransactionMessageCollateralType.NATIVE -> SendMessageRequestCollateral.TypeEnum.NATIVE
                TransactionMessageCollateralType.ERC20 -> SendMessageRequestCollateral.TypeEnum.ERC20
            }
            collateral.amount = it.amount
            collateral.exchangePrice = it.exchangePrice
            collateral.exchangeCurrency = it.exchangeCurrency
            collateral.tokenSymbol = it.tokenSymbol
            collateral
        }
        return ResponseEntity.ok(iso20022ClientService.sendMessage(request))
    }

    @PostMapping("/pacs-002-001-12")
    override fun sendPacs002Message(@RequestBody transferResponse: OutgoingTransactionMessage.TransferResponse): ResponseEntity<SendMessageResponse> {
        val pacs002 = pacsMapper.mapToPacs002(transferResponse)
        val request = SendMessageRequest()
        request.endToEndId = UUID.randomUUID().toString()
        request.sender = transferResponse.senderBic
        request.recipient = transferResponse.recipientBic
        request.payloadType = SendMessageRequest.PayloadTypeEnum.ISO20022
        request.payload = pacs002
        request.collateral = transferResponse.collateral?.let {
            val collateral = SendMessageRequestCollateral()
            collateral.type = when (it.type) {
                TransactionMessageCollateralType.NATIVE -> SendMessageRequestCollateral.TypeEnum.NATIVE
                TransactionMessageCollateralType.ERC20 -> SendMessageRequestCollateral.TypeEnum.ERC20
            }
            collateral.amount = it.amount
            collateral.exchangePrice = it.exchangePrice
            collateral.exchangeCurrency = it.exchangeCurrency
            collateral.tokenSymbol = it.tokenSymbol
            collateral
        }
        return ResponseEntity.ok(iso20022ClientService.sendMessage(request))
    }
}
