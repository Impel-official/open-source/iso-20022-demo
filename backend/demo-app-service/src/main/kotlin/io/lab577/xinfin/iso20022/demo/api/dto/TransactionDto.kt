package io.lab577.xinfin.iso20022.demo.api.dto

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*

/** A transaction groups a set of pacs message interactions within a single process */
open class TransactionDto(
    id: UUID,
    version: Long?,
    senderBic: String,
    recipientBic: String,
    counterPartyBic: String,
    senderAccount: String,
    senderXdcAddress: String,
    recipientAccount: String,
    amount: BigDecimal,
    currency: String,
    direction: TransactionMessageDirection,
    status: TransactionStatus,
    requestDate: OffsetDateTime,
    validUntil: OffsetDateTime,
    endToEndId: String,
    referenceInfo: String?,
    collateral: TransactionMessageCollateralDto?,
    initiator: String,
    responder: String?,
    rollupTransactionHash: String?,
    @Schema(description = "Transaction messages if included in the response")
    val messages: List<TransactionMessage> = emptyList()
) : TransactionSummary(
    id = id,
    version = version,
    senderBic = senderBic,
    recipientBic = recipientBic,
    counterPartyBic = counterPartyBic,
    senderAccount = senderAccount,
    senderXdcAddress = senderXdcAddress,
    recipientAccount = recipientAccount,
    amount = amount,
    currency = currency,
    direction = direction,
    status = status,
    requestDate = requestDate,
    validUntil = validUntil,
    endToEndId = endToEndId,
    referenceInfo = referenceInfo,
    collateral = collateral,
    initiator = initiator,
    responder = responder,
    rollupTransactionHash = rollupTransactionHash
)
