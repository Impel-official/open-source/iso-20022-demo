import {State} from "./State";
import {Currency} from "./Currency";
import {Party} from "./Party";
import {Account} from "./Account";

export class ReferenceData {
    public constructor(
        public bic: string,
        public states: State[],
        public parties: Party[],
        public currencies: Currency[],
        public accounts: Account[],
    ) {
    }

    static fromJson(json: any): ReferenceData {
        const bic = json['bic'];
        const currencies = json['currencies'].map((it: any) => Currency.fromJson(it));
        const states = json['statuses'].map((it: any) => State.fromJson(it));
        const parties = json['parties'].map((it: any) => Party.fromJson(it));
        const accounts = json['accounts'].map((it: any) => Account.fromJson(it));

        // TODO: this needs json schema validation
        return new ReferenceData(
            bic, states, parties, currencies, accounts
        )
    }
}
