import {authService, AuthService} from "./AuthService";
import {Commands, Context, RedirectResult} from '@vaadin/router';
import {AppConfig, config} from "../config";

export class AuthGuard {

  constructor(
    private config: AppConfig,
    private authService: AuthService
  ) {}

  public async isAuthenticated(
    context: Context,
    commands: Commands,
    pathRedirect: string
  ): Promise<RedirectResult | undefined> {
    const isAuthenticated = this.authService.isAuthenticated();
    if (!isAuthenticated) {
      this.authService.redirectToLoginPage(pathRedirect);
    }
    if(isAuthenticated) {
      await this.authService.authenticate();
    }
    return undefined;
  }
}

export const authGuard = new AuthGuard(config, authService);

