import {css, html, LitElement} from 'lit';
import {customElement, query} from 'lit/decorators.js';
import {classMap} from 'lit/directives/class-map.js';
import { NotificationsStore, notificationsStore } from "./NotificationsStore";
import {
  Notification,
  NotificationType,
  TransferRequestResponse
} from "./models/Notification";
import {repeat} from 'lit/directives/repeat.js';

const styles = css`
.icon {
  position: relative;
  display: flex;
}
.badge {
  position: absolute;
  background: #54CCCC;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
}
.one-digit {
  top: 0;
  right: -8px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
}
.two-digits {
  top: 0;
  right: -10px;
  width: 22px;
  height: 20px;
  border-radius: 50%;
}
.three-digits {
  top: 0;
  right: -20px;
  width: 32px;
  height: 20px;
  border-radius: 45%;
}
#popover {
  width: 600px;
}
.none {
  display: none;
}
`

@customElement('notification-icon')
export class NotificationIconElement extends LitElement {
  static override styles = styles;

  @query('#button')
  button: any;
  @query('#popover')
  popover: any;

  private notificationsStore: NotificationsStore;
  
  constructor() {
    super();
    this.notificationsStore = notificationsStore;
  }
  
  override connectedCallback() {
    super.connectedCallback();
    this.notificationsStore.bindComponentToPropertyChanges(this);
  }

  private toggleNotificationsList() {
    this.popover.isOpen() ?
      this.popover.close() :
      this.popover.showAt(this.button);
  }
  
  private removeNotification(notification: Notification<NotificationType>) {
    this.notificationsStore.remove(notification.id);
  }
  
  private handleNotificationAction(
    notification: Notification<NotificationType>
  ): void {
    // no select event is provided from sap ui, so I used click event,
    // issue when you click to close notification:
    // - first close event is triggered then 
    // - then click event is triggered
    if(this.notificationsStore.notifications.has(notification.id)) {
      this.popover.close();
      this.notificationsStore.handleNotificationAction(notification);
    }
  }
  
  renderAvatar(notification: Notification<NotificationType>) {
    switch (notification.type){
      case "TransferRequest": {
        return html`
          <ui5-avatar size="XS"
                      icon="sap-icon://monitor-payments"
                      color-scheme=${notification.state === 'READ' ? 'Accent10': ''}
                      slot="avatar">
          </ui5-avatar>
        `
      }
      case "TransferRequestResponse": {
        const transferRequestResponseNotification = 
          notification as Notification<TransferRequestResponse>;
        const isApproved = 
          transferRequestResponseNotification.payload.isApproved;

        return html`
          <ui5-avatar size="XS"
                      icon="sap-icon://monitor-payments"
                      color-scheme=${notification.state === 'READ' ? 'Accent10': ''}
                      slot="avatar"
                      
          >
          </ui5-avatar>
        `
      }
    }
    return undefined
  }
  
  renderPriority(notification: Notification<NotificationType>): string {
    switch (notification.type) {
      case "TransferRequest": {
        return 'None';
      }
      case "TransferRequestResponse": {
        const transferRequestResponseNotification =
          notification as Notification<TransferRequestResponse>;
        const isApproved =
          transferRequestResponseNotification.payload.isApproved;
        return isApproved ? 'Low' : 'High'
      }
    }
  }
  
  renderNotification(notification: Notification<NotificationType>) {
    return html`
      <ui5-li-notification
        show-close
        title-text=${notification.title}
        .read=${notification.state === 'READ'}
        @close=${() => this.removeNotification(notification)}
        @click=${() => this.handleNotificationAction(notification)}
        .priority=${this.renderPriority(notification)}
      >
        ${notification.description}
        ${this.renderAvatar(notification)}
        <span slot="footnotes">${notification.from}</span>
        <span slot="footnotes">${notification.timeAgo()}</span>
      </ui5-li-notification>
    `
  }


  override render() {
    const unreadCount = this.notificationsStore.notifications.unreadCount.toString();
    const notifications = this.notificationsStore.notifications.notifications;
    const badgeClasses = {
      badge: true,
      none: unreadCount === '0',
      'one-digit': unreadCount.length === 1,
      'two-digits': unreadCount.length === 2,
      'three-digits': unreadCount.length > 2,
    };
    return html`
      <div class="icon">
        <ui5-button id="button"
                    icon="bell"
                    design="Transparent"
                    @click=${this.toggleNotificationsList}
        >
        </ui5-button>
        <span class=${classMap(badgeClasses)}>${unreadCount}</span>
      </div>

      <ui5-popover id="popover"
                   placement-type="Bottom"
                   horizontal-align="Center"
      >
        <ui5-list header-text="Notifications">
          ${repeat(notifications,
            (notification) => notification.id,
            (notification, _) => this.renderNotification(notification)
          )}
        </ui5-list>
      </ui5-popover>
    `;
  }
}