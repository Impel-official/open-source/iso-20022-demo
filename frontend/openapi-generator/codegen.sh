#!/usr/bin/env bash

# Usage: codegen.sh [<module>] [<update>]
#
# Performs API generation using openapi-generator.
#
# Arguments:
#    module - name of a module to generate. Default value is "all".
#    update - updates API module directory instead of removing it before generation. Default value is false.
#
# Example: codegen.sh file false
# Example: codegen.sh

API_DOCS="\
  http://localhost:8095/v3/api-docs
"

OUTPUT_DIRECTORY="src/app/shared/api"


main () {

    module="$1"
    update="$2"


    if [[ -z ${module} ]]; then
        module="all"
    fi

    for doc in `echo -e ${API_DOCS}`; do
        docName=$(echo "${doc}" | awk -F"/" '{print $4}')

        if [[ -z ${docName} ]]; then
            echo "Couldn't extract module name from input ${doc}"
            exit;
        fi


        if [[ ${module} == "all" ]] || [[ ${module} == "${docName}" ]]; then
            echo "Generating API from ${doc}"

            if [[ -z ${update} ]]; then
                echo "Removing previous API ${OUTPUT_DIRECTORY}"
                rm -rf ${OUTPUT_DIRECTORY}
            fi

            npx openapi-generator-cli generate \
            -i "${doc}" -t openapi-generator/template -g typescript-fetch \
            -o ${OUTPUT_DIRECTORY} --skip-validate-spec -c openapi-generator/config.json
        fi
    done

    npx eslint --fix "${OUTPUT_DIRECTORY}/**/*.{js,jsx,ts,tsx}"
    npx prettier --write "${OUTPUT_DIRECTORY}/**/*.{js,jsx,ts,tsx}"
}

main "$@"
