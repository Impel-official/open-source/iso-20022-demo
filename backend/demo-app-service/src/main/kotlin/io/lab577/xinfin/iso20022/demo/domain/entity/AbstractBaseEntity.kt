package io.lab577.xinfin.iso20022.demo.domain.entity

import java.util.*
import javax.persistence.*


/**
 * Provides an "Version-Property" state detection strategy
 * for Spring to detect whether an entity is new or not.
 *
 * The strategy also provides for optimistic locking
 * and allows a non-nullable [id], so we find it preferable
 * to implementing org.springframework.data.domain.Persistable.
 *
 * See https://docs.spring.io/spring-data/jpa/docs/current-SNAPSHOT/reference/html/#jpa.entity-persistence.saving-entites.strategies
 */
@MappedSuperclass
abstract class AbstractBaseEntity(givenId: UUID? = null) : BaseEntity<UUID> {

    @Id
    @Column(name = "id", length = 16, unique = true, nullable = false)
    override var id: UUID = givenId ?: UUID.randomUUID()

    @Version
    var version: Long? = null

    @Transient
    private val clazz = "jpa"

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other == null -> false
            other !is AbstractBaseEntity -> false
            else -> id == other.id
        }
    }

    override fun hashCode(): Int = id.hashCode()
}
