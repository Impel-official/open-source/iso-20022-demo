export type MessageType = 'Information' | 'Positive' | 'Negative' | 'Warning';

export class Message {
  constructor(
    public readonly id: string,
    public readonly type: MessageType,
    public readonly description: string
  ) {}
}