package io.lab577.xinfin.iso20022.demo.api

import io.lab577.xinfin.iso20022.demo.api.dto.*
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import io.lab577.xinfin.iso20022.demo.metrics.TransactionAllMetrics
import io.lab577.xinfin.iso20022.demo.service.TransactionMetricsService
import io.lab577.xinfin.iso20022.demo.service.TransactionMessageService
import io.lab577.xinfin.iso20022.demo.service.TransactionService
import io.lab577.xinfin.iso20022.demo.transactions.model.*
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("api/gateway/iso20022")
class PacsTransactionsControllerImpl(
    @Value("\${app.bic}")
    var bic: String,
    private val transactionService: TransactionService,
    private val transactionMessageService: TransactionMessageService,
    private val transactionMetricsService: TransactionMetricsService,
    private val iso20022ClientService: Iso20022ClientService
) : PacsTransactionsController {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(PacsTransactionsControllerImpl::class.java)
    }

    @Value("\${app.tx.options.currencies}")
    lateinit var currencies: List<String>

    // TODO: Move below to separate config service and make service method cacheable or something like that
    val referenceDataDtoCache: ReferenceDataDto by lazy {
        ReferenceDataDto(
            bic = bic,
            currencies = currencies,
            statuses = TransactionStatus.values().map { it.toString() },
            // TODO: By default only first 50 items are get
            parties = iso20022ClientService.findParties().sortedBy { it.name },
            accounts = iso20022ClientService.findPartyAccounts().sortedBy { it.holder },
        )
    }

    @GetMapping("transaction-metrics")
    override fun getAllMetrics(): TransactionAllMetrics = transactionMetricsService.getAllMetrics()

    @GetMapping("transaction-options")
    override fun getReferenceData(): ReferenceDataDto = referenceDataDtoCache

    @PostMapping("/review")
    override fun review(
        @Valid
        @RequestBody
        transactionsReviewRequestBody: TransactionsReviewRequestBody
    ): ResponseEntity<TransactionsReviewResponse> {
        val reviewedTransactions = mutableSetOf<UUID>()
        val failedTransactions = mutableSetOf<UUID>()
        transactionsReviewRequestBody.transactionIds.forEach {
            try {
                transactionService.review(it, transactionsReviewRequestBody.approve)
                reviewedTransactions.add(it)
            } catch (ex: Exception) {
                logger.error("Error occurred on transaction review {transactionId: $it, approve: ${transactionsReviewRequestBody.approve}}")
                failedTransactions.add(it)
            }
        }
        return ResponseEntity.ok(
            TransactionsReviewResponse(
                reviewedTransactions = reviewedTransactions,
                failedTransactions = failedTransactions
            )
        )
    }

    @GetMapping("transactions")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Find transactions", description = "Returns a transactions page")
    fun find(
        @Parameter(description = "The RSQL filter to apply as search  criteria", required = false)
        @RequestParam(required = false, defaultValue = "")
        filter: String,
        @Parameter(description = "The zero-indexed page number, optional", required = false, example = "0")
        @RequestParam("pn", required = false, defaultValue = "0")
        pn: Int,
        @Parameter(description = "The page size, optional with default being 10", required = false, example = "10")
        @RequestParam("ps", required = false, defaultValue = "10")
        ps: Int,
        @Parameter(description = "The field to use for sorting", required = false, example = "requestDate")
        @RequestParam("sort", required = false, defaultValue = "requestDate")
        sortBy: String,
        @Parameter(description = "The sort direction, either ASC or DESC", required = false, example = "DESC")
        @RequestParam("direction", required = false, defaultValue = "DESC")
        sortDirection: Sort.Direction
    ): Page<TransactionSummary> {
        return transactionService
            .findPagedByFilter(filter, sortBy, sortDirection, pn, ps)
            .map { it.toSummaryDto() }
    }

    @GetMapping("transactions/{transactionId}")
    @Operation(summary = "Find transaction by ID", description = "Returns a single transaction")
    fun findById(
        @Parameter(description = "ID of transaction", required = true)
        @PathVariable
        transactionId: String?
    ): ResponseEntity<TransactionDto> {

        return transactionService.findById(UUID.fromString(transactionId))
            ?.let {
                ResponseEntity(
                    it.toDto(transactionMessageService.findAllSummariesByTransactionId(it.id)),
                    HttpStatus.OK
                )
            }
            ?: ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @GetMapping("transaction-messages/{transactionMessageId}/raw")
    @Operation(
        summary = "Find the raw XML content for transaction message by ID",
        description = "Returns a single transaction"
    )
    fun getRawMessageById(
        @Parameter(description = "ID of the transaction message", required = true)
        @PathVariable
        transactionMessageId: UUID
    ): ResponseEntity<String> = transactionMessageService.findRawContentById(transactionMessageId)
        ?.let { ResponseEntity(it, HttpStatus.OK) }
        ?: ResponseEntity(HttpStatus.NOT_FOUND)


    @GetMapping("transactions/{transactionId}/messages")
    @Operation(
        summary = "Get paged messages for a transaction",
        description = "Get paged messages for the transaction matching the given ID"
    )
    fun findMessagesByTxId(
        @Parameter(description = "ID of the transaction", required = true)
        @PathVariable
        transactionId: UUID,
        @Parameter(description = "The RSQL filter to apply as search  criteria", required = false)
        @RequestParam(required = false, defaultValue = "")
        filter: String,
        @Parameter(description = "The zero-indexed page number, optional", required = false, example = "0")
        @RequestParam("pn", required = false, defaultValue = "0")
        pn: Int,
        @Parameter(description = "The page size, optional with default being 10", required = false, example = "10")
        @RequestParam("ps", required = false, defaultValue = "10")
        ps: Int,
        @Parameter(description = "The field to use for sorting", required = false, example = "name")
        @RequestParam("sort", required = false, defaultValue = "messageDate")
        sortBy: String,
        @Parameter(description = "The sort direction, either ASC or DESC", required = false, example = "DESC")
        @RequestParam("direction", required = false, defaultValue = "DESC")
        sortDirection: Sort.Direction
    ): ResponseEntity<Page<TransactionMessage>> {
        val txFilter = "transaction.id==$transactionId"
        val completeFilter = if (filter.isBlank()) txFilter else "$txFilter;($filter)"
        val resultsPage = transactionMessageService
            .findPagedByFilter(completeFilter, sortBy, sortDirection, pn, ps)
            .map { it.toDto() }
        return ResponseEntity(resultsPage, HttpStatus.OK)
    }

    @GetMapping("transactions/{transactionId}/messages/all")
    @Operation(
        summary = "Get all messages for a transaction",
        description = "Get all messages for the transaction matching the given ID"
    )
    fun findAllMessagesByTxId(
        @Parameter(description = "ID of the transaction", required = true)
        @PathVariable
        transactionId: UUID
    ): ResponseEntity<List<TransactionMessage>> {
        return transactionService.findById(transactionId)?.let { transactionEntity ->
            val transaction = transactionEntity.messages.map { it.toDto() }
            ResponseEntity(transaction, HttpStatus.OK)
        } ?: ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @GetMapping("/transactions/unapproved/count")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Find transactions", description = "Returns a transactions page")
    fun findUnapproved(): CountResponse {
        return CountResponse(transactionService.count("recipientBic==${bic};status==Received"))
    }

    @PostMapping("/transactions/{transactionId}/review")
    override fun review(
        @PathVariable
        transactionId: UUID,
        @Valid
        @RequestBody
        transactionReviewRequestBody: TransactionReviewRequestBody
    ): ResponseEntity<Unit> {
        transactionService.review(transactionId, transactionReviewRequestBody.approve)
        return ResponseEntity(HttpStatus.OK)
    }
}
