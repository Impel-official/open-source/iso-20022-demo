package io.lab577.xinfin.iso20022.demo.domain.entity

import java.time.OffsetDateTime
import java.time.ZoneOffset.UTC
import javax.persistence.Column
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Version


/**
 * Provides an "Version-Property" state detection strategy
 * for Spring to detect whether an entity is new or not.
 *
 * The strategy also provides for optimistic locking
 * and allows a non-nullable [id], so we find it preferable
 * to implementing org.springframework.data.domain.Persistable.
 *
 * See https://docs.spring.io/spring-data/jpa/docs/current-SNAPSHOT/reference/html/#jpa.entity-persistence.saving-entites.strategies
 */
@MappedSuperclass
abstract class AbstractOffsetDateTimeEntity(givenId: OffsetDateTime? = null) : BaseEntity<OffsetDateTime> {

    @Id
    @Column(unique = true, nullable = false)
    override var id: OffsetDateTime = givenId ?: OffsetDateTime.now(UTC)

    @Version
    var version: Long? = null

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other == null -> false
            other !is AbstractOffsetDateTimeEntity -> false
            else -> id == other.id
        }
    }

    override fun hashCode(): Int = id.hashCode()
}
