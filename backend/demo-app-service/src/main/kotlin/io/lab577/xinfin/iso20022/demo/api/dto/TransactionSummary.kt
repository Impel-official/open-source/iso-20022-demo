package io.lab577.xinfin.iso20022.demo.api.dto

import io.lab577.xinfin.iso20022.demo.api.dto.TransactionMessageCollateralDto
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*

open class TransactionSummary(
    @Schema(description = "Unique identifier for this transaction", required = true)
    val id: UUID,
    val version: Long?,
    val senderBic: String,
    val recipientBic: String,
    @Schema(description = "Initiating party's IBAN number", required = true)
    val senderAccount: String,
    @Schema(description = "The sender XDC wallet/address", required = true)
    var senderXdcAddress: String,
    @Schema(description = "Counterparty IBAN number", required = true)
    val recipientAccount: String,
    @Schema(description = "The actual counterparty, either sender or receiver", required = true)
    val counterPartyBic: String,
    @Schema(description = "Transaction amount", required = true)
    val amount: BigDecimal,
    @Schema(description = "Transaction currency", required = true)
    val currency: String,
    @Schema(description = "Transaction direction", required = true)
    val direction: TransactionMessageDirection,
    @Schema(description = "Current status of the transaction", required = true)
    val status: TransactionStatus,
    @Schema(
        description = "Transaction date in ISO-8601 zoned date-time",
        example = "1970-01-01T02:30:00Z",
        required = false
    )
    val requestDate: OffsetDateTime,
    @Schema(
        description = "Transaction validity deadline in ISO-8601 zoned date-time",
        example = "1970-01-01T02:30:00Z",
        required = false
    )
    val validUntil: OffsetDateTime,
    val endToEndId: String,
    val referenceInfo: String?,
    @Schema(description = "Collateral")
    var collateral: TransactionMessageCollateralDto?,
    @Schema(description = "The user that created the TX", example = "user1", required = false)
    val initiator: String?,
    @Schema(description = "The user that accepted ot rejected the TX", example = "user2", required = false)
    val responder: String?,
    @Schema(
        description = "The rollup TX hash",
        example = "0x59f0cf5c25d6e5cc8bffe9127bbcc2cdbf1b9257a59d189cc21777057429f6XX"
    )
    var rollupTransactionHash: String? = null
)
