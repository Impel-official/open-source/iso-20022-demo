package io.lab577.xinfin.iso20022.demo.transactions.model

import com.prowidesoftware.swift.constraints.BicConstraint
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*
import javax.validation.constraints.NotEmpty

open class TransactionMessage(
    @field:NotEmpty
    @Schema(description = "Unique identifier for this message", required = true)
    val id: UUID,
    @field:NotEmpty
    @Schema(description = "Unique identifier for the transaction this message belongs to", required = true)
    val transactionId: UUID,
    @field:BicConstraint
    @field:NotEmpty
    @Schema(description = "Sender BIC/SWIFT number", required = true)
    val senderBic: String,
    @field:BicConstraint
    @field:NotEmpty
    @Schema(description = "Recipient BIC/SWIFT number", required = true)
    val recipientBic: String,
    @field:BicConstraint
    @field:NotEmpty
    @Schema(description = "Counterparty BIC/SWIFT number", required = true)
    val counterPartyBic: String,
    @field:NotEmpty
    @Schema(description = "Transaction amount", required = true)
    val amount: BigDecimal,
    @field:NotEmpty
    @Schema(description = "Transaction currency", required = true)
    val currency: String,
    @Schema(description = "Message direction", required = true)
    val direction: TransactionMessageDirection,
    @Schema(description = "Transaction status", required = true)
    val type: PacsType,
    @Schema(
        description = "Additional free-text info about this TX message",
        example = "Proforma Invoice 1234",
        required = false
    )
    val referenceInfo: String?,// = null,
    @Schema(
        description = "Message date in ISO-8601 zoned date-time",
        example = "1970-01-01T02:30:00Z",
        required = false
    )
    val messageDate: OffsetDateTime,
    @field:NotEmpty
    @Schema(description = "Network used to carry the message", required = true)
    val network: String,
    @Schema(description = "Rollup transaction hash containing corresponding to this transaction", required = false)
    val rollupTransactionHash: String? = null
)
