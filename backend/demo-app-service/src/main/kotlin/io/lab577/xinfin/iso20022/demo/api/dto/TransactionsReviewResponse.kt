package io.lab577.xinfin.iso20022.demo.api.dto

import java.util.*

data class TransactionsReviewResponse(
    val reviewedTransactions: Set<UUID>,
    val failedTransactions: Set<UUID>
)
