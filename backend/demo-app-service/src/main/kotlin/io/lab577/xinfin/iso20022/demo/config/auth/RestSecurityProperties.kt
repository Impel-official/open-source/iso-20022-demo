package io.lab577.xinfin.iso20022.demo.config.auth

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.web.cors.CorsConfiguration

@ConstructorBinding
@ConfigurationProperties(prefix = "rest.security")
data class RestSecurityProperties (
  val enabled: Boolean,
  val apiMatcher: String,
  val l2Matcher: String,
  val cors: Cors,
  val issuerUri: String,
){
  fun getCorsConfiguration(): CorsConfiguration {
    val corsConfiguration = CorsConfiguration()
    with(cors){
      allowedOrigins?.also{ corsConfiguration.allowedOrigins = allowedOrigins }
      allowedOrigins?.also{ corsConfiguration.allowedMethods = allowedMethods }
      allowedOrigins?.also{ corsConfiguration.allowedHeaders = allowedHeaders }
      allowedOrigins?.also{ corsConfiguration.exposedHeaders = exposedHeaders }
      allowedOrigins?.also{ corsConfiguration.allowCredentials = allowCredentials }
      allowedOrigins?.also{ corsConfiguration.maxAge = maxAge }
    }
    return corsConfiguration
  }
}

data class Cors(
  val allowedOrigins: List<String>? = null,
  val allowedMethods: List<String>? = null,
  val allowedHeaders: List<String>? = null,
  val exposedHeaders: List<String>? = null,
  val allowCredentials: Boolean? = null,
  val maxAge: Long? = null,
)
