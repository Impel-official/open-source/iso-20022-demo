package io.lab577.xinfin.iso20022.demo

import io.lab577.xinfin.iso20022.demo.config.auth.PublicKeyFormatInitializer
import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntity
import io.lab577.xinfin.iso20022.demo.domain.entity.GatewayEventEntityRepository
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.domain.repository.TransactionEntityRepository
import io.lab577.xinfin.iso20022.demo.integration.hubspot.HubSpotClient
import io.lab577.xinfin.iso20022.demo.integration.hubspot.config.HubSpotProperties
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022Client
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022EventClient
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022IdentityClient
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022MetricsClient
import io.lab577.xinfin.iso20022.demo.integration.kucoin.KuCoinClient
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.retry.annotation.EnableRetry
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication(
    scanBasePackageClasses = [
        Application::class,
    ],
    exclude = [
        SecurityAutoConfiguration::class,
        UserDetailsServiceAutoConfiguration::class
    ]
)
@ConfigurationPropertiesScan(
    basePackageClasses = [
        Application::class,
        HubSpotProperties::class,
    ]
)
@EnableFeignClients(
    clients = [
        Iso20022Client::class,
        Iso20022EventClient::class,
        Iso20022IdentityClient::class,
        Iso20022MetricsClient::class,
        KuCoinClient::class,
        HubSpotClient::class
    ]
)
@EntityScan(
    basePackageClasses = [
        GatewayEventEntity::class,
        TransactionEntity::class,
    ]
)
@EnableJpaRepositories(
    basePackageClasses = [
        GatewayEventEntityRepository::class,
        TransactionEntityRepository::class,
    ]
)
@EnableCaching
@EnableRetry
@EnableAsync(proxyTargetClass = true)
@EnableScheduling
class Application

fun main(args: Array<String>) {
    val application = SpringApplication(Application::class.java)
    application.addInitializers(PublicKeyFormatInitializer())
    application.run(*args)
}
