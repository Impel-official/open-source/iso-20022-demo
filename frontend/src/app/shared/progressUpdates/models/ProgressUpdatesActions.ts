import { ProgressUpdateState } from "./ProgressUpdates";
import { ProgressUpdate } from "./ProgressUpdate";

type ProgressUpdatesAddAction = {
  name: "ADD",
  progressUpdate: ProgressUpdate
};
type ProgressUpdatesChangeAction = {
  name: "CHANGE",
  progressUpdate: ProgressUpdate
};
type ProgressUpdatesStateChangeAction = {
  name: "STATE",
  state: ProgressUpdateState
};
type ProgressUpdatesEmptyAction = {
  name: "EMPTY",
};

export type ProgressUpdateAction = 
  ProgressUpdatesAddAction |
  ProgressUpdatesChangeAction |
  ProgressUpdatesStateChangeAction |
  ProgressUpdatesEmptyAction;
export type ProgressUpdatesActions = Array<ProgressUpdateAction>