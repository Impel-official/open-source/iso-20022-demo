export class State {
  public constructor(
    public name: string,
  ) {}

  static fromJson(json: any): State {
    // TODO: this needs json schema validation
    return new State(json)
  }
}