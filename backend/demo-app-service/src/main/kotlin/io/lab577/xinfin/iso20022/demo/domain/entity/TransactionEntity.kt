package io.lab577.xinfin.iso20022.demo.domain.entity

import com.fasterxml.jackson.annotation.JsonManagedReference
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionDto
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionMessageCollateralDto
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionProofStatus
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage
import io.lab577.xinfin.iso20022.demo.api.dto.TransactionSummary
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "transactions")
class TransactionEntity(
    id: UUID? = null,
    @Column(name = "end2end_id", nullable = false)
    var endToEndId: String,
    @Column(name = "sender_bic", nullable = false)
    var senderBic: String,
    @Column(name = "recipient_bic", nullable = false)
    var recipientBic: String,
    @Column(name = "sender_account", nullable = false)
    var senderAccount: String,
    @Column(name = "sender_xdc_address", nullable = false)
    var senderXdcAddress: String,
    @Column(name = "recipient_account", nullable = false)
    var recipientAccount: String,
    @Column(name = "counter_party_bic", nullable = false)
    var counterPartyBic: String,
    @Column(name = "tx_amount", nullable = false, precision = 50, scale = 25)
    var amount: BigDecimal,
    @Column(nullable = false)
    var currency: String,
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    val direction: TransactionMessageDirection,
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    var status: TransactionStatus,
    @Column(name = "valid_until", nullable = false)
    val validUntil: OffsetDateTime,
    @Column(name = "request_date", nullable = false)
    val requestDate: OffsetDateTime,
    @Schema(
        description = "Additional free-text info about this TX message",
        example = "Proforma Invoice 1234",
        required = false
    )
    val referenceInfo: String?,
    @Column(name = "native_collateral_amount", precision = 50, scale = 25, nullable = true, updatable = false)
    var nativeCollateralAmount: BigDecimal? = null,
    @Column(name = "exchange_price", precision = 50, scale = 25, nullable = true, updatable = false)
    var exchangePrice: BigDecimal? = null,
    @Column(name = "exchange_currency", nullable = true, updatable = false)
    var exchangeCurrency: String? = null,
    @Schema(description = "The user that created the TX", example = "user1", required = true)
    @Column(name = "initiator", nullable = false, updatable = false)
    val initiator: String,// = null,
    @Schema(description = "The user that accepted or rejected the TX", example = "user2")
    @Column(name = "responder")
    var responder: String?,// = null,
    @Schema(
        description = "The rollup TX hash",
        example = "0x59f0cf5c25d6e5cc8bffe9127bbcc2cdbf1b9257a59d189cc21777057429f6XX"
    )
    @Column(name = "rollup_tx_hash")
    var rollupTransactionHash: String? = null,
    @Column(name = "proof_status", nullable = false)
    @Enumerated(EnumType.STRING)
    var proofStatus: TransactionProofStatus = TransactionProofStatus.None,
    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL], mappedBy = "transaction")
    val messages: MutableList<TransactionMessageEntity>
) : AbstractAuditableEntity(id) {

    fun toDto(
        messageDtos: List<TransactionMessage> = messages.map { it.toDto() }
    ) = TransactionDto(
        id = id,
        version = version,
        senderBic = senderBic,
        recipientBic = recipientBic,
        senderAccount = senderAccount,
        senderXdcAddress = senderXdcAddress,
        recipientAccount = recipientAccount,
        counterPartyBic = counterPartyBic,
        amount = amount,
        currency = currency,
        direction = direction,
        status = status,
        endToEndId = endToEndId,
        requestDate = requestDate,
        validUntil = validUntil,
        referenceInfo = referenceInfo,
        collateral = when {
            messages.isEmpty() -> null
            messages[0].collateral == null -> null
            else -> {
                val pacs009Collateral = messages[0].collateral!!
                TransactionMessageCollateralDto(
                    type = pacs009Collateral.type,
                    amount = pacs009Collateral.amount,
                    exchangePrice = pacs009Collateral.exchangePrice,
                    exchangeCurrency = pacs009Collateral.exchangeCurrency,
                    tokenSymbol = pacs009Collateral.tokenSymbol,
                )
            }
        },
        initiator = initiator,
        responder = responder,
        rollupTransactionHash = rollupTransactionHash,
        messages = messageDtos,
    )

    fun toSummaryDto() = TransactionSummary(
        id = id,
        version = version,
        senderBic = senderBic,
        recipientBic = recipientBic,
        counterPartyBic = counterPartyBic,
        senderAccount = senderAccount,
        senderXdcAddress = senderXdcAddress,
        recipientAccount = recipientAccount,
        amount = amount,
        currency = currency,
        direction = direction,
        status = status,
        endToEndId = endToEndId,
        requestDate = requestDate,
        validUntil = validUntil,
        referenceInfo = referenceInfo,
        collateral = when {
            messages.isEmpty() -> null
            messages[0].collateral == null -> null
            else -> {
                val pacs009Collateral = messages[0].collateral!!
                TransactionMessageCollateralDto(
                    type = pacs009Collateral.type,
                    amount = pacs009Collateral.amount,
                    exchangePrice = pacs009Collateral.exchangePrice,
                    exchangeCurrency = pacs009Collateral.exchangeCurrency,
                    tokenSymbol = pacs009Collateral.tokenSymbol,
                )
            }
        },
        initiator = initiator,
        responder = responder,
        rollupTransactionHash = rollupTransactionHash,
    )
}
