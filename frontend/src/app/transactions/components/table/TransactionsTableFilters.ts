import {customElement, property, query, queryAll, state} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {
  TransactionsTableFilter,
  TransactionsTableFilterFieldType,
  TransactionsTableFilterStatus
} from '../../model/TransactionsTableFilter';
import {Currency} from '../../model/Currency';
import {State} from '../../model/State';
import {Party} from '../../model/Party';
import {cardCss, commonCss, formCss} from "../../../shared/config/styles";
import {PropertyValues} from "@lit/reactive-element/development/reactive-element";
import {styleHelper} from "../../../shared/config/StyleHelper";
import {enumFromStringValue} from "../../../shared/utils";

const styles = css`
${commonCss}
${formCss}
${cardCss}

ui5-date-picker, ui5-select, ui5-input{
  height: 40.5px;
  width: 100%;
}
.transactions {
  margin-top: 1em;
}
div.card-content{
  padding: 1em;
  display: flex;
  flex-direction: column;
}
.filter-row {
  gap: 1rem;
  justify-content: space-between;
  overflow-x: unset;
}
.filter{
  display: flex;
  flex-direction: column;
  max-width: 250px;
  justify-content: end;
  flex-grow: 1;
}
#filter-field-value {
  max-width: 100%;
  margin: 0 1.5rem;
}
.filter-button{
  display: flex;
  flex-direction: column;
  justify-content: end;
  flex-grow: 1;
  flex-shrink: 0;
  max-width: 200px;
}
div.header-content {
  padding-left: 1em;
  display: flex;
  flex-direction: row;
}
.filter-column {
  display: flex;
  flex-direction: column;
  justify-content: end;
}
.filter-header {
  margin-bottom: -1rem;
}
ui5-card-header.secondary::part(title) {
  margin-bottom: 1rem;
}
#filter-update-results-mobile-button {
  display: none;
}

/* Responsive layout - makes a one column layout instead of a two-column layout */
@media (max-width: 800px) {
 .filter-row {
    flex-direction: column;
    gap: 0rem;
  }
  .filter {
    max-width: 100%;
  }
  #filter-field-value {
    margin: 0;
  }
  .filter-button {
    max-width: 100%;
    gap: 1rem;
  }
  div.card-content{
    padding: 0rem;
  }
  #filter-update-results-desktop-button {
    display: none;
  }
  #filter-update-results-mobile-button {
    display: unset;
    margin-top: 2rem;
  }
}
`

export type TransactionsTableFilterSelection = { filter: TransactionsTableFilter, isSelected: boolean }
export type TransactionsTableFilterEvent = CustomEvent<TransactionsTableFilterSelection>;

@customElement('transactions-table-filters')
export class TransactionsTableFilters extends LitElement {

  @property()
  currencies: Array<Currency> = [];
  @property()
  states: Array<State> = [];
  @property()
  bics: Array<Party> = [];
  @property()
  transactionsTableFilter = TransactionsTableFilter.empty();
  @state()
  nonEmptyFields = false;

  // Currently (v1.0.1) there are no type declarations for the ui5 elements so `any` is used
  // https://github.com/SAP/ui5-webcomponents/issues/4337
  @query('#dateFrom')
  private dateFromInput: any;
  @query('#dateTo')
  private dateToInput: any;
  @query('#status')
  private statusInput: any;
  @query('#fieldType')
  private fieldTypeInput: any;
  @query('#fieldValue')
  private fieldValueInput: any;
  @queryAll('ui5-date-picker, ui5-input, ui5-step-input, ui5-daterange-picker, ui5-select, ui5-textarea, input-suggestion')
  formFields: HTMLCollection | undefined;

  protected override update(changedProperties: PropertyValues) {
    super.update(changedProperties);
    if (this.formFields?.length) {
      styleHelper.maybeStyleFormFields(this.formFields);
    }
  }

  override connectedCallback() {
    super.connectedCallback();
    document.addEventListener("keydown", this.submitOnEnter.bind(this));
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener("keydown", this.submitOnEnter.bind(this));
  }

  async submitOnEnter(event: any) {
    if (event.code === "Enter" || event.code === "NumpadEnter") {
      event.preventDefault();
      await this.filterTable();
    }
  }

  static override styles = styles;

  openDatepicker(event: any) {
    event.preventDefault()
    event.currentTarget.openPicker();
  }

  async updateFilters() {
    // Currently (v1.0.1) there is a bug in DateRange picker,
    // that emits onChange event before updating the DOM
    await new Promise(resolve => setTimeout(resolve, 0));
    const startDate = this.dateFromInput.value ?? '';
    const endDate = this.dateToInput.value ?? '';
    const status = enumFromStringValue(TransactionsTableFilterStatus, this.statusInput.selectedOption?.value || '');
    const fieldType = this.fieldTypeInput.selectedOption?.value || '';
    const fieldValue = this.fieldValueInput.value.trim();
    this.nonEmptyFields = !!(startDate || endDate || status || (fieldType && fieldValue));
    const transactionsTableFilter = new TransactionsTableFilter(
        startDate,
        endDate,
        fieldType,
        fieldValue,
        status
    );
    this.emitFilterEvent({filter: transactionsTableFilter, isSelected: false});
    return transactionsTableFilter;
  }

  async filterTable() {
    const transactionsTableFilter = await this.updateFilters();
    this.emitFilterEvent({filter: transactionsTableFilter, isSelected: true});
  }

  async clearFilters() {
    this.dateFromInput.value = '';
    this.fieldValueInput.value = '';
    this.fieldTypeInput._select(0);
    this.statusInput._select(0);

    this.emitFilterEvent({filter: TransactionsTableFilter.empty(), isSelected: true});
  }

  private emitFilterEvent(data: TransactionsTableFilterSelection) {
    const filterEvent = new CustomEvent<TransactionsTableFilterSelection>(
        'filter', {
          detail: data,
          bubbles: true,
          composed: true
        }
    );
    this.dispatchEvent(filterEvent);
  }

  override render() {
    const filter = this.transactionsTableFilter;
    return html`
        <ui5-card>
            <div class="card-content">
                <ui5-card-header class="secondary filter-header" slot="header" title-text="Filter Transactions">Filter
                    Transactions
                </ui5-card-header>
                <div class="row filter-row">
                    <div class="filter">
                        <div class="label"></div>
                        <ui5-date-picker id="dateFrom"
                                         placeholder="Start Date"
                                         @click=${this.openDatepicker}
                                         @change=${this.updateFilters}
                                         .value=${filter.startDate}></ui5-date-picker>
                    </div>
                    <div class="filter">
                        <div class="label"></div>
                        <ui5-date-picker id="dateTo"
                                         placeholder="End Date"
                                         @click=${this.openDatepicker}
                                         @change=${this.updateFilters}
                                         .value=${filter.endDate}></ui5-date-picker>
                    </div>
                    <div class="filter">
                        <div class="label"></div>
                        <ui5-select id="status"
                                    @change=${this.updateFilters}>
                            <ui5-option>Status - Any</ui5-option>
                            ${statusLabelMap.map(item => html`
                                <ui5-option value="${item.key}" .selected=${filter.status == item.key}>
                                    ${item.label}
                                </ui5-option>`)}
                        </ui5-select>
                    </div>
                    <div class="filter-button">
                        <ui5-button id="filter-update-results-desktop-button" design="Emphasized"
                                    @click=${this.filterTable}>Update Results
                        </ui5-button>
                    </div>
                </div>
                <div class="row filter-row">
                    <div class="filter">
                        <div class="label"></div>
                        <ui5-select id="fieldType"
                                    @change=${this.updateFilters}>
                            <ui5-option>Filter by</ui5-option>
                            ${fieldTypeLabelMap.map(item => html`
                                <ui5-option value="${item.key}" .selected=${filter.fieldType == item.key}>
                                    ${item.label}
                                </ui5-option>`)}
                        </ui5-select>
                    </div>
                    <div id="filter-field-value" class="filter">
                        <div class="label"></div>
                        <ui5-input id="fieldValue"
                                   placeholder="Filter value"
                                   @change=${this.updateFilters}
                                   .value=${filter.fieldValue}
                                   show-clear-icon></ui5-input>
                    </div>
                    <div class="filter-button">
                        <ui5-button id="filter-update-results-mobile-button" design="Emphasized"
                                    @click=${this.filterTable}>Update Results
                        </ui5-button>
                        <ui5-button design="Default" @click=${this.clearFilters}>Clear Filters</ui5-button>
                    </div>
                </div>
            </div>
            </div>
        </ui5-card>
    `
  }
}

const statusLabelMap: { key: TransactionsTableFilterStatus, label: string }[] = [
  {key: TransactionsTableFilterStatus.expired, label: 'Expired'},
  {key: TransactionsTableFilterStatus.inbox_received, label: 'Received - Waiting Approval'},
  {key: TransactionsTableFilterStatus.inbox_accepted, label: 'Received - Approved'},
  {key: TransactionsTableFilterStatus.inbox_rejected, label: 'Received - Rejected'},
  {key: TransactionsTableFilterStatus.outbox_received, label: 'Delivered - Waiting Approval'},
  {key: TransactionsTableFilterStatus.outbox_approved, label: 'Delivered - Approved'},
  {key: TransactionsTableFilterStatus.outbox_rejected, label: 'Delivered - Rejected'},
  {key: TransactionsTableFilterStatus.outbox_failed, label: 'Attempted Delivery - Failed'},
];

const fieldTypeLabelMap: { key: TransactionsTableFilterFieldType, label: string }[] = [
  {key: TransactionsTableFilterFieldType.endToEndId, label: 'Transaction ID'},
  {key: TransactionsTableFilterFieldType.counterPartyBic, label: 'BIC'},
  {key: TransactionsTableFilterFieldType.recipientAccount, label: 'To IBAN'},
  {key: TransactionsTableFilterFieldType.senderAccount, label: 'From IBAN'},
  {key: TransactionsTableFilterFieldType.amount, label: 'Amount'},
];

