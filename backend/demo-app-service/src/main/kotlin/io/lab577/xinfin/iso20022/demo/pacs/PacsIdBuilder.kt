package io.lab577.xinfin.iso20022.demo.pacs

import java.util.*

/** Message ID that conforms to ISO XML schemata */
object PacsIdBuilder {
    fun get(id: UUID): String = id.toString().replaceFirst("-", "")

    fun randomId(): String = get(UUID.randomUUID())
}
