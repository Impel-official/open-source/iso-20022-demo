import {MessageCollateral} from "./api";
import _ from "lodash";

export const enumFromStringValue = <T>(enm: { [s: string]: T }, value: string): T | undefined =>
    (Object.values(enm) as unknown as string[]).includes(value)
        ? value as unknown as T
        : undefined;

export const formatNumber = (value?: number, defaultValue: number = Number.NaN, fractionDigits = 2): string => {
  const valueToFormat = value == null ? defaultValue : value;
  if (valueToFormat == null || isNaN(valueToFormat)) {
    return value?.toString() || '';
  }
  return valueToFormat.toLocaleString(undefined, {
    minimumFractionDigits: fractionDigits,
    maximumFractionDigits: fractionDigits
  });
}

export const truncate = (value?: string, prefixSize: number = 5, suffixSize: number = 5, truncateText = '***'): (string | undefined) => {
  if (value == null || value.length < (prefixSize + suffixSize)) {
    return value;
  }

  const startText = value.slice(0, prefixSize);
  const endText = value.slice(value.length - suffixSize);
  return `${startText}${truncateText}${endText}`
}

// TODO: #359 Remove currencySymbol form method parameters
export const getCollateralText = (collateral?: MessageCollateral, currencySymbol?: string) => {
  let collateralText: string;
  if (collateral) {
    const collateralAmount = collateral.amount;
    const collateralToken = collateral.tokenSymbol || 'XDC';
    const collateralExchangePrice = collateral.exchangePrice;
    const collateralExchangeCurrency = collateral.exchangeCurrency;
    collateralText = `${collateralAmount} ${collateralToken} @ ${currencySymbol}${collateralExchangePrice} ${collateralExchangeCurrency}`;
  } else {
    collateralText = "None";
  }
  return collateralText
}

export const round = (value: number | string | undefined, fractionDigits = 18): number => {
  if (value == null) {
    return 0
  }
  const valueAsNumber = _.toNumber(value)
  return _.round(valueAsNumber, fractionDigits)
}
