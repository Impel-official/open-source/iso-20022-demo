import {routingService} from './app/shared/routing/RoutingService';
import {authService} from "./app/shared/auth/AuthService";
import {notificationsStore} from "./app/shared/notifications/NotificationsStore";
import {referenceDataStore} from './app/transactions/ReferenceDataStore';
import {stompAdapter} from './app/shared/stomp/StompAdapter';
import {transactionsStore} from './app/transactions/TransactionsStore';
// import all lit elements here
import './App'
import './app/shared/components/NotFoundView';
import './app/shared/components/UnauthorizedView';
import './app/shared/components/NavBar';
import './app/shared/components/FooterBar';
import './app/shared/components/InputSuggestion';
import './app/shared/components/DateCountdown';
import './app/shared/components/Countdown';
import './app/shared/components/ShareDemoExperiencePanel';
import './app/shared/components/AppToast';
import './app/shared/components/CharactersRemainingElement';
import './app/shared/notifications/NotificationIconElement';
import './app/shared/messages/MessagesElement';
import './app/shared/progressUpdates/ProgressUpdatesElement';
import './app/shared/user/UserAvatarElement';
import './app/dashboard/DashboardView';
import './app/settings/SettingsView';
import './app/tranfer-request/TransferRequestView';
import './app/tranfer-request/TransferRequestConfirmationDialog';
import './app/transactions/components/TransactionsView';
import './app/transactions/components/table/TransactionsTable';
import './app/transactions/components/table/TransactionsTableFilters';
import './app/transactions/components/UnapprovedView';
import './app/transactions/components/details/TransactionDetails';
import './app/transactions/components/details/UnapprovedTransactionConfirmationButtons';
import './app/transactions/components/TransactionDetailsView';
import './app/transactions/components/table/TablePaging';
import './app/transactions/components/table/TxDetailsLink';

// import all UI5 here
import "@ui5/webcomponents/dist/Assets.js"
import "@ui5/webcomponents-localization/dist/Assets.js"
import "@ui5/webcomponents-icons/dist/AllIcons";
import "@ui5/webcomponents-icons-business-suite/dist/AllIcons";
import "@ui5/webcomponents/dist/Link";
import "@ui5/webcomponents/dist/Card";
import "@ui5/webcomponents/dist/CardHeader";
import "@ui5/webcomponents/dist/Table";
import "@ui5/webcomponents/dist/TableColumn";
import "@ui5/webcomponents/dist/TableRow";
import "@ui5/webcomponents/dist/TableCell";
import "@ui5/webcomponents/dist/Label";
import "@ui5/webcomponents/dist/Input";
import "@ui5/webcomponents/dist/TabContainer";
import "@ui5/webcomponents/dist/Tab";
import "@ui5/webcomponents/dist/TabSeparator";
import "@ui5/webcomponents/dist/TextArea";
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/SplitButton";
import "@ui5/webcomponents/dist/Switch";
import "@ui5/webcomponents/dist/SegmentedButton";
import "@ui5/webcomponents/dist/SegmentedButtonItem";
import "@ui5/webcomponents/dist/ToggleButton";
import "@ui5/webcomponents/dist/DateRangePicker";
import "@ui5/webcomponents/dist/Select";
import "@ui5/webcomponents/dist/StepInput";
import "@ui5/webcomponents/dist/MessageStrip";
import "@ui5/webcomponents/dist/Dialog";
import "@ui5/webcomponents/dist/Toast";
import "@ui5/webcomponents/dist/Title";
import "@ui5/webcomponents/dist/Panel";
import "@ui5/webcomponents/dist/BusyIndicator";
import "@ui5/webcomponents/dist/Breadcrumbs";
import "@ui5/webcomponents/dist/BreadcrumbsItem";
import "@ui5/webcomponents/dist/Avatar";
import "@ui5/webcomponents/dist/features/InputSuggestions";
import "@ui5/webcomponents-fiori/dist/Bar.js"
import "@ui5/webcomponents-fiori/dist/NotificationListItem";
import {config} from "./app/shared/config";
import {hubSpotAnalyticsService} from "./app/shared/HubSpotAnalyticsService";
// @ts-ignore
import { setLanguage } from "@ui5/webcomponents-base/dist/config/Language.js";

setLanguage("en");

(async () => {
  try {
    if(config.auth.type !== "NONE") {
      await authService.init();
    }
    await routingService.init();
    await stompAdapter.start();
    await referenceDataStore.init();
    await transactionsStore.init();
    await notificationsStore.init();
    if(config.hubSpotAnalyticsEnabled) {
      await hubSpotAnalyticsService.init();
    }
    const favicon = document.getElementById("favicon");
    if(favicon) {
      favicon.setAttribute("href", config.branding.icoUrl);
    }
  } catch (e) {
    console.error('Could not initialise services: ', e);
    throw e
  }
})();

