package io.lab577.xinfin.iso20022.demo.integration.kucoin

import io.lab577.xinfin.iso20022.demo.integration.kucoin.model.Price
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "kucoin", url = "\${app.integration.kucoin.url}")
interface KuCoinClient {

    @GetMapping("prices")
    fun getPrices(
        @RequestParam(required = true)
        base: String,
        @RequestParam(required = false, defaultValue = "XDC")
        currencies: Set<String>,
    ): Price
}
