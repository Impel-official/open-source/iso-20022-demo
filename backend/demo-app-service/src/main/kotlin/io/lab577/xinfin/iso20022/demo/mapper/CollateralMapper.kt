package io.lab577.xinfin.iso20022.demo.mapper

import io.lab577.xinfin.iso20022.demo.api.dto.TransactionMessageCollateralDto
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageInfoCollateral

object CollateralMapper {

    fun mapCollateralToMessageCollateral(collateral: MessageInfoCollateral?): TransactionMessageCollateralDto? {
        return if (collateral == null) null else TransactionMessageCollateralDto(
            type = when (collateral.type!!) {
                MessageInfoCollateral.TypeEnum.NATIVE -> TransactionMessageCollateralType.NATIVE
                MessageInfoCollateral.TypeEnum.ERC20 -> TransactionMessageCollateralType.ERC20
            },
            amount = collateral.amount,
            exchangePrice = collateral.exchangePrice,
            exchangeCurrency = collateral.exchangeCurrency,
            tokenSymbol = collateral.tokenSymbol,
        )
    }
}

