package io.lab577.xinfin.iso20022.demo.domain.model

enum class TransactionMessageDirection {
    INBOX, OUTBOX
}
