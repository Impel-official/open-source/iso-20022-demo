package io.lab577.xinfin.iso20022.demo.domain.repository

import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageEntity
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TransactionMessageEntityRepository : BaseRepository<TransactionMessageEntity, UUID> {

    @Query(
        "SELECT new io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageProjection(tm) " +
            "FROM TransactionMessageEntity tm " +
            "WHERE tm.transaction.id = :transactionId"
    )
    fun findAllSummariesByTransactionId(@Param("transactionId") transactionId: UUID): List<TransactionMessage>

    @Query(
        "SELECT tm.content " +
            "FROM TransactionMessageEntity tm " +
            "WHERE tm.id = :transactionId"
    )
    fun findRawContentById(@Param("transactionId") transactionId: UUID): String?

}
