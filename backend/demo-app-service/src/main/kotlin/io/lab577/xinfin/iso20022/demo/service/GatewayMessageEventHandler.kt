package io.lab577.xinfin.iso20022.demo.service

import com.prowidesoftware.swift.model.mx.MxPacs00200112
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType.Companion.SUPPORTED_ISO20022_MESSAGE_TYPES
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageAcknowledgedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageCreatedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageNotAcknowledgedEvent
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageReceivedEvent
import io.lab577.xinfin.iso20022.demo.utils.IsoXml
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class GatewayMessageEventHandler(
    private val transactionMessageService: TransactionMessageService,
    private val transactionService: TransactionService,
    private val iso20022ClientService: Iso20022ClientService,
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(GatewayMessageEventHandler::class.java)
    }

    @Transactional(readOnly = false)
    fun handleMessageCreatedEvent(event: MessageCreatedEvent) {
        logger.info("Handling ${MessageCreatedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndId: ${event.endToEndId}, payloadType: ${event.payloadType}}")
        if (event.payloadType != MessageCreatedEvent.PayloadTypeEnum.ISO20022) {
            logger.warn("Ignoring unsupported message type ${event.payloadType}")
            return
        }

        val message = iso20022ClientService.getMessage(event.traceId, true)
        val mx = IsoXml.xmlToMx(message.payload)
        val mxType = mx.mxId.id()

        if (!SUPPORTED_ISO20022_MESSAGE_TYPES.contains(mxType)) {
            logger.warn("Ignoring unsupported ISO message type $mxType")
            return
        }

        transactionMessageService.save(
            message = message,
            mx = mx,
            assignedId = event.traceId,
            direction = TransactionMessageDirection.OUTBOX
        )
    }

    @Transactional(readOnly = false)
    fun handleMessageReceivedEvent(event: MessageReceivedEvent) {
        logger.info("Handling ${MessageReceivedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndId: ${event.endToEndId}, payloadType: ${event.payloadType}}")
        if (event.payloadType != MessageReceivedEvent.PayloadTypeEnum.ISO20022) {
            logger.warn("Ignoring unsupported message type ${event.payloadType}")
            return
        }

        val message = iso20022ClientService.getMessage(event.traceId, true)
        val mx = IsoXml.xmlToMx(message.payload)
        val mxType = mx.mxId.id()

        if (!SUPPORTED_ISO20022_MESSAGE_TYPES.contains(mxType)) {
            logger.warn("Ignoring unsupported ISO message type $mxType")
            return
        }

        transactionMessageService.save(
            message = message,
            mx = mx,
            assignedId = event.traceId,
            direction = TransactionMessageDirection.INBOX
        )
    }

    @Transactional(readOnly = false)
    fun handleMessageAcknowledgedEvent(event: MessageAcknowledgedEvent) {
        logger.info("Handling ${MessageAcknowledgedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndId: ${event.endToEndId}}")
        transactionMessageService.findById(event.traceId)?.let {
            when (it.type) {
                PacsType.PACS_009 -> {
                    val transaction = it.transaction
                    transaction.status = TransactionStatus.Received
                    transactionService.save(transaction)
                }
                PacsType.PACS_002 -> {
                    val mx = IsoXml.xmlToMx(it.content) as MxPacs00200112
                    val isPaymentRejected = mx.fiToFIPmtStsRpt?.orgnlGrpInfAndSts?.firstOrNull()?.grpSts == "RJCT"
                    val transaction = it.transaction
                    transaction.status =
                        if (isPaymentRejected) TransactionStatus.Rejected else TransactionStatus.Approved
                    transactionService.save(transaction)
                }
                else -> null
            }
        }
    }

    @Transactional(readOnly = false)
    fun handleMessageNotAcknowledgedEvent(event: MessageNotAcknowledgedEvent) {
        logger.info("Handling ${MessageNotAcknowledgedEvent::class.simpleName} event {traceId: ${event.traceId}, endToEndId: ${event.endToEndId}}")
        transactionMessageService.findById(event.traceId)?.let {
            val transaction = it.transaction
            transaction.status = TransactionStatus.Rejected
            transactionService.save(transaction)
        }
    }
}
