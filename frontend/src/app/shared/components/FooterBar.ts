import {css, html, LitElement} from 'lit';
import {customElement} from 'lit/decorators.js';
import {commonCss} from "../config/styles";

const styles = css`
${commonCss}
#footer-container{
  position: absolute;
  bottom: 0px; 
  width: 100%;
  display: flex;
  flex-direction: row;
  height: 58px;
  max-width:1140px;
}
ui5-bar{
  margin-left: 8px;
  margin-right: 8px;
  border-top: 1px solid #005DF2;
}
`

@customElement('footer-bar')
export class FooterBar extends LitElement {
  static override styles = styles;

  constructor() {
    super();
  }


  override render() {
    return html`
        <div id="footer-container">
            <ui5-bar design="Footer">
                <p slot="startContent">Impel © 2022</p>
                <p slot="endContent">
                    <a class="link-plain" href="https://www.impel.global/privacy-policy" target="_blank">Privacy
                        Policy</a> |
                    <a class="link-plain" href="https://www.impel.global/terms-conditions" target="_blank">Terms of
                        Service</a>
                </p>
            </ui5-bar>
        </div>
    `;
  }
}


