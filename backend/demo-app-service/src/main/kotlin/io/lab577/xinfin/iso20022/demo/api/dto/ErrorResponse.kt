package io.lab577.xinfin.iso20022.demo.api.dto

data class FieldError(
    val message: String,
    val code: String,
    val property: String? = null,
    val rejectedValue: String? = null
)

data class ErrorResponse(
    val message: String,
    val code: String,
    val status: Int? = null,
    val fieldErrors: List<FieldError>? = null
)
