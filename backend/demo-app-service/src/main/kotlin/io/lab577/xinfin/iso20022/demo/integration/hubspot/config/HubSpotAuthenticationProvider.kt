package io.lab577.xinfin.iso20022.demo.integration.hubspot.config

import io.lab577.xinfin.iso20022.demo.integration.hubspot.HubSpotClientService
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(name = ["app.integration.hubspot.enabled"], havingValue = "true", matchIfMissing = false)
class HubSpotAuthenticationProvider(
    val hubSpotClientService: HubSpotClientService
) : AuthenticationProvider {

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        val password = authentication.credentials.toString()
        val hubSpotUser = hubSpotClientService.findContactByIdProperty(password)
            ?: throw UsernameNotFoundException("User does not exist")

        val authorities: MutableList<GrantedAuthority> = ArrayList()
        authorities.add(SimpleGrantedAuthority("ROLE_USER"))
        return UsernamePasswordAuthenticationToken(hubSpotUser.properties["email"], password, authorities)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}
