package io.lab577.xinfin.iso20022.demo.domain.repository

import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionProofStatus
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TransactionEntityRepository : BaseRepository<TransactionEntity, UUID> {

    fun findTopByEndToEndIdOrderByCreatedDesc(endToEndId: String): TransactionEntity

    @Query("SELECT t FROM TransactionEntity t WHERE t.endToEndId = :endToEndId")
    fun getByEndToEndId(endToEndId: String): TransactionEntity

    @Modifying
    @Query(
        nativeQuery = true, value = "" +
            "UPDATE transactions t " +
            "SET proof_status = :#{#proofStatus?.name()} " +
            "FROM transaction_messages tm " +
            "WHERE tm.id in (:messagesIds) " +
            "AND tm.transaction_id = t.id"
    )
    fun updateProofStatusByMessagesIds(@Param("proofStatus") proofStatus: TransactionProofStatus, messagesIds: List<UUID>): Int

    @Modifying
    @Query(
        nativeQuery = true, value = "" +
            "UPDATE transactions t " +
            "SET rollup_tx_hash = :rollupTransactionHash " +
            "FROM transaction_messages tm " +
            "WHERE tm.id in (:messagesIds) " +
            "AND tm.transaction_id = t.id"
    )
    fun updateRollupTransactionHashByMessagesIds(rollupTransactionHash: String, messagesIds: List<UUID>): Int

    @Query("SELECT t FROM TransactionEntity t WHERE t.status = :status and t.senderBic = :senderBic")
    fun getByStatusAndSenderBic(status: String, senderBic: String, pageable: Pageable): Page<TransactionEntity>

    @Query(
        "SELECT DISTINCT t.recipientBic FROM TransactionEntity t " +
            "WHERE t.status = :status and t.senderBic = :senderBic"
    )
    fun findRecipientBicsByStatusAndSenderBic(
        status: TransactionStatus,
        senderBic: String
    ): List<String>

    @Query(
        "SELECT DISTINCT t.recipientBic FROM TransactionEntity t " +
            "WHERE t.status in(:statuses) and t.senderBic = :senderBic and t.recipientBic <> :senderBic"
    )
    fun findRecipientBicsByStatusesAndSenderBic(
        statuses: Set<TransactionStatus>,
        senderBic: String
    ): List<String>

    @Query("SELECT t FROM TransactionEntity t WHERE t.status = :status and t.recipientBic = :recipientBic")
    fun findByStatusAndRecipientBic(
        status: TransactionStatus, recipientBic: String, pageable: Pageable
    ): Page<TransactionEntity>

    @Query("SELECT t FROM TransactionEntity t WHERE t.status = :status and t.recipientBic = :recipientBic")
    fun findAllByStatusAndRecipientBic(
        status: TransactionStatus, recipientBic: String
    ): List<TransactionEntity>

    @Query("SELECT t FROM TransactionEntity t WHERE t.status in(:statuses) and t.proofStatus = :proofStatus and t.recipientBic = :recipientBic")
    fun findAllByStatusesAndRecipientBic(
        statuses: Set<TransactionStatus>,
        proofStatus: TransactionProofStatus,
        recipientBic: String
    ): List<TransactionEntity>

    @Query("SELECT t FROM TransactionEntity t WHERE t.endToEndId in (:endToEndIds)")
    fun findAllByEndToEndIds(
        endToEndIds: List<String>,
    ): List<TransactionEntity>

    @Query(
        nativeQuery = true, value = "" +
            "SELECT DISTINCT t.* " +
            "FROM transaction_messages tm " +
            "LEFT JOIN transactions t ON t.id = tm.transaction_id " +
            "WHERE tm.id IN (:messagesIds)"
    )
    fun findAllByMessagesIds(messagesIds: List<UUID>): List<TransactionEntity>
}
