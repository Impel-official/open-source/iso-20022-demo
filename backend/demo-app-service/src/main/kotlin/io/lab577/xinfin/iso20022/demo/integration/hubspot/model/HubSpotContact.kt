package io.lab577.xinfin.iso20022.demo.integration.hubspot.model

data class HubSpotContact(
    val id: String,
    val properties: Map<String, String>,
    val archived: Boolean?
)
