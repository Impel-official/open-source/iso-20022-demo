import {Params, Router} from "@vaadin/router";
import _ from "lodash";
import {routeConfig, RouteName} from "./routeConfig";
import {AppConfig, config} from "../config";

declare global {
  interface WindowEventMap {
    'location-changed': CustomEvent<Router.Location>;
    'navigate-request': CustomEvent<NavigateRequestEvent>;
  }
}

export interface NavigateRequestEvent {
  name: RouteName,
  params?: Params | null
}

export type LocationChangeCallback = (location: Router.Location) => {};

export class RoutingService {
  private router?: Router;
  private listeners = new Map<unknown, LocationChangeCallback>();

  constructor(
      private config: AppConfig
  ) {
    window.addEventListener('vaadin-router-location-changed', (event) => {
      this.updatePageTitle(event.detail.location.route?.name);
      this.notifyListeners(event.detail.location);
      !window.dispatchEvent(new CustomEvent('location-changed', {detail: event.detail.location}));
    });
    window.addEventListener('navigate-request', (event: CustomEvent<NavigateRequestEvent>) => {
      this.handleNavigationRequestEvent(event.detail)
    });
  }

  async init() {
    const outlet = document.getElementById('outlet');
    this.router = new Router(outlet);
    await this.router.setRoutes(routeConfig);
  }

  getRouter(): Router {
    if (this.router == null) throw new Error('router not initialized');
    return this.router;
  }

  setQueryParamsToCurrentUrl(queryParams: URLSearchParams): string {
    const url = new URL(window.location.href);
    // Remove all query params
    [...url.searchParams.keys()].forEach( it => url.searchParams.delete(it));
    // Add new query params
    [...queryParams.entries()].forEach(([key, value]) => {
      if (value != null && value !== '') {
        url.searchParams.set(key, value);
      }
    })
    return url.toString();
  }

  urlForName(name: RouteName, params?: Params | null): string {
    return this.getRouter().urlForName(name, params);
  }

  navigateToRoute(routeName: RouteName, params?: Params | null) {
    const url = this.urlForName(routeName, params);
    this.navigate(url);
  }

  navigate(url: string) {
    Router.go(url);
  }

  getUrlSearchParams() {
    const url: URL = new URL(window.location.href);
    return url.searchParams;
  }

  addLocationChangesListener(id: unknown, callback: LocationChangeCallback) {
    this.listeners.set(id, callback);
  }

  removeLocationChangesListener(id: unknown) {
    if (this.listeners.has(id)) this.listeners.delete(id);
  }

  private notifyListeners(location: Router.Location) {
    for (const callback of this.listeners.values()) {
      callback(location);
    }
  }

  private handleNavigationRequestEvent(event: NavigateRequestEvent) {
    const url = this.urlForName(event.name, event.params);
    this.navigate(url);
  }

  private updatePageTitle(routeName?: string) {
    document.title = `Impel ${_.startCase(this.config.branding.orgName)} - ${_.startCase(routeName)}`;
  }
}

export const routingService = new RoutingService(config);
