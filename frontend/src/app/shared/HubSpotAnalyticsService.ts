import {Router} from "@vaadin/router";
import {User} from "./user/User";

declare global {
  interface Window {
    _hsq: HubSpotTrackingCode;
  }
}

type HubSpotTrackingCode = {
  push(data: any[]): void
}

export class HubSpotAnalyticsService {

  constructor() {
    window.addEventListener('location-changed', (event: CustomEvent<Router.Location>) => {
      this.setPath(event.detail.route?.name || '')
    });
    window.addEventListener('user-authenticated', (event: CustomEvent<User>) => {
      this.identify(event.detail.email)
    });
  }

  init() {

  }

  setPath(path: string): void {
    const hsq = this.getHsq();
    hsq.push(['setPath', path])
    hsq.push(['trackPageView'])
  }

  identify(email?: string): void {
    const hsq = this.getHsq();
    hsq.push(['identify', {email: email}])
    hsq.push(['trackPageView'])
  }

  getHsq(): HubSpotTrackingCode | any[] {
    return window._hsq = window._hsq || []
  }
}

export const hubSpotAnalyticsService = new HubSpotAnalyticsService();
