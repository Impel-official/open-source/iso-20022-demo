export type ProgressUpdateType = 'Success' | 'Skipped' | 'Waiting' | 'Error';
import { v4 as uuidv4 } from 'uuid';

export class ProgressUpdate {
  constructor(
    public readonly id: string,
    public readonly type: ProgressUpdateType,
    public readonly description: string,
  ) {}
  
  static new(type: ProgressUpdateType, description: string): ProgressUpdate {
    return new ProgressUpdate(uuidv4(), type, description);
  }
  
  equals(progressUpdate: ProgressUpdate): boolean {
    return this.id === progressUpdate.id
  }
}