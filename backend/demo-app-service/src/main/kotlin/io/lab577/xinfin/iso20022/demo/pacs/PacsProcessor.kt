package io.lab577.xinfin.iso20022.demo.pacs

import com.prowidesoftware.swift.model.mx.AbstractMX
import com.prowidesoftware.swift.model.mx.MxPacs00200112
import com.prowidesoftware.swift.model.mx.MxPacs00900110
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageInfo

abstract class PacsProcessor<T> {

    open fun process(message: MessageInfo, mx: AbstractMX): T {
        return when (mx) {
            is MxPacs00200112 -> process(message, mx)
            is MxPacs00900110 -> process(message, mx)
            else -> throw IllegalArgumentException("Unsupported MX message type: ${mx.mxId.id()}")
        }
    }

    abstract fun process(message: MessageInfo, mx: MxPacs00200112): T

    abstract fun process(message: MessageInfo, mx: MxPacs00900110): T
}
