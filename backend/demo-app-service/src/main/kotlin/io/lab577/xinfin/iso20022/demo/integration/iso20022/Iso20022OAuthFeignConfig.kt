package io.lab577.xinfin.iso20022.demo.integration.iso20022

import feign.RequestInterceptor
import feign.RequestTemplate
import io.lab577.xinfin.iso20022.demo.integration.config.OAuthClientCredentialsFeignManager
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpHeaders
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository


class Iso20022OAuthFeignConfig(
    private val oAuth2AuthorizedClientService: OAuth2AuthorizedClientService,
    private val clientRegistrationRepository: ClientRegistrationRepository
) {

    companion object {
        const val CLIENT_REGISTRATION_ID = "iso20022"
    }

    @Bean
    fun iso20022RequestInterceptor(authorizedClientManager: OAuth2AuthorizedClientManager): RequestInterceptor {
        val clientRegistration = clientRegistrationRepository.findByRegistrationId(CLIENT_REGISTRATION_ID)
        val clientCredentialsFeignManager = OAuthClientCredentialsFeignManager(
            authorizedClientManager,
            clientRegistration
        )
        return RequestInterceptor { requestTemplate: RequestTemplate ->
            requestTemplate.header(
                HttpHeaders.AUTHORIZATION,
                "Bearer " + clientCredentialsFeignManager.getAccessToken()
            )
        }
    }

    @Bean
    fun authorizedClientManager(): OAuth2AuthorizedClientManager {
        val authorizedClientProvider = OAuth2AuthorizedClientProviderBuilder.builder()
            .clientCredentials()
            .build()
        val authorizedClientManager = AuthorizedClientServiceOAuth2AuthorizedClientManager(
            clientRegistrationRepository,
            oAuth2AuthorizedClientService
        )
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider)
        return authorizedClientManager
    }
}
