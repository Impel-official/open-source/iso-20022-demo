import {MultipleHostsAbstractStore} from '../MultipleHostsAbstractStore';
import {AppConfig, config} from '../config';
import {Messages} from './models/Messages';
import {MessageType} from './models/Message';


export class MessagesStore extends MultipleHostsAbstractStore {
  messages: Messages;
  
  constructor(config: AppConfig, initialMessages = Messages.empty()) {
    super(config);
    this.messages = initialMessages;
  }
  
  showMessage(type: MessageType, description: string){
    this.messages = this.messages.addMessage(type, description);
    this.updatePropertiesToBoundHosts();
  }
  
  showUnexpectedErrorMessage() {
    this.showMessage(
      'Negative',
      'An unexpected error occurred. Try again or raise a ticket.'
    )
  }
  
  removeMessage(messageId: string) {
    this.messages = this.messages.removeMessage(messageId);
    this.updatePropertiesToBoundHosts();
  }
  
}

export const messagesStore = new MessagesStore(config);