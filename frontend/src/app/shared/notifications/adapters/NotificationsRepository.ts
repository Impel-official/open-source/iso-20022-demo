import { Notifications } from "../models/Notifications";

export interface NotificationsRepository {
  save(notifications: Notifications): void;
  get(): Notifications;
  clear(): void;
}