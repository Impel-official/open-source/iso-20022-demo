package io.lab577.xinfin.iso20022.gateway.lib.support

import com.prowidesoftware.swift.model.mx.BusinessAppHdrV02
import com.prowidesoftware.swift.model.mx.MxPacs00900110
import com.prowidesoftware.swift.model.mx.MxPain00200103
import com.prowidesoftware.swift.model.mx.dic.TransactionGroupStatus3Code
import io.lab577.xinfin.iso20022.demo.utils.IsoXml
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.nio.file.Paths
import javax.xml.bind.UnmarshalException

class IsoXmlTest {

    @Test
    fun `should map pain_002_001_03 xml to MX message`() {
        // GIVEN
        val xml = Paths.get("src/test/resources/samples/pain.002.001.03.xml")
            .toFile().readText()
        // WHEN
        val mx = IsoXml.xmlToMx(xml)
        // THEN
        assertTrue(mx is MxPain00200103)
        assertEquals("pain.002.001.03", mx.mxId.id())
        assertEquals(TransactionGroupStatus3Code.ACTC, (mx as MxPain00200103).cstmrPmtStsRpt.orgnlGrpInfAndSts.grpSts)
    }

    @Test
    fun `should map pain_002_001_03 xml with header to MX message`() {
        // GIVEN
        val xml = Paths.get("src/test/resources/samples/pain.002.001.03_header.xml")
            .toFile().readText()
        // WHEN
        val mx = IsoXml.xmlToMx(xml)
        // THEN
        assertTrue(mx is MxPain00200103)
        assertEquals("pain.002.001.03", mx.mxId.id())
        assertEquals(TransactionGroupStatus3Code.ACTC, (mx as MxPain00200103).cstmrPmtStsRpt.orgnlGrpInfAndSts.grpSts)
        assertTrue(mx.appHdr is BusinessAppHdrV02)
    }

    @Test
    fun `should map pacs_009_001_10 xml to to MX message`() {
        // GIVEN
        val xml = Paths.get("src/test/resources/samples/pacs.009.001.10.xml")
            .toFile().readText()
        // WHEN
        val mx = IsoXml.xmlToMx(xml)
        // THEN
        assertTrue(mx is MxPacs00900110)
        assertEquals("pacs.009.001.10", mx.mxId.id())
        assertEquals("END2ENDID1234", (mx as MxPacs00900110).fiCdtTrf.cdtTrfTxInf[0].pmtId.endToEndId)
    }

    @Test
    fun `should marshal and unmarshal ISO 20022 message`() {
        // GIVEN
        val xml = Paths.get("src/test/resources/samples/pacs.009.001.10.xml")
            .toFile().readText()
        // WHEN
        val mx = IsoXml.xmlToMx(xml)
        val mxXml = mx.message()
        // THEN
        assertEquals(xml, mxXml)
    }

    @Test
    fun `should fail on processing invalid xml`() {
        // GIVEN
        val xml = Paths.get("src/test/resources/samples/invalid_pacs.009.001.10_bad_element_names.xml")
            .toFile().readText()
        // WHEN/THEN
        assertThrows<UnmarshalException> {
            IsoXml.xmlToMx(xml)
        }
    }
}
