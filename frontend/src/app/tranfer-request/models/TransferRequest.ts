import {MessageCollateral} from "../../shared/api";

export class TransferRequest {
  constructor(
    public readonly senderBic: string,
    public readonly recipientBic: string,
    public readonly senderAccount: string,
    public readonly recipientAccount: string,
    public readonly amount: string,
    public readonly currency: string,
    public readonly referenceInfo: string,
    public readonly delayedTracking: boolean,
    public readonly collateral?: MessageCollateral
  ) {
    this.validate()
  }

  validate(){
    // TODO: validate fields and throw error events
  }
}
