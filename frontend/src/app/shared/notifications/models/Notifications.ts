import { Notification, NotificationType } from "./Notification";

export class Notifications {
  public readonly unreadCount: number;
  constructor(
    public readonly notifications: Array<Notification<NotificationType>>
  ) {
    this.unreadCount = this.notifications.filter(it=>it.state === 'UNREAD').length;
  }
  
  markAsRead(id: string): Notifications {
    const notifications = this.notifications.map(it => {
      if(it.id === id) return it.markAsRead();
      return it.copy();
    });
    return new Notifications(notifications);
  }
  
  add(notification: Notification<NotificationType>): Notifications {
    if(this.has(notification.id)) return new Notifications(this.notifications);
    const notifications = this.notifications.map(it=> it.copy());
    notifications.unshift(notification);
    return new Notifications(notifications);
  }
  
  remove(id: string): Notifications {
    const notifications = this.notifications.filter(it=> it.id !== id);
    return new Notifications(notifications);
  }
  
  has(id: string): boolean {
    return this.notifications.find(it=> it.id === id) !== undefined;
  }
  
  static empty(): Notifications {
    return new Notifications([]);
  }
}