package io.lab577.xinfin.iso20022.demo.domain.entity

import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "transaction_message_collateral")
class TransactionMessageCollateralEntity(
    id: UUID? = null,
    @Column(name = "type", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    var type: TransactionMessageCollateralType,
    @Column(name = "amount", precision = 50, scale = 25, nullable = false, updatable = false)
    var amount: BigDecimal,
    @Column(name = "exchange_price", precision = 50, scale = 25, nullable = true, updatable = false)
    var exchangePrice: BigDecimal? = null,
    @Column(name = "exchange_currency", nullable = true, updatable = false)
    val exchangeCurrency: String? = null,
    @Column(name = "token_symbol", nullable = true, updatable = false)
    val tokenSymbol: String? = null,
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_message_id", nullable = false, updatable = false)
    var transactionMessage: TransactionMessageEntity,
) : AbstractAuditableEntity(id)
