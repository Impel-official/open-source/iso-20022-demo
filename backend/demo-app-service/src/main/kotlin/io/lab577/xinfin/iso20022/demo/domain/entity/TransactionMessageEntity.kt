package io.lab577.xinfin.iso20022.demo.domain.entity

import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage
import org.hibernate.annotations.Type
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "transaction_messages")
class TransactionMessageEntity(
    id: UUID? = null,
    @Column(name = "e2e_id", nullable = false)
    val endToEndId: String,
    @Column(name = "sender_bic", nullable = false)
    val senderBic: String,
    @Column(name = "recipient_bic", nullable = false)
    val recipientBic: String,
    @Column(name = "counter_party_bic", nullable = false)
    var counterPartyBic: String,
    @Column(nullable = false, updatable = false, precision = 50, scale = 25)
    var amount: BigDecimal,
    @Column(nullable = false, updatable = false)
    var currency: String,
    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    val direction: TransactionMessageDirection,
    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    val type: PacsType,
    @Column(name = "message_date", nullable = false, updatable = false)
    val messageDate: OffsetDateTime,
    @Column(nullable = false, updatable = false)
    var network: String,
    @Column(nullable = true, updatable = false)
    val referenceInfo: String?,// = null,
    @Lob
    @Type(type = "text")
    @Column(nullable = false)
    var content: String,
    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "transaction_id", nullable = false)
    var transaction: TransactionEntity,
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "transactionMessage")
    var collateral: TransactionMessageCollateralEntity? = null,
) : AbstractBaseEntity(id) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(TransactionMessageEntity::class.java)
    }

    fun toDto() = TransactionMessage(
        id = id,
        transactionId = transaction.id,
        senderBic = senderBic,
        recipientBic = recipientBic,
        counterPartyBic = counterPartyBic,
        amount = amount,
        currency = currency,
        direction = direction,
        type = type,
        referenceInfo = referenceInfo,
        messageDate = messageDate,
        network = network,
        rollupTransactionHash = transaction.rollupTransactionHash
    )
}
