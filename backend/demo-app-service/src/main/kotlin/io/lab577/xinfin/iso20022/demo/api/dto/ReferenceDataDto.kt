package io.lab577.xinfin.iso20022.demo.api.dto

import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.PartyAccountDto
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.PartyDto

data class ReferenceDataDto(
    val bic: String,
    val statuses: List<String>,
    val currencies: List<String>,
    val parties: List<PartyDto>,
    val accounts: List<PartyAccountDto>
)
