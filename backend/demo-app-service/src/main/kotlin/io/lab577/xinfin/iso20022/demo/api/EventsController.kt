package io.lab577.xinfin.iso20022.demo.api

import io.cloudevents.CloudEvent
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.http.ResponseEntity

interface EventsController {
    @Operation(
        summary = "Handle events in CloudEvents format",
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(schema = Schema(implementation = CloudEvent::class))],
            required = true
        ),
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "Bad request", responseCode = "400"),
            ApiResponse(description = "Error", responseCode = "500")
        ]
    )
    fun handleEvent(cloudEvent: CloudEvent): ResponseEntity<Void>
}
