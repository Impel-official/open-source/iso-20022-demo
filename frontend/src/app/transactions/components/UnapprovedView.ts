import {customElement, query} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import {transactionsStore, TransactionsStore} from '../TransactionsStore';
import {TransactionsTableFilter} from '../model/TransactionsTableFilter';
import {TransactionSelectedEvent} from './table/TransactionsTable';
import {messagesStore, MessagesStore} from '../../shared/messages/MessagesStore';
import {Router} from '@vaadin/router';
import {ActionEndedEvent} from "./details/UnapprovedTransactionConfirmationButtons";

const styles = css`
.transactions-table {
  margin-top: 2em;
}
.paging {
   margin-top: 0.5em;
   margin-bottom: 2em;
}
`

@customElement('unapproved-view')
export class UnapprovedView extends LitElement {
  static override styles = styles;

  @query('#transaction-dialog')
  private transactionDialog: any;
  @query('#message-view-dialog')
  private messageViewDialog: any;

  private transactionsStore: TransactionsStore;
  private messagesStore: MessagesStore;

  constructor() {
    super();
    // TODO: this needs to provided as constructor parameter
    this.transactionsStore = transactionsStore;
    this.messagesStore = messagesStore;
  }

  override async connectedCallback() {
    super.connectedCallback();
    this.transactionsStore.bindComponentToPropertyChanges(this);
    this.transactionsStore.setTransactionsPagingFromQueryParams();
    const unapprovedTransactionsFilter =
        new TransactionsTableFilter('', '', undefined, '', undefined);
    this.transactionsStore.setTransactionsTableFilter(unapprovedTransactionsFilter);
    await this.transactionsStore.getFilteredTransactionsTable();
    this.transactionsStore.addLocationChangesListener(
        this,
        this.locationChangeHandler.bind(this)
    );
  }

  override disconnectedCallback() {
    super.disconnectedCallback();
    this.transactionsStore.unbindComponentFromPropertyChanges(this);
    this.transactionsStore.removeLocationChangesListener(this,);
  }

  async openTransactionsDialog(event: TransactionSelectedEvent) {
    await this.transactionsStore.getTransactionDetails(event.detail.id);
    this.transactionDialog.show();
  }

  handleActionEndedEvent(event: ActionEndedEvent) {
    if (event.detail.isSuccess) this.transactionDialog.close();
  }

  async closeTransactionsDialog() {
    this.transactionDialog.close();
  }

  async closeMessageViewDialog() {
    this.messageViewDialog.close();
    this.transactionDialog.show();
  }

  private async locationChangeHandler(location: Router.Location) {
    if (location?.route?.name == 'unapproved') {
      this.transactionsStore.setTransactionsPagingFromQueryParams();
      await this.transactionsStore.getFilteredTransactionsTable();
    }
  }

  override render() {
    return html`
        <div class="transactions-table">
            <transactions-table
                    tableTitle='Unapproved Transactions List'
                    .transactionsTable=${this.transactionsStore.transactionsTable}
                    @transactionSelected=${this.openTransactionsDialog}
            >
            </transactions-table>
        </div>

        <ui5-dialog id="transaction-dialog" stretch>
            <transaction-details .transaction=${this.transactionsStore.selectedTransaction}>
            </transaction-details>
            <unapproved-transaction-confirmation-buttons
                    @actionEndedEvent=${this.handleActionEndedEvent}
                    slot="footer"
            >
                <ui5-button design="Default"
                            @click=${this.closeTransactionsDialog}
                            slot="extra-buttons"
                >Close
                </ui5-button>
            </unapproved-transaction-confirmation-buttons>
        </ui5-dialog>

        <ui5-dialog id="message-view-dialog" stretch>
            <pre>${this.transactionsStore.selectedTransactionMessageRaw}</pre>
            <div slot="footer" class="buttons-area">
                <ui5-button design="Default" @click=${this.closeMessageViewDialog}>Close</ui5-button>
            </div>
        </ui5-dialog>
    `
  }
}
