import { ProgressUpdate } from "../../shared/progressUpdates/models/ProgressUpdate";

const message1 = ProgressUpdate.new('Waiting', 'Verifying request');
const message2 =  ProgressUpdate.new('Waiting', 'Creating pacs009 message');
const message3 = ProgressUpdate.new('Waiting', 'Encrypting message');
const message4 = ProgressUpdate.new('Waiting', 'Committing to blockchain');

export const waitingState : Readonly<Array<ProgressUpdate>> = [
  message1,
  message2,
  message3,
  message4
];

export const successState : Readonly<Array<ProgressUpdate>> = [
  new ProgressUpdate(message1.id, 'Success', message1.description),
  new ProgressUpdate(message2.id, 'Success', message2.description),
  new ProgressUpdate(message3.id, 'Success', message3.description),
  new ProgressUpdate(message4.id, 'Success', message4.description),
];

export const errorState : Readonly<Array<ProgressUpdate>> = [
  new ProgressUpdate(message1.id, 'Error', message1.description),
  new ProgressUpdate(message2.id, 'Error', message2.description),
  new ProgressUpdate(message3.id, 'Error', message3.description),
  new ProgressUpdate(message4.id, 'Error', message4.description),
];

export const skippedState : Readonly<Array<ProgressUpdate>> = [
  new ProgressUpdate(message1.id, 'Skipped', message1.description),
  new ProgressUpdate(message2.id, 'Skipped', message2.description),
  new ProgressUpdate(message3.id, 'Skipped', message3.description),
  new ProgressUpdate(message4.id, 'Skipped', message4.description),
];
