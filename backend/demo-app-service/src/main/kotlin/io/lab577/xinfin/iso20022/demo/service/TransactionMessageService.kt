package io.lab577.xinfin.iso20022.demo.service

import com.prowidesoftware.swift.model.mx.AbstractMX
import io.lab577.xinfin.iso20022.common.lib.iso.support.createPacs002
import io.lab577.xinfin.iso20022.demo.config.WebSocketConfig.Companion.WEBSOCKET_TOPIC_TRANSACTIONS
import io.lab577.xinfin.iso20022.demo.config.WebSocketConfig.Companion.WEBSOCKET_TOPIC_TRANSACTION_MESSAGES
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionEntity
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageCollateralEntity
import io.lab577.xinfin.iso20022.demo.domain.entity.TransactionMessageEntity
import io.lab577.xinfin.iso20022.demo.domain.model.PacsType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageCollateralType
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionMessageDirection.OUTBOX
import io.lab577.xinfin.iso20022.demo.domain.model.TransactionStatus
import io.lab577.xinfin.iso20022.demo.domain.repository.TransactionMessageCollateralEntityRepository
import io.lab577.xinfin.iso20022.demo.domain.repository.TransactionMessageEntityRepository
import io.lab577.xinfin.iso20022.demo.integration.iso20022.Iso20022ClientService
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.MessageInfo
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageRequest
import io.lab577.xinfin.iso20022.demo.integration.iso20022.client.model.SendMessageRequestCollateral
import io.lab577.xinfin.iso20022.demo.pacs.PacsOverview
import io.lab577.xinfin.iso20022.demo.pacs.PacsOverviewConverter
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessage
import io.lab577.xinfin.iso20022.demo.transactions.model.TransactionMessageEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional(readOnly = true)
class TransactionMessageService(
    repository: TransactionMessageEntityRepository,
    @Lazy private val transactionService: TransactionService,
    private val iso20022ClientService: Iso20022ClientService,
    private val simpTemplate: SimpMessagingTemplate,
    private val securityHelper: SecurityHelper,
    private val transactionMessageCollateralEntityRepository: TransactionMessageCollateralEntityRepository,
) : BaseService<TransactionMessageEntity, UUID, TransactionMessageEntityRepository>(repository) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(TransactionMessageService::class.java)
    }

    /** Handle updates without going through domain events */
    override fun postSave(resource: TransactionMessageEntity) {
        if (listOf(TransactionStatus.Received, TransactionStatus.Requested).contains(resource.transaction.status)) {
            simpTemplate.convertAndSend(WEBSOCKET_TOPIC_TRANSACTIONS, resource.transaction.toDto())
        } else {
            simpTemplate.convertAndSend(WEBSOCKET_TOPIC_TRANSACTION_MESSAGES, TransactionMessageEvent.from(resource))
        }
    }

    @Transactional(readOnly = false)
    fun save(
        message: MessageInfo,
        mx: AbstractMX,
        direction: TransactionMessageDirection,
        assignedId: UUID = UUID.randomUUID()
    ): TransactionMessageEntity {
        val pacsOverview = PacsOverviewConverter.process(message, mx)
        val transaction = getOrCreateTransaction(pacsOverview, direction, assignedId)
        val counterPartyBic = if (direction == OUTBOX) pacsOverview.recipientBic else pacsOverview.senderBic
        val transactionMessage = save(
            TransactionMessageEntity(
                id = assignedId,
                endToEndId = pacsOverview.end2endId,
                senderBic = pacsOverview.senderBic,
                recipientBic = pacsOverview.recipientBic,
                counterPartyBic = counterPartyBic,
                amount = pacsOverview.amount,
                currency = pacsOverview.currency,
                direction = direction,
                type = pacsOverview.type,
                messageDate = pacsOverview.messageDate,
                referenceInfo = pacsOverview.referenceInfo,
                network = "xinfin",
                transaction = transaction,
                content = pacsOverview.rawXml
            )
        )
        pacsOverview.collateral?.let {
            val transactionMessageCollateral = transactionMessageCollateralEntityRepository.save(
                TransactionMessageCollateralEntity(
                    type = it.type,
                    amount = it.amount,
                    exchangePrice = it.exchangePrice,
                    exchangeCurrency = it.exchangeCurrency,
                    tokenSymbol = it.tokenSymbol,
                    transactionMessage = transactionMessage,
                )
            )
            transactionMessage.collateral = transactionMessageCollateral
        }
        transaction.messages.add(transactionMessage)
        return transactionMessage
    }

    fun findAllSummariesByTransactionId(transactionId: UUID): List<TransactionMessage> =
        repository.findAllSummariesByTransactionId(transactionId)

    fun findRawContentById(transactionId: UUID): String? =
        repository.findRawContentById(transactionId)

    private fun getOrCreateTransaction(
        overview: PacsOverview,
        direction: TransactionMessageDirection,
        assignedId: UUID = UUID.randomUUID(),
    ): TransactionEntity {
        return if (overview.type == PacsType.PACS_009) {
            TransactionEntity(
                id = assignedId,
                endToEndId = overview.end2endId,
                senderBic = overview.senderBic,
                recipientBic = overview.recipientBic,
                senderAccount = overview.senderAccount ?: error("Input has no senderAccount"),
                senderXdcAddress = "xdcAddress",
                recipientAccount = overview.recipientAccount ?: error("Input has no recipientAccount"),
                counterPartyBic = if (direction == OUTBOX) overview.recipientBic else overview.senderBic,
                amount = overview.amount,
                currency = overview.currency,
                direction = direction,
                status = TransactionStatus.New,
                requestDate = overview.messageDate,
                validUntil = overview.validUntil ?: error("Input has no validUntil"),
                referenceInfo = overview.referenceInfo,
                nativeCollateralAmount = when (overview.collateral?.type) {
                    TransactionMessageCollateralType.NATIVE -> overview.collateral.amount
                    else -> null
                },
                exchangePrice = when (overview.collateral?.type) {
                    TransactionMessageCollateralType.NATIVE -> overview.collateral.exchangePrice
                    else -> null
                },
                exchangeCurrency = when (overview.collateral?.type) {
                    TransactionMessageCollateralType.NATIVE -> overview.collateral.exchangeCurrency
                    else -> null
                },
                initiator = securityHelper.getCurrentUserName(),
                responder = null,
                messages = mutableListOf()
            )
        } else transactionService.getByEndToEndId(overview.end2endId)
    }

    fun sendPacs002(transaction: TransactionEntity, approve: Boolean): UUID {
        val pacs002 = createPacs002(
            senderBic = transaction.recipientBic,
            recipientBic = transaction.senderBic,
            amount = transaction.amount,
            currency = transaction.currency,
            endToEndId = transaction.endToEndId,
            approve = approve,
        )
        val request = SendMessageRequest()
        request.endToEndId = transaction.endToEndId
        request.sender = transaction.recipientBic
        request.recipient = transaction.senderBic
        request.payloadType = SendMessageRequest.PayloadTypeEnum.ISO20022
        request.payload = pacs002
        request.collateral = when {
            approve -> null
            transaction.messages[0].collateral != null -> {
                val pacs009Collateral = transaction.messages[0].collateral!!
                val collateral = SendMessageRequestCollateral()
                collateral.type = when (pacs009Collateral.type) {
                    TransactionMessageCollateralType.NATIVE -> SendMessageRequestCollateral.TypeEnum.NATIVE
                    TransactionMessageCollateralType.ERC20 -> SendMessageRequestCollateral.TypeEnum.ERC20
                }
                collateral.amount = pacs009Collateral.amount
                collateral.exchangePrice = pacs009Collateral.exchangePrice
                collateral.exchangeCurrency = pacs009Collateral.exchangeCurrency
                collateral.tokenSymbol = pacs009Collateral.tokenSymbol
                collateral
            }
            else -> null
        }
        return iso20022ClientService.sendMessage(request).messageId
    }
}
