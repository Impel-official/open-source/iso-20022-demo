import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import utc from "dayjs/plugin/utc";
import tz from "dayjs/plugin/timezone";
import {RouteName} from "../routing/routeConfig";

dayjs.extend(duration)
dayjs.extend(utc)
dayjs.extend(tz)

declare global {
  interface Window { config: AppConfig; }
}

// TODO: check if json schema is correct
export const config: AppConfig = window.config;

console.info('Running with config', config)

export type AppConfig = {
  host: string,
  baseUrl: string,
  branding : {
    logoUrl: string,
    icoUrl: string,
    orgName: string
  },
  api?: {
    requestTimeoutMs?: number,
    retryAttempts?: number,
    retryTimeIntervalMs?: number,
  }
  rest: {
    references: string,
    transactions: string,
    transactionMessages: string,
    pacs009: string,
    transactionMetrics: string,
    prices: string,
    hubSpotContacts: string
  },
  stomp: {
    url: string,
    newTransactionsTopic: string,
    transactionsChangesTopic: string,
    transactionsMetricsTopic: string
  },
  hubSpotAnalyticsEnabled: boolean,
  auth: {
    type: 'NONE' | 'KEYCLOAK' | 'HUB_SPOT',
    keycloak: {
      url: string,
      realm: string,
      clientId: string,
    },
    hubSpot: {
      unauthorizedMessage: string,
      unauthorizedMessageLink: string
    }
  },
  xdc: {
    // Mainnet: https://observer.xdc.org/transaction-details/
    // Apothem: https://explorer.apothem.network/txs/
    txPrefixUrl: string
  },
  bic: string,
  progressUpdatesIntervalMs: number,
  progressUpdatesDelayedIntervalMs: number,
  shareDemoExperience: {
    text: string,
    url: string,
    twitterHashTags: string
  },
  defaultView: RouteName,
  reportingNetworkTableDisabled: boolean,
  reportingReceivedTableDisabled: boolean,
  featureFlags: {
    transactionsView: {
      showAcceptRejectButtons: boolean,
      showSelectCheckbox: boolean,
    }
  },
  collateralTypes: Array<{
    type: 'NATIVE' | 'ERC20',
    symbol: string,
    name: string,
  }>
}
