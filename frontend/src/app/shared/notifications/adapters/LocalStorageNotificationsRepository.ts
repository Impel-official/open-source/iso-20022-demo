import { NotificationsRepository } from "./NotificationsRepository";
import { Notifications } from "../models/Notifications";
import { Notification } from "../models/Notification";


export class LocalStorageNotificationsRepository implements NotificationsRepository {
  private KEY = 'notifications';
  
  constructor(private localStorage:Storage) {}
  
  clear(): void {
    localStorage.removeItem(this.KEY);
  }

  get(): Notifications {
    const serialisedNotifications = this.localStorage.getItem(this.KEY);
    if(serialisedNotifications === null) return Notifications.empty();
    return this.deserialize(serialisedNotifications);
  }

  save(notifications: Notifications): void {
    this.localStorage.setItem(this.KEY, this.serialise(notifications))
  }
  
  private serialise(notifications: Notifications): string {
    return JSON.stringify(notifications);
  }
  
  private deserialize(notificationsString: string): Notifications {
    try {
      const notificationsJson: any = JSON.parse(notificationsString);
      // TODO: check json schema
      const notificationsList = notificationsJson.notifications.map((it: any) => {
        return new Notification(
          it.id,
          it.type,
          it.title,
          it.description,
          it.from, 
          it.timestamp,
          it.payload,
          it.state
        );
      });
      return new Notifications(notificationsList);
    } catch (e) {
      console.error("Error parsing notifications from local storage, clearing, returning empty");
      this.clear()
      return Notifications.empty();
    }
  }
}

const localStorage = window.localStorage;
export const notificationsRepository = 
  new LocalStorageNotificationsRepository(localStorage);