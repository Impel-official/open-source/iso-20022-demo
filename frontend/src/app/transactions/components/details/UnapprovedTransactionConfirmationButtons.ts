import {customElement} from 'lit/decorators.js';
import {css, html, LitElement} from 'lit';
import { MessagesStore, messagesStore } from "../../../shared/messages/MessagesStore";
import { TransactionsStore, transactionsStore } from "../../TransactionsStore";
import {RoutingService, routingService} from '../../../shared/routing/RoutingService';
import {ReferenceDataStore, referenceDataStore} from "../../ReferenceDataStore";

const styles = css`
:host {
  width: 100%;
}
.buttons-area {
  display: flex;
  width: 100%;
  padding: 1rem 0;
  box-sizing: border-box;
}
.buttons-area ui5-button {
  margin-right: 1em;
}
`;

export type IsSuccess = { isSuccess: boolean };
export type ActionEndedEvent = CustomEvent<IsSuccess>;

@customElement('unapproved-transaction-confirmation-buttons')
export class UnapprovedTransactionConfirmationButtons extends LitElement {
  static override styles = styles;

  private messagesStore: MessagesStore;
  private transactionsStore: TransactionsStore;
  private referenceDataStore: ReferenceDataStore;
  private routingService: RoutingService;

  constructor() {
    super();
    // TODO: this needs to provided as constructor parameter
    this.transactionsStore = transactionsStore;
    this.messagesStore = messagesStore;
    this.referenceDataStore = referenceDataStore;
    this.routingService = routingService;
  }
  override connectedCallback() {
    super.connectedCallback();
    this.transactionsStore.bindComponentToPropertyChanges(this);
  }

  /* A function that is called when the user clicks on the approve or reject button. */
  async transactionConfirmation(approve: boolean){
    try {
      await this.transactionsStore.confirmSelectedTransaction(approve);
      await this.dispatchActionEndedEvent(true);
      this.messagesStore.showMessage('Positive', 'Success');
    } catch(e){
      await this.dispatchActionEndedEvent(false);
      this.messagesStore.showUnexpectedErrorMessage();
    }
  }

  dispatchActionEndedEvent(isSuccess: boolean){
    const eventOptions = {
      detail: { isSuccess },
      bubbles: true,
      composed: true
    };
    const actionEndedEvent: ActionEndedEvent =
      new CustomEvent<IsSuccess>('actionEndedEvent', eventOptions);
    this.dispatchEvent(actionEndedEvent);
  }


  override render() {

    const tx = this.transactionsStore.selectedTransaction!;
    const bic = this.referenceDataStore.bic;
    const unapproved = tx && tx.isUnapproved();

    return html`
        <ui5-busy-indicator
                size="Medium"
                .active=${this.transactionsStore.selectedTransaction?.isApprovalPending}
                class="buttons-area">
            <slot name="extra-buttons"></slot>
            ${ unapproved && tx.recipientBic === bic  
                ?  this.renderButtonsForRejectable()
                :  this.renderButtonsForUnrejectable()
            }
        </ui5-busy-indicator>
    `
  }

  private renderButtonsForUnrejectable() {
    return html`
        <ui5-button @click=${() => this.routingService.navigateToRoute('transactions')}>
            Back to Transactions
        </ui5-button>
    `
  }

  private renderButtonsForRejectable() {
    return html` ${!this.transactionsStore.selectedTransaction?.isApprovalPending ?
        html`
            <ui5-button design="Negative" @click=${() => this.transactionConfirmation(false)}>
              Reject
            </ui5-button>
            <ui5-button design="Positive" @click=${() => this.transactionConfirmation(true)}>
              Approve
            </ui5-button>
            <ui5-button @click=${() => this.routingService.navigateToRoute('transactions')}>
                Back to Transactions
            </ui5-button>
          `
        : this.renderButtonsForUnrejectable()}
      `
  }
}
