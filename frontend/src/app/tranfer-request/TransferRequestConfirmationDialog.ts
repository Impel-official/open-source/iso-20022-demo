import { css, html, LitElement } from 'lit';
import { customElement, property, query } from "lit/decorators.js";
import { cardCss, commonCss, kvTableCss } from "../shared/config/styles";
import { RoutingService, routingService } from "../shared/routing/RoutingService";
import { referenceDataStore, ReferenceDataStore } from "../transactions/ReferenceDataStore";
import { PendingTransferRequest, TransferRequestStore, transferRequestStore } from "./TransferRequestStore";
import {formatDateTime} from "../shared/date-utils";
import {getCollateralText} from "../shared/utils";

const styles = css`
${commonCss}
${cardCss}
${kvTableCss}

#transfer-request-confirmation-dialog {
 width: 600px;
}
.dialog-footer {
  padding: 1rem;
}
`

@customElement('transfer-request-confirmation-dialog')
export class TransferRequestConfirmationDialog extends LitElement {
  static override styles = styles;

  private transferRequestStore: TransferRequestStore;
  private referenceDataStore: ReferenceDataStore;
  private routingService: RoutingService;

  @query('#transfer-request-confirmation-dialog')
  private dialog: any;

  @property()
  transferRequest?: PendingTransferRequest = undefined;
  @property()
  endToEndId = '';

  constructor() {
    super();
    this.routingService = routingService;
    this.transferRequestStore = transferRequestStore;
    this.referenceDataStore = referenceDataStore;
  }

  show() {
    this.dialog.show();
  }

  private handleCloseDialog() {
    this.dispatchEvent(new CustomEvent<any>('close'));
  }

  override render() {

    const currency = this.referenceDataStore.currencies.find(currency => currency.code === this.transferRequest?.currency);
    const currencySymbol = currency?.symbol || '?';
    const currencyCode = this.transferRequest?.currency;

    const collateralText = getCollateralText(this.transferRequest?.collateral, currencySymbol);
    const createdBy = this.referenceDataStore.parties
        .find(it => it.bic === this.transferRequest?.senderBic)
        ?.name
    const fromIban = this.referenceDataStore.accounts
        .find(it => it.iban === this.transferRequest?.senderAccount)
        ?.holder
    const toIban = this.referenceDataStore.accounts
        .find(it => it.iban === this.transferRequest?.recipientAccount)
        ?.holder

    return html`
        <ui5-dialog id="transfer-request-confirmation-dialog"
                    @after-close=${this.handleCloseDialog}>
            <slot name="header" slot="header"></slot>
            <div class="title">Transaction Confirmation</div>
            <table id="detailsTable" class="kv-table">
                <tr>
                    <td class="label vertical-th">Status</td>
                    <td class="value">Delivered - Requested</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Created By</td>
                    <td class="value">${createdBy}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Transaction ID</td>
                    <td class="value">${this.endToEndId}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Transfer Request Date</td>
                    <td class="value">${formatDateTime(this.transferRequest?.requestDate).replace(',', '')}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Valid Until</td>
                    <td class="value">${formatDateTime(this.transferRequest?.validUntil).replace(',', '')}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">From BIC/IBAN</td>
                    <td class="value">${this.transferRequest?.senderBic} / ${fromIban}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">To BIC/IBAN</td>
                    <td class="value">${this.transferRequest?.recipientBic} / ${toIban}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Special Instructions</td>
                    <td class="value">${this.transferRequest?.referenceInfo}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Amount</td>
                    <td class="value">${currencySymbol}${this.transferRequest?.amount} ${currencyCode}</td>
                </tr>
                <tr>
                    <td class="label vertical-th">Collateral</td>
                    <td class="value">${collateralText}</td>
                </tr>
            </table>
            <div slot="footer" class="dialog-footer">
                <ui5-button id="closeDialogButton"
                            design="Default"
                            @click=${() => this.dialog.close()}
                >Close
                </ui5-button>
            </div>
        </ui5-dialog>
    `
  }
}
