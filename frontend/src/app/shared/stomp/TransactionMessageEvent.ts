import {TransactionMessage} from '../../transactions/model/Transaction';
import {TransactionDtoStatusEnum} from "../api";

export class TransactionMessageEvent {
  constructor(
    public readonly transactionId: string,
    public readonly status: TransactionDtoStatusEnum,
    public readonly message: TransactionMessage
  ) {}
  
  static fromJson(json: any): TransactionMessageEvent {
    // TODO: schema validation
    return new TransactionMessageEvent(
      json.transactionId,
      json.status,
      TransactionMessage.fromJson(json.message)
    );
  }
  
}
