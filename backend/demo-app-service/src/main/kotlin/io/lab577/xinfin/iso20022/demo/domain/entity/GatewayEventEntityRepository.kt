package io.lab577.xinfin.iso20022.demo.domain.entity

import io.lab577.xinfin.iso20022.demo.domain.repository.BaseRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.OffsetDateTime
import java.util.*

@Repository
interface GatewayEventEntityRepository : BaseRepository<GatewayEventEntity, UUID> {
    @Query(value = "SELECT max(timestamp) FROM GatewayEventEntity")
    fun getLastEventDateTime(): OffsetDateTime?
}
