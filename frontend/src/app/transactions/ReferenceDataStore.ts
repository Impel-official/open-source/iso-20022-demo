import {MultipleHostsAbstractStore} from '../shared/MultipleHostsAbstractStore';
import {Currency} from './model/Currency';
import {State} from './model/State';
import {AppConfig, config} from "../shared/config";
import {Account} from "./model/Account";
import {Party} from './model/Party';
import {restAdapter, RestAdapter} from "../shared/rest/RestAdapter";

export class ReferenceDataStore extends MultipleHostsAbstractStore {
  bic = '';
  party: Party | undefined = undefined;
  currencies: Array<Currency> = [];
  states: Array<State> = [];
  parties: Array<Party> = [];
  accounts: Array<Account> = [];

  constructor(
    config: AppConfig,
    private restAdapter: RestAdapter
  ) {
    super(config);
  }

  async init() {
    await this.getReferenceData();
    this.updatePropertiesToBoundHosts();
  }

  async getReferenceData() {
    const referenceData = await this.restAdapter.getReferenceData();
    this.bic = referenceData.bic;
    this.party = referenceData.parties.find(it => it.bic === referenceData.bic);
    this.currencies = referenceData.currencies;
    this.states = referenceData.states;
    this.parties = referenceData.parties?.sort((a, b) => a.name?.localeCompare(b.name));
    this.accounts = referenceData.accounts?.sort((a, b) => a.holder?.localeCompare(b.holder));
  }
}

export const referenceDataStore = new ReferenceDataStore(config, restAdapter);
