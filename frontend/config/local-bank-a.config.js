var config = {
  host: 'http://localhost:8001',
  baseUrl: "",
  branding : {
    logoUrl: "/assets/logo-banka.svg",
    icoUrl: "/sandbox-bank-a/assets/banka-favicon.ico",
    orgName: "Bank A"
  },
  api: {
    requestTimeoutMs: 30000, // 30s
    retryAttempts: 3,
    retryTimeIntervalMs: 200, // 0.2s
  },
  rest: {
    references: "http://localhost:8095/api/gateway/iso20022/transaction-options",
    transactions: "http://localhost:8095/api/gateway/iso20022/transactions",
    transactionMessages: "http://localhost:8095/api/gateway/iso20022/transaction-messages",
    pacs009: "http://localhost:8095/api/gateway/iso20022/pacs/messages/pacs-009-001-10",
    transactionMetrics: "http://localhost:8095/api/gateway/iso20022/transaction-metrics",
    prices: "http://localhost:8095/api/kucoin/price",
    hubSpotContacts: "http://localhost:8095/api/hubspot/contact"
  },
  stomp: {
    url: "ws://localhost:8095/ws",
    newTransactionsTopic: "/topic/transactions",
    transactionsChangesTopic: "/topic/transaction-messages",
    transactionsMetricsTopic: "/topic/transaction-metrics"
  },
  hubSpotAnalyticsEnabled: true,
  auth: {
    type: 'HUB_SPOT',
    keycloak: {
      url: 'https://auth-banka-sandbox.impel-lab577.co.uk/auth',
      realm: 'BBBBCAXXXXX',
      clientId: 'gateway-ui',
    },
    hubSpot: {
      unauthorizedMessage: 'Please use a link from registration e-mail or visit ',
      unauthorizedMessageLink: "https://impel.global"
    }
  },
  xdc: {
    txPrefixUrl: 'https://explorer.apothem.network/txs/'
  },
  bic: "BBBBCAXXXXX",
  progressUpdatesIntervalMs: 750,
  progressUpdatesDelayedIntervalMs: 750 * 4,
  shareDemoExperience: {
    url: "https://impel.global",
  },
  defaultView: 'transfer-request',
  reportingNetworkTableDisabled: true,
  reportingReceivedTableDisabled: true,
  featureFlags: {
    transactionsView: {
      showAcceptRejectButtons: false, // https://gitlab.com/lab577/xinfin-iso20022/-/issues/269
      showSelectCheckbox: false // https://gitlab.com/lab577/xinfin-iso20022/-/issues/307
    }
  },
  collateralTypes: [
    { type: 'NATIVE', symbol: 'XDC', name: 'XDC'},
    { type: 'ERC20', symbol: 'BTC', name: 'xBTC (Wrapped BTC)'},
    { type: 'ERC20', symbol: 'USDC', name: 'xUSDC (Wrapped USDC)'}
  ]
}
