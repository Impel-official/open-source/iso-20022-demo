/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import {exists, mapValues} from '../runtime';
/**
 *
 * @export
 * @interface CountResponse
 */
export interface CountResponse {
  /**
   *
   * @type {number}
   * @memberof CountResponse
   */
  count: number;
}

export function CountResponseFromJSON(json: any): CountResponse {
  return CountResponseFromJSONTyped(json, false);
}

export function CountResponseFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean
): CountResponse {
  if (json === undefined || json === null) {
    return json;
  }
  return {
    count: json['count'],
  };
}

export function CountResponseToJSON(value?: CountResponse | null): any {
  if (value === undefined) {
    return undefined;
  }
  if (value === null) {
    return null;
  }
  return {
    count: value.count,
  };
}
