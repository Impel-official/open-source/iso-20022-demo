const bundleName = process.env.BUNDLE_NAME;
const urlPathName = process.env.URL_PATH || '';
const port = process.env.PORT;


const rootPath = `dist/${bundleName}`;

export default {
  open: `/${urlPathName}?xcx=ed4cd260-4f80-4fde-8a0d-a0950a9ec43d`,
  watch: true,
  nodeResolve: {exportConditions: ['development']},
  preserveSymlinks: true,
  rootDir: rootPath,
  appIndex: `${rootPath}/index.html`,
  plugins: [],
  port: parseInt(port),
  middleware: [],
};
