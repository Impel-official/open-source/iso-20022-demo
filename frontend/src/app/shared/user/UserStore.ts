import {SingleHostAbstractStore} from "../SingleHostAbstractStore";
import {ReactiveElement} from "lit";
import {User} from "./User";
import {config} from "../config";

export class UserStore extends SingleHostAbstractStore<ReactiveElement> {
  user?: User;

  setUser(user: User | undefined): void {
    this.user = user;
    this.updatePropertiesToBoundHost();
  }

  hasUser(): boolean {
    return this.user !== undefined;
  }
}

export const userStore = new UserStore(config);
